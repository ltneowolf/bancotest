#!/bin/bash

ROOT_PATH="/var/www/controlvalley/LinuxService"

case "$1" in
start)
   if [ -e $ROOT_PATH/controlpid ]; then
         echo El servicio se esta ejecutando, pid=`cat $ROOT_PATH/controlpid`
   else
        $ROOT_PATH/programador.sh & echo $!>$ROOT_PATH/controlpid
   fi
   ;;
stop)
   kill `cat $ROOT_PATH/controlpid`
   rm $ROOT_PATH/controlpid
   ;;
restart)
   $0 stop
   $0 start
   ;;
status)
   if [ -e $ROOT_PATH/controlpid ]; then
      echo El servicio se esta ejecutando, pid=`cat $ROOT_PATH/controlpid`
   else
      echo El servicio no se esta ejecutando
      exit 1
   fi
   ;;
*)
   echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0