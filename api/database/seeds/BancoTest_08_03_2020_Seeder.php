<?php

use App\Models\GE\Gemodulo;
use App\Models\GE\Geparameters;
use App\Models\GE\Getipo;
use App\Models\SE\Seendpoint;
use \App\Models\SE\SeendpointMethod;
use App\Models\SE\Serole;
use App\Models\SE\SeuserRole;
use App\Models\User;
use Illuminate\Database\Seeder;

class BancoTest_08_03_2020_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Corriendo el Seeder " . __CLASS__ . "\n";

        echo "Obteniendo modulo GENERAL " . __LINE__ . "\n";
        $modulo = Gemodulo::where(["nombre" => "GENERAL"])->first();

        echo "Creando Parametro Grupo Tipo Identificaciones " . __LINE__ . "\n";
        $paramTiposId = Geparameters::create(array(
            "key" => "GRO_TIPO_IDENTIFICACIONES",
            "strval" => "TIPOID",
            "observation" => "Parametro para el Grupo de Tipo Identificaciones",
            "gemodulo_id" => $modulo->id
        ));

        echo "Creando Parametro Valor Minimo Activacion " . __LINE__ . "\n";
        Geparameters::create(array(
            "key" => "MINIMO_ACTIVACION_CUENTA",
            "numval" => 100000,
            "observation" => "Parametro con el valor minimo que la cuenta debe tener para poder realizar la activacion",
            "gemodulo_id" => $modulo->id
        ));

        /********************************************************************************************************************************/

        echo "Creando Tipos Identificacion " . __LINE__ . "\n";
        $tiposId = array(
            ['nombre' => 'NIT', 'abreviatura' => 'NIT', 'gemodulo_id' => $modulo->id, 'grupo' => $paramTiposId->strval],
            ['nombre' => 'Cedula Ciudadania', 'abreviatura' => 'CC', 'gemodulo_id' => $modulo->id, 'grupo' => $paramTiposId->strval],
            ['nombre' => 'Tarjeta de Identidad', 'abreviatura' => 'TI', 'gemodulo_id' => $modulo->id, 'grupo' => $paramTiposId->strval],
            ['nombre' => 'Cedula Extranjeria', 'abreviatura' => 'CE', 'gemodulo_id' => $modulo->id, 'grupo' => $paramTiposId->strval],
        );

        foreach ($tiposId as $tipoId) {
            Getipo::create($tipoId);
        }

        /********************************************************************************************************************************/

        echo "Creando Tipos Transacciones y Parametros " . __LINE__ . "\n";

        $consignacion = Getipo::create(['nombre' => 'Consignacion', 'gemodulo_id' => $modulo->id]);

        Geparameters::create(array(
            "key" => "TIPO_TRS_CONSIGNACION",
            "numval" => $consignacion->id,
            "observation" => "Parametro del id del tipo de operacion",
            "gemodulo_id" => $modulo->id
        ));

        $retiro = Getipo::create(['nombre' => 'Retiro', 'gemodulo_id' => $modulo->id]);

        Geparameters::create(array(
            "key" => "TIPO_TRS_RETIRO",
            "numval" => $retiro->id,
            "observation" => "Parametro del id del tipo de operacion",
            "gemodulo_id" => $modulo->id
        ));

        /********************************************************************************************************************************/

        echo "Asignando persimos al rol User del endpoint geparametersServ y tipos " . __LINE__ . "\n";

        $sendpointInstanceParam = Seendpoint::where('state', 'geparametersServ')->first();

        $sendpointInstanceTipo = Seendpoint::create(["state" => "tiposOcult", "menu_label" => "TIPOS_LABEL", "show" => "0",
            "page_desc" => "TIPOS_LABEL"]);

        $sendpointInstanceCiudad = Seendpoint::create(["state" => "consultaCiudad", "menu_label" => "CONSULTA_CIUDAD_LABEL", "show" => "0",
            "page_desc" => "CONSULTA_CIUDAD_LABEL"]);

        $rolInstance = Serole::where('name', 'User')->first();

        SeendpointMethod::create([
            'method' => 'GET',
            'seendpoint_id' => $sendpointInstanceParam->id,
            'serole_id' => $rolInstance->id
        ]);

        SeendpointMethod::create([
            'method' => '*',
            'seendpoint_id' => $sendpointInstanceTipo->id,
            'serole_id' => $rolInstance->id
        ]);

        SeendpointMethod::create([
            'method' => 'GET',
            'seendpoint_id' => $sendpointInstanceCiudad->id,
            'serole_id' => $rolInstance->id
        ]);

        /********************************************************************************************************************************/

        echo "Creando rol Cajero y Asesor " . __LINE__ . "\n";
        $rolCajero = Serole::create(['name' => 'Cajero']);
        $rolAsesor = Serole::create(['name' => 'Asesor']);

        echo "Creando GEPARAMETERS para el rol cajero " . __LINE__ . "\n";
        Geparameters::create([
            "key" => "ROL_CAJERO",
            "numval" => $rolCajero->id,
            "observation" => "Id del Rol Cajero",
            "gemodulo_id" => $modulo->id
        ]);

        echo "Creando GEPARAMETERS para el rol asesor " . __LINE__ . "\n";
        Geparameters::create([
            "key" => "ROL_ASESOR",
            "numval" => $rolAsesor->id,
            "observation" => "Id del Rol Asesor",
            "gemodulo_id" => $modulo->id
        ]);

        echo "Obteniendo rol Usuario " . __LINE__ . "\n";
        $roleUsuario = Serole::where(['name' => 'User'])->first();

        /********************************************************************************************************************************/

        $endPointClientes = Seendpoint::create(["state" => "clientes", "menu_label" => "CLIENTES_LABEL", "show" => "1",
            "page_desc" => "CLIENTES_DESC_LABEL", "icon" => "assignment_ind"]);

        SeendpointMethod::create(["method" => "*", "seendpoint_id" => $endPointClientes->id, "serole_id" => 1]);
        SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $endPointClientes->id, "serole_id" => $rolAsesor->id]);
        SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endPointClientes->id, "serole_id" => $rolAsesor->id]);
        SeendpointMethod::create(["method" => "POST", "seendpoint_id" => $endPointClientes->id, "serole_id" => $rolAsesor->id]);

        $endPointCuentas = Seendpoint::create(["state" => "cuentas", "menu_label" => "CUENTAS_LABEL", "show" => "1",
            "page_desc" => "CUENTAS__DESC_LABEL", "icon" => "account_balance"]);

        SeendpointMethod::create(["method" => "*", "seendpoint_id" => $endPointCuentas->id, "serole_id" => 1]);
        SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $endPointCuentas->id, "serole_id" => $rolAsesor->id]);
        SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endPointCuentas->id, "serole_id" => $rolAsesor->id]);
        SeendpointMethod::create(["method" => "POST", "seendpoint_id" => $endPointCuentas->id, "serole_id" => $rolAsesor->id]);

        SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endPointCuentas->id, "serole_id" => $rolCajero->id]);

        $endPointTransaccion = Seendpoint::create(["state" => "transacciones", "menu_label" => "TRANSACCIONES_LABEL", "show" => "1",
            "page_desc" => "TRANSACCIONES__DESC_LABEL", "icon" => "monetization_on"]);

        SeendpointMethod::create(["method" => "POST", "seendpoint_id" => $endPointTransaccion->id, "serole_id" => $rolCajero->id]);
        SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endPointTransaccion->id, "serole_id" => $rolCajero->id]);

        SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endPointTransaccion->id, "serole_id" => $rolAsesor->id]);

        /********************************************************************************************************************************/

        echo "Creando Usuarios prueba " . __LINE__ . "\n";

        $asesor1 = User::create(['name' => 'Asesor 1', 'email' => 'asesor1@test.com', 'password' => 'asesor1']);
        $asesor2 = User::create(['name' => 'Asesor 2', 'email' => 'asesor2@test.com', 'password' => 'asesor2']);

        $cajero1 = User::create(['name' => 'Cajero 1', 'email' => 'cajero1@test.com', 'password' => 'cajero1']);
        $cajero2 = User::create(['name' => 'Cajero 2', 'email' => 'cajero2@test.com', 'password' => 'cajero2']);

        $arrayPermisos = array(
            ['user_id' => $asesor1->id, 'serole_id' => $rolAsesor->id],
            ['user_id' => $asesor1->id, 'serole_id' => $roleUsuario->id],
            ['user_id' => $asesor2->id, 'serole_id' => $rolAsesor->id],
            ['user_id' => $asesor2->id, 'serole_id' => $roleUsuario->id],
            ['user_id' => $cajero1->id, 'serole_id' => $rolCajero->id],
            ['user_id' => $cajero1->id, 'serole_id' => $roleUsuario->id],
            ['user_id' => $cajero2->id, 'serole_id' => $rolCajero->id],
            ['user_id' => $cajero2->id, 'serole_id' => $roleUsuario->id],
        );

        foreach ($arrayPermisos as $permiso) {
            SeuserRole::create($permiso);
        }
        /**************************************************************************************************************/

        echo "Creando Traducciones " . __LINE__ . " \n";

        $newLangDefs = array(
            ["key" => "APELLIDOS_LABEL", "value" => "Apellidos", "gelanguage_id" => "2"],
            ["key" => "CUENTAS_LABEL", "value" => "Cuentas", "gelanguage_id" => "2"],
            ["key" => "TRANSACCIONES_LABEL", "value" => "Transacciones", "gelanguage_id" => "2"],
            ["key" => "INFO_GENERAL_LABEL", "value" => "Informacion General", "gelanguage_id" => "2"],
            ["key" => "CURRENT_PASSWORD_LABEL", "value" => "Contraseña Actual", "gelanguage_id" => "2"],
            ['key' => 'CHANGE_PASSWORD_FIELD_LABEL',
                'value' => 'Cambiar contraseña',
                'gelanguage_id' => 2],
            ['key' => 'PASSWORD_RULES_FIELD_LABEL',
                'value' => 'Utilice 6 caracteres como mínimo, con una combinación de letras (mayúsculas y minúsculas), números y símbolos.',
                'gelanguage_id' => 2],
            ['key' => 'PASS_MIN_ERROR',
                'value' => 'La contraseña debe tener mas de 6 caracteres.',
                'gelanguage_id' => 2],
            ['key' => 'PASS_RUL_BAS_ERROR',
                'value' => 'La contraseña debe tener como mínimo una letra en mayúscula y un numero.',
                'gelanguage_id' => 2],
            ["key" => "CTA_NO_EXISTE_LABEL", "value" => "Cuenta no existe", "gelanguage_id" => "2"],
            ["key" => "CTA_NO_ACTIVADA_LABEL", "value" => "La cuenta no se encuentra activa", "gelanguage_id" => "2"],
            ["key" => "MONTO_NEGATIVO_LABEL", "value" => "El monto no puede ser negativo", "gelanguage_id" => "2"],
            ["key" => "NOT_CONSIGNA_DISPONIBLE_LABEL", "value" => "La cuenta no se encuentra abierta, debe realizar la consignacion inicial", "gelanguage_id" => "2"],
            ["key" => "MONTO_INFERIOR_SALDO_LABEL", "value" => "El saldo de la cuenta es menor al monto a retirar", "gelanguage_id" => "2"],
            ["key" => "DIRECCION_LABEL", "value" => "Direccion", "gelanguage_id" => "2"],
            ["key" => "TELEFONO_LABEL", "value" => "Telefono", "gelanguage_id" => "2"],
            ["key" => "IDENTIFICACION_LABEL", "value" => "Identificacion", "gelanguage_id" => "2"],
            ["key" => "TIPO_IDENTIFICACION_LABEL", "value" => "Tipo Identificacion", "gelanguage_id" => "2"],
            ["key" => "ASESOR_LABEL", "value" => "Asesor", "gelanguage_id" => "2"],
            ["key" => "CAJERO_LABEL", "value" => "Cajero", "gelanguage_id" => "2"],
            ["key" => "MONTO_LABEL", "value" => "Monto", "gelanguage_id" => "2"],
            ["key" => "SELECCIONE_CIUDAD_LABEL", "value" => "Seleccione una ciudad", "gelanguage_id" => "2"],
            ["key" => "CAMPO_REQUIRED_LABEL", "value" => "Campo requerido", "gelanguage_id" => "2"],
            ["key" => "ERROR_FORMULARIO_CLIENTE_LABEL", "value" => "Error en el formulario de clientes", "gelanguage_id" => "2"],
            ["key" => "CONFIRMA_ENVIO_INFORMACION_LABEL", "value" => "Confirma envio de informacion?", "gelanguage_id" => "2"],
            ["key" => "SE_REGISTRO_CLIENTE_LABEL", "value" => "Se registro el cliente!!", "gelanguage_id" => "2"],
            ["key" => "SEL_TIPO_IDE_LABEL", "value" => "Seleccione el tipo de identificacion", "gelanguage_id" => "2"],
            ["key" => "SEND_CLIENTE_LABEL", "value" => "Enviar informacion del cliente", "gelanguage_id" => "2"],
            ["key" => "NUEVO_CLIENTE_LABEL", "value" => "Nuevo Cliente", "gelanguage_id" => "2"],
            ["key" => "CONSULTAR_LABEL", "value" => "Consultar", "gelanguage_id" => "2"],
            ["key" => "BUSCANDO_LABEL", "value" => "Buscando", "gelanguage_id" => "2"],
            ["key" => "NO_REGISTROS_LABEL", "value" => "No se encontraron registros", "gelanguage_id" => "2"],
            ["key" => "EMAIL_NOT_VALID_LABEL", "value" => "Email no valido", "gelanguage_id" => "2"],
            ["key" => "MENU_CLIENTES_LABEL", "value" => "Menu clientes", "gelanguage_id" => "2"],
            ["key" => "VENTANA_CLIENTES_NOTIFICACIONES", "value" => "Ventana para la creacion de clientes", "gelanguage_id" => "2"],

        );
        foreach ($newLangDefs as $newLangDef) {
            \App\Models\GE\GelangDef::create($newLangDef);
        }
    }
}
