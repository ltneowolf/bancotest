<?php

use Illuminate\Database\Seeder;

use App\Models\GE\GelangDef;

class ActualizacionTraduccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        echo "Comenzando ejecucion de ".__CLASS__."\n";
        $traducciones = array(

            array(
                'key' => 'SIMBOLO_LABEL',
                'value' => 'Simbolo',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'CODIGO_ISO_LABEL',
                'value' => 'Cod. ISO',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'FACTOR_CONVERSION_LABEL',
                'value' => 'Fac. Conversión',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'AJUSTE_LABEL',
                'value' => 'Ajuste',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'CONVERTIR_MONEDA_LABEL',
                'value' => 'Conv. Moneda',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'ESTADOS_LABEL',
                'value' => 'Estados',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'MODULO_LABEL',
                'value' => 'Módulo',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'MODULOS_LABEL',
                'value' => 'Módulos',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'LABEL',
                'value' => 'Etiqueta',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'VALUE',
                'value' => 'Valor',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'LANGUAGE_LABEL',
                'value' => 'Idioma',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'NOMBRE_PLANTILLA_LABEL',
                'value' => 'Nombre Plantilla',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'ASUNTO_LABEL',
                'value' => 'Asunto',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'CUERPO_MENSAJE_LABEL',
                'value' => 'Cuerpo Mensaje',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'ALERTAS_LABEL',
                'value' => 'Alertas',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'SIEMPRE_CC_LABEL',
                'value' => 'Siempre CC',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'SIEMPRE_CCO_LABEL',
                'value' => 'Siemper CCO',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'IS_USER_LOGGED_LABEL',
                'value' => 'Uusario Logueado',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'TIPO_ALERTA_LABEL',
                'value' => 'Tipo Alerta',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'TIPO_ALERTAS_LABEL',
                'value' => 'Tipos de Alerta',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'VISUALIZAR_LABEL',
                'value' => 'Mostrar',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'VALOR_ARANCEL_LABEL',
                'value' => 'Valor Arancel',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'VALOR_IVA_LABEL',
                'value' => 'Iva',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'TARIFA_IMPUESTO_LABEL',
                'value' => 'Tarifa de Impuesto',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'IDBOX_REMITENTE_LABEL',
                'value' => 'IDBOX Rem.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'NOMBRE_REMITENTE_LABEL',
                'value' => 'Nombre Rem.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'APELLIDO_REMITENTE_LABEL',
                'value' => 'Apellido Rem.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'APELLIDO_DESTINATARIO_LABEL',
                'value' => 'Apellido Dest.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'STATUS_MENU',
                'value' => 'Status',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'STATUS_GUIAS',
                'value' => 'Status a Guias',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'VISIBILIDAD_LABEL',
                'value' => 'Visiblidad',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'COMENTARIO_LABEL',
                'value' => 'Comentario',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'FECHA_STATUS_LABEL',
                'value' => 'Fecha',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'VALOR_REAJUSTE_LABEL',
                'value' => 'Reajuste',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'CONSECUTIVO_DIAN_LABEL',
                'value' => 'Consecutivo DIAN',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'REAJUSTE_LABEL',
                'value' => 'Reajuste',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'CONSECUTIVO_LABEL',
                'value' => 'Consecutivo',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'APLICADO_LABEL',
                'value' => 'Aplicado',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'APLICAR_LIBERACIONES_AUTOMATICAS',
                'value' => 'Ap. Liberaciones',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'LIBERAR_SELECCION',
                'value' => 'Lib. Selección',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'APLICAR_REAJUSTES',
                'value' => 'Ap. Reajustes',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'REAJUSTAR_SELECCION',
                'value' => 'Reajustar Sel.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'NOTIFICAR_LIBERACIONES',
                'value' => 'Notif. L',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'NOTIFICAR_REAJUSTES',
                'value' => 'Notif. R',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'SERVICIO_LABEL',
                'value' => 'Servicio',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'IMPUESTOS_PAGADOS_LABEL',
                'value' => 'Imp.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'DECLARADO_LABEL',
                'value' => 'Dec.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'DIFERENCIA_LABEL',
                'value' => 'Dif.',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'TOTALPAGAR_LABEL',
                'value' => 'Tot. pagar',
                'gelanguage_id' => '2',
            ),array(
                'key' => 'RECOLECCION',
                'value' => 'Recolección',
                'gelanguage_id' => '2',
            )
        );

        foreach ($traducciones as $trad){
            GelangDef::where('key', $trad['key'])->delete();
            GelangDef::create($trad);
        }
    }
}
