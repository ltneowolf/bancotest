<?php

use Illuminate\Database\Seeder;

class AppExecSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocalizacionSeeder::class);
        $this->call(ActualizacionTraduccionesSeeder::class);
        $this->call(EditarUserSTMPSeeder::class);
        $this->call(GelogsSedder::class);
        $this->call(BancoTest_08_03_2020_Seeder::class);
    }
}
