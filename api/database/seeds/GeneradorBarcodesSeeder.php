<?php

use Illuminate\Database\Seeder;

class GeneradorBarcodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Comenzando ejecucion de ".__CLASS__."\n";
        echo "Creando enpoint para generar barcodes \n";
        $barcode = \App\Models\SE\Seendpoint::create(["state" => "barcode", "menu_label" => "BARCODE_LABEL", "show" => "0", "page_desc" => "enpoint para generar diferentes tipos de barcode", "seendpoint_id" => ""]);
        \App\Models\SE\SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $barcode->id, "serole_id" => "1"]);
    }
}
