<?php

use Illuminate\Database\Seeder;

class EditarUserSTMPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Comenzando ejecucion de " . __CLASS__ . "\n";


        $modulo = \App\Models\GE\Gemodulo::where('nombre', 'GENERAL')->first();

        echo "Creando rol Usuario\n";
        $roleUsuario = \App\Models\SE\Serole::firstOrCreate(['name' => 'User']);

        echo "Creando GEPARAMETERS para el rol Usuario\n";
        $parameter = array(
            "key" => "ROL_USER",
            "numval" => $roleUsuario->id,
            "observation" => "Id del Rol Usuario",
            "gemodulo_id" => $modulo->id
        );
        \App\Models\GE\Geparameters::firstOrCreate($parameter);

        /********************************************************************************************************/

        echo "Obteniendo Id del menu General " . __LINE__ . " \n";
        $generalEndpoint = \App\Models\SE\Seendpoint::where('menu_label', '=', 'GENERAL_LABEL')->get()->first();

        echo "Generando endpoint de Edicion de Perfil " . __LINE__ . " \n";
        $newEndpoint = \App\Models\SE\Seendpoint::create([
            "state" => "editarperfil",
            'page_title' => 'EDIT_PERFIL_LABEL',
            'menu_label' => 'EDIT_PERFIL_LABEL',
            "show" => "0",
            "page_desc" => "Endpoint para la edicion del perfil",
            "seendpoint_id" => $generalEndpoint->id]);

        echo "Ejecutando Traducciones" . __LINE__ . "\n";

        \App\Models\SE\SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $newEndpoint->id, "serole_id" => "1"]);
        \App\Models\SE\SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $newEndpoint->id, "serole_id" => $roleUsuario->id]);

        echo "Generando endpoint para cambio de Password " . __LINE__ . " \n";
        $newEndpoint = \App\Models\SE\Seendpoint::create([
            "state" => "changepassword",
            'page_title' => 'CHNG_PASS_LABEL',
            'menu_label' => 'CHNG_PASS_LABEL',
            "show" => "0",
            "page_desc" => "Endpoint para cambio de Password",
            "seendpoint_id" => $generalEndpoint->id]);

        echo "Ejecutando Traducciones" . __LINE__ . "\n";

        \App\Models\SE\SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $newEndpoint->id, "serole_id" => "1"]);
        \App\Models\SE\SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $newEndpoint->id, "serole_id" => $roleUsuario->id]);

        echo "Generando endpoint para cambio de Password " . __LINE__ . " \n";
        $newEndpoint = \App\Models\SE\Seendpoint::create([
            "state" => "changepassword",
            'page_title' => 'CHNG_PASS_LABEL',
            'menu_label' => 'CHNG_PASS_LABEL',
            "show" => "0",
            "page_desc" => "Endpoint para cambio de Password",
            "seendpoint_id" => $generalEndpoint->id]);

        echo "Ejecutando Traducciones" . __LINE__ . "\n";

        \App\Models\SE\SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $newEndpoint->id, "serole_id" => "1"]);
        \App\Models\SE\SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $newEndpoint->id, "serole_id" => $roleUsuario->id]);

        echo "Generando endpoint para envio de correo de pruebas " . __LINE__ . " \n";
        $newEndpoint = \App\Models\SE\Seendpoint::create([
            "state" => "emailprueba",
            'page_title' => 'EMAIL_PRUEBA_LABEL',
            'menu_label' => 'EMAIL_PRUEBA_LABEL',
            "show" => "0",
            "page_desc" => "Endpoint envio de correo de pruebas",
            "seendpoint_id" => $generalEndpoint->id]);

        \App\Models\SE\SeendpointMethod::create(["method" => "POST", "seendpoint_id" => $newEndpoint->id, "serole_id" => "1"]);
        \App\Models\SE\SeendpointMethod::create(["method" => "POST", "seendpoint_id" => $newEndpoint->id, "serole_id" => $roleUsuario->id]);

        echo "Se agrega el perimiso a los endpoitns users y dashboard\n";
        $endpointDash = \App\Models\SE\Seendpoint::where('state', 'dashboard')->first();
        \App\Models\SE\SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endpointDash->id,
            "serole_id" => $roleUsuario->id]);

        $endpointUser = \App\Models\SE\Seendpoint::where('state', 'users')->first();
        \App\Models\SE\SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endpointUser->id,
            "serole_id" => $roleUsuario->id]);
        \App\Models\SE\SeendpointMethod::create(["method" => "PUT", "seendpoint_id" => $endpointUser->id,
            "serole_id" => $roleUsuario->id]);


        echo "Ejecutando Traducciones" . __LINE__ . "\n";

        $newLangDefs = array(
            /***********************************************************************************************************/
            ["key" => "EDIT_PERFIL_LABEL", "value" => "Editar Perfil", "gelanguage_id" => "2"],
            ["key" => "CONF_SMTP_LABEL", "value" => "Configuracion SMTP", "gelanguage_id" => "2"],
            ["key" => "CONF_SMTP_GEN_LABEL", "value" => "Configuracion SMTP", "gelanguage_id" => "2"],
            ["key" => "USER_SMTP_LABEL", "value" => "Usuario", "gelanguage_id" => "2"],
            ["key" => "PASS_SMTP_LABEL", "value" => "Password", "gelanguage_id" => "2"],
            ["key" => "REM_EMAIL_SMTP_LABEL", "value" => "Remitente", "gelanguage_id" => "2"],
            ["key" => "REM_NOMBRE_SMTP_LABEL", "value" => "De", "gelanguage_id" => "2"],
            ["key" => "AUTEN_SMTP_LABEL", "value" => "Requiere Autenticacion", "gelanguage_id" => "2"],
            ["key" => "ACTIVA_SMTP_LABEL", "value" => "Activar", "gelanguage_id" => "2"],
            ["key" => "HOST_SMTP_LABEL", "value" => "Host", "gelanguage_id" => "2"],
            ["key" => "PORT_SMTP_LABEL", "value" => "Puerto", "gelanguage_id" => "2"],
            ["key" => "ENCRYP_SMTP_LABEL", "value" => "Encriptacion", "gelanguage_id" => "2"],
            ["key" => "NEW_PASSWORD_FIELD_LABEL", "value" => "Nueva Contraseña", "gelanguage_id" => "2"],
            ["key" => "REPIT_NEW_PASS_FIELD_LABEL", "value" => "Repita Nueva Contraseña", "gelanguage_id" => "2"],
            ["key" => "CAMBIAR_PASSWORD", "value" => "Cambiar Contraseña", "gelanguage_id" => "2"],
            ["key" => "FIELD_REQUIRED", "value" => "Este Campo es Requerido", "gelanguage_id" => "2"],
            ["key" => "NEW_PASS_ERROR", "value" => "Las contraseñas no son Iguales", "gelanguage_id" => "2"],
            ["key" => "CLAVE_AGENTE_LABEL", "value" => "Clave Agente", "gelanguage_id" => "2"],
            ["key" => "CLAVE_AGENCIA_LABEL", "value" => "Clave Agencia Pagos", "gelanguage_id" => "2"],
            ["key" => "PASS_UPDATE_OK_LABEL", "value" => "Contraseña Actualizada Exitosamente", "gelanguage_id" => "2"],
            ["key" => "PASS_NOT_VALID", "value" => "Contraseña invalida", "gelanguage_id" => "2"],
            ["key" => "INFO_USER_LABEL", "value" => "Informacion Usuario", "gelanguage_id" => "2"],
            ["key" => "CLAVE_USER_LABEL", "value" => "Clave Usuario", "gelanguage_id" => "2"],
            ["key" => "EMAIL_FIELD_INVALID", "value" => "Formato de Email Incorrecto", "gelanguage_id" => "2"],
            ["key" => "INFO_EDIT_LABEL", "value" => "Informacio Editada Exitosamente", "gelanguage_id" => "2"],
            ["key" => "CONF_SMTP_EXITO_LABEL", "value" => "SMTP Configurado Exitosamente", "gelanguage_id" => "2"],
            ["key" => "SAVE_LABEL", "value" => "Guardar", "gelanguage_id" => "2"],
            ["key" => "EMAIL_LABEL", "value" => "Email", "gelanguage_id" => "2"],
            ["key" => "NONE_LABEL", "value" => "Ninguno", "gelanguage_id" => "2"],
            ["key" => "HELP_NOM_SMTP_LABEL", "value" => "Si este campo no se digita se utilizara el nombre del usuario como remitente", "gelanguage_id" => "2"],
            ["key" => "ACT_SMTP_LABEL", "value" => "Activar SMTP", "gelanguage_id" => "2"],
            ["key" => "EMAIL_PRUEBA_LABEL", "value" => "Email de Prueba", "gelanguage_id" => "2"],
            ["key" => "SEND_EMAIL_PRUEBA_LABEL", "value" => "Enviar Email de Prueba", "gelanguage_id" => "2"],
            ["key" => "SEND_LABEL", "value" => "Enviar", "gelanguage_id" => "2"],
            ["key" => "CANCELAR_LABEL", "value" => "Cancelar", "gelanguage_id" => "2"],
            ["key" => "DESTINATARIO_LABEL", "value" => "Destinatario", "gelanguage_id" => "2"],
            ["key" => "CONF_SMTP_NOT_VALID", "value" => "Configuracion SMTP No Valida", "gelanguage_id" => "2"],
            ["key" => "EMAIL_EXITO_LABEL", "value" => "Envio de correo Exitoso", "gelanguage_id" => "2"],
            /***********************************************************************************************************/
        );

        foreach ($newLangDefs as $newLangDef) {
            \App\Models\GE\GelangDef::firstOrCreate($newLangDef);
        }


    }
}
