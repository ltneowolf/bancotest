<?php

use App\Models\GE\Geciudade;
use App\Models\GE\Gedepartamento;
use App\Models\GE\Gepaise;
use App\Models\GE\Gezona;
use App\Models\SER\Sertariseguro;
use Illuminate\Database\Seeder;

class LocalizacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Comenzando ejecucion de ".__CLASS__."\n";

       /* echo "Creando Zonas\n";
        $munzona = Gezona::create(["nombre" => "Zona Mundo"]);*/

        echo "Creando Paises\n";
        $paises = array(["nombre" => "Belice", "abreviatura" => "BLZ"],
            ["nombre" => "Colombia", "abreviatura" => "COL"],
            ["nombre" => "Costa Rica", "abreviatura" => "CRI"],
            ["nombre" => "El Salvador", "abreviatura" => "SLV"],
            ["nombre" => "Guatemala", "abreviatura" => "GTM"],
            ["nombre" => "Honduras", "abreviatura" => "HND"],
            ["nombre" => "Nicaragua", "abreviatura" => "NIC"],
            ["nombre" => "Panamá", "abreviatura" => "PAN"],
            ["nombre" => "Bahamas", "abreviatura" => "BHS"],
            ["nombre" => "Cuba", "abreviatura" => "CUB"],
            ["nombre" => "Haití", "abreviatura" => "HTI"],
            ["nombre" => "Jamaica", "abreviatura" => "JAM"],
            ["nombre" => "México", "abreviatura" => "MEX"],
            ["nombre" => "República Dominicana", "abreviatura" => "DOM"],
            ["nombre" => "Puerto Rico", "abreviatura" => "PRI"],
            ["nombre" => "Venezuela", "abreviatura" => "VEN"],
            ["nombre" => "Estados Unidos de América", "abreviatura" => "USA"],);

        foreach ($paises as $paise) {
            Gepaise::create($paise);
        }

        echo "Creando departamentos\n";
        $deptos = array(["nombre" => "Amazonas", "gepaise_id" => "2"],
            ["nombre" => "Cauca", "gepaise_id" => "2"],
            ["nombre" => "Meta", "gepaise_id" => "2"],
            ["nombre" => "Vaupes", "gepaise_id" => "2"],
            ["nombre" => "Antioquia", "gepaise_id" => "2"],
            ["nombre" => "Cesar", "gepaise_id" => "2"],
            ["nombre" => "Nariño", "gepaise_id" => "2"],
            ["nombre" => "Vichada", "gepaise_id" => "2"],
            ["nombre" => "Arauca", "gepaise_id" => "2"],
            ["nombre" => "Córdoba", "gepaise_id" => "2"],
            ["nombre" => "Norte de santander", "gepaise_id" => "2"],
            ["nombre" => "Archipielago de san andrés", "gepaise_id" => "2"],
            ["nombre" => "Chocó", "gepaise_id" => "2"],
            ["nombre" => "Putumayo", "gepaise_id" => "2"],
            ["nombre" => "Atlántico", "gepaise_id" => "2"],
            ["nombre" => "Cundinamarca", "gepaise_id" => "2"],
            ["nombre" => "Quindío", "gepaise_id" => "2"],
            ["nombre" => "Bolívar", "gepaise_id" => "2"],
            ["nombre" => "Guainia", "gepaise_id" => "2"],
            ["nombre" => "Risaralda", "gepaise_id" => "2"],
            ["nombre" => "Boyacá", "gepaise_id" => "2"],
            ["nombre" => "Guaviare", "gepaise_id" => "2"],
            ["nombre" => "Santander", "gepaise_id" => "2"],
            ["nombre" => "Caldas", "gepaise_id" => "2"],
            ["nombre" => "Huila", "gepaise_id" => "2"],
            ["nombre" => "Sucre", "gepaise_id" => "2"],
            ["nombre" => "Caquetá", "gepaise_id" => "2"],
            ["nombre" => "La guajira", "gepaise_id" => "2"],
            ["nombre" => "Tolima", "gepaise_id" => "2"],
            ["nombre" => "Casanare", "gepaise_id" => "2"],
            ["nombre" => "Magdalena", "gepaise_id" => "2"],
            ["nombre" => "Valle del cauca", "gepaise_id" => "2"]);
        foreach ($deptos as $depto) {
            Gedepartamento::create($depto);
        }

        echo "Creando ciudades\n";
        $ciudades = array(["nombre" => "Buenaventura", "gedepartamento_id" => "32"],
            ["nombre" => "Buga", "gedepartamento_id" => "32"],
            ["nombre" => "Cali", "gedepartamento_id" => "32"],
            ["nombre" => "Candelaria", "gedepartamento_id" => "32"],
            ["nombre" => "Cartago", "gedepartamento_id" => "32"],
            ["nombre" => "Calima El Darién", "gedepartamento_id" => "32"],
            ["nombre" => "El Cerrito", "gedepartamento_id" => "32"],
            ["nombre" => "Florida", "gedepartamento_id" => "32"],
            ["nombre" => "Guacarí", "gedepartamento_id" => "32"],
            ["nombre" => "Jamundí", "gedepartamento_id" => "32"],
            ["nombre" => "Palmira", "gedepartamento_id" => "32"],
            ["nombre" => "Tuluá", "gedepartamento_id" => "32"],
            ["nombre" => "Yumbo", "gedepartamento_id" => "32"],
            ["nombre" => "Zarzal", "gedepartamento_id" => "32"],
            ["nombre" => "Medellín", "gedepartamento_id" => "5"],
            ["nombre" => "Itagüí", "gedepartamento_id" => "5"],
            ["nombre" => "Envigado", "gedepartamento_id" => "5"],
            ["nombre" => "Tarazá", "gedepartamento_id" => "5"],
            ["nombre" => "Zaragoza", "gedepartamento_id" => "5"],
            ["nombre" => "Bogotá", "gedepartamento_id" => "16"],
            ["nombre" => "Soacha", "gedepartamento_id" => "16"],
            ["nombre" => "Fusagasugá", "gedepartamento_id" => "16"],
            ["nombre" => "Chía", "gedepartamento_id" => "16"]
        );
        foreach ($ciudades as $ciudad) {
            Geciudade::create($ciudad);
        }
    }
}
