<?php

use App\Models\GE\Gemodulo;
use App\Models\GE\Geparameters;
use App\Models\GE\Gemoneda;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\SE\SeuserRole;
use App\Models\SE\Serole;
use App\Models\GE\Gelanguage;
use App\Models\GE\GelangDef;
use App\Models\SE\Seendpoint;
use App\Models\SE\SeendpointMethod;
use App\Models\GE\Gepaise;
use App\Models\GE\Gedepartamento;
use App\Models\GE\Geciudade;

class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Comenzando ejecucion de " . __CLASS__ . "\n";
        echo "Creando usuario Admin\n";
        $user = ['name' => 'Control Admin', 'email' => 'useradmin@test.com', 'password' => 'admin'];

        $user = User::create($user);

        echo "Creando rol Admin\n";
        $role = ['name' => 'Admin'];

        $role = Serole::create($role);

        echo "Creando user roles\n";

        $userRole = ['user_id' => $user->id, 'serole_id' => $role->id];
        SeuserRole::create($userRole);

        echo "Creando el usuario Log Control " . __LINE__ . "\n";
        $user = ['name' => 'Log control', 'email' => 'log@control.com', 'password' => 'controlLog'];

        $userLogControl = User::create($user);

        echo "Creando lenguajes\n";
        $langs = array(
            ['id' => 1, 'lang' => 'en', 'label' => 'English us', 'icon' => 'us'],
            ['id' => 2, 'lang' => 'es', 'label' => 'Español es', 'icon' => 'es'],
            ['id' => 3, 'lang' => '从h', 'label' => '中文  cn', 'icon' => 'cn']);

        foreach ($langs as $lang) {
            Gelanguage::create($lang);
        }

        echo "Creando traducciones \n";
        $langDefs = array(["key" => "EDIT_LABEL", "value" => "Edit", "gelanguage_id" => "1"],
            ["key" => "EDIT_LABEL", "value" => "Editar", "gelanguage_id" => "2"],
            ["key" => "METHOD_NOT_ALLOWED_LABEL", "value" => "Metodo no Permitido", "gelanguage_id" => "2"],
            ["key" => "FORGOT_PASSWORD_LABEL", "value" => "Olvidé mi contraseña", "gelanguage_id" => "2"],
            ["key" => "RECORDAR_PASS_LABEL", "value" => "Recordar contraseña", "gelanguage_id" => "2"],
            ["key" => "DIGITE_EMAIL_PARA_RECORDAR_LABEL", "value" => "Digite su correo electrónico para recordar la contraseña",
                "gelanguage_id" => "2"],
            ["key" => "EDIT_LABEL", "value" => "編輯", "gelanguage_id" => "3"],
            ["key" => "DIAS_LIBRES_DEVOLUCION_LABEL", "value" => "Dias Libres Devolución", "gelanguage_id" => "2"],
            ["key" => "SELECT_FILE_LABEL", "value" => "Select File", "gelanguage_id" => "1"],
            ["key" => "SELECT_FILE_LABEL", "value" => "Seleccionar Archivo", "gelanguage_id" => "2"],
            ["key" => "CHANGE_LABEL", "value" => "Change", "gelanguage_id" => "1"],
            ["key" => "CHANGE_LABEL", "value" => "Cambiar", "gelanguage_id" => "2"],
            ["key" => "Add_Quotation_Label", "value" => "Add Quotation", "gelanguage_id" => "1"],
            ["key" => "Add_Quotation_Label", "value" => "Agregar Cotización", "gelanguage_id" => "2"],
            ["key" => "INTERNAL_ERROR_CONTACT_ADMIN", "value" => "Error interno, por favor contacte al administador",
                "gelanguage_id" => "2"],
            ["key" => "OBSERVACION_LABEL", "value" => "Observacion", "gelanguage_id" => "2"],
            ["key" => "OBSERVACION_LABEL", "value" => "Observation", "gelanguage_id" => "1"],
            ["key" => "NUM_VAL_LABEL", "value" => "Valor Numerico", "gelanguage_id" => "2"],
            ["key" => "NUM_VAL_LABEL", "value" => "Numeric Value", "gelanguage_id" => "1"],
            ["key" => "KEY_LABEL", "value" => "Key", "gelanguage_id" => "1"],
            ["key" => "KEY_LABEL", "value" => "Llave", "gelanguage_id" => "2"],
            ["key" => "GEPARAMETERS_LABEL", "value" => "Parametros del Sistema", "gelanguage_id" => "2"],
            ["key" => "GEPARAMETERS_LABEL", "value" => "System Parameters", "gelanguage_id" => "1"],
            ["key" => "STORED_TARIFFS", "value" => "Tarifas creadas", "gelanguage_id" => "2"],
            ["key" => "STORED_TARIFFS", "value" => "Stored tariffs", "gelanguage_id" => "1"],
            ["key" => "RECHAZAR_LABEL", "value" => "Refuse", "gelanguage_id" => "1"],
            ["key" => "YES_LABEL", "value" => "Yes", "gelanguage_id" => "1"],
            ["key" => "YES_LABEL", "value" => "Si", "gelanguage_id" => "2"],
            ["key" => "NO_LABEL", "value" => "Not", "gelanguage_id" => "1"],
            ["key" => "NO_LABEL", "value" => "No", "gelanguage_id" => "2"],
            ["key" => "EDITAR_COTIZACION_LABEL", "value" => "Editar Cotización", "gelanguage_id" => "2"],
            ["key" => "EDITAR_COTIZACION_LABEL", "value" => "Edit Quotation", "gelanguage_id" => "1"],
            ["key" => "INVALID_MIME_TYPE", "value" => "Tipo de Archivo Invalido", "gelanguage_id" => "2"],
            ["key" => "INVALID_MIME_TYPE", "value" => "Wrong File", "gelanguage_id" => "1"],
            ["key" => "VALOR_VENTA_LABEL", "value" => "Selling Value", "gelanguage_id" => "1"],
            ["key" => "VALOR_VENTA_LABEL", "value" => "Valor Venta", "gelanguage_id" => "2"],
            ["key" => "CANTIDAD_MINIMA_LABEL", "value" => "Min. Quantity", "gelanguage_id" => "1"],
            ["key" => "CANTIDAD_MINIMA_LABEL", "value" => "Cantidad Minima", "gelanguage_id" => "2"],
            ["key" => "EMAIL_FIELD_LABEL", "value" => "Email", "gelanguage_id" => "1"],
            ["key" => "EMAIL_FIELD_INVALID", "value" => "Wrong Email Format", "gelanguage_id" => "1"],
            ["key" => "EMAIL_FIELD_REQUIRED", "value" => "Email Required", "gelanguage_id" => "1"],
            ["key" => "PASSWORD_FIELD_LABEL", "value" => "Password", "gelanguage_id" => "1"],
            ["key" => "PASSWORD_FIELD_REQUIRED", "value" => "Password Required", "gelanguage_id" => "1"],
            ["key" => "LOGIN_BTN_LABEL", "value" => "Login", "gelanguage_id" => "1"],
            ["key" => "INVALID_CREDENTIALS", "value" => "Wrong Email or Password", "gelanguage_id" => "1"],
            ["key" => "FIELD_REQUIRED", "value" => "This Field is Required", "gelanguage_id" => "1"],
            ["key" => "SEARCH_PLACEHOLDER", "value" => "Search...", "gelanguage_id" => "1"],
            ["key" => "EMAIL_FIELD_LABEL", "value" => "Email", "gelanguage_id" => "2"],
            ["key" => "EMAIL_FIELD_INVALID", "value" => "Formato de Email Incorrecto", "gelanguage_id" => "2"],
            ["key" => "EMAIL_FIELD_REQUIRED", "value" => "Email requerido", "gelanguage_id" => "2"],
            ["key" => "PASSWORD_FIELD_LABEL", "value" => "Contraseña", "gelanguage_id" => "2"],
            ["key" => "PASSWORD_FIELD_REQUIRED", "value" => "Contraseña requerida", "gelanguage_id" => "2"],
            ["key" => "LOGIN_BTN_LABEL", "value" => "Iniciar Sesión", "gelanguage_id" => "2"],
            ["key" => "INVALID_CREDENTIALS", "value" => "Usuario o Contraseña Incorrecta", "gelanguage_id" => "2"],
            ["key" => "FIELD_REQUIRED", "value" => "Este Campo es Requerido", "gelanguage_id" => "2"],
            ["key" => "SEARCH_PLACEHOLDER", "value" => "Buscar...", "gelanguage_id" => "2"],
            ["key" => "EMAIL_FIELD_LABEL", "value" => "电子邮件", "gelanguage_id" => "3"],
            ["key" => "EMAIL_FIELD_INVALID", "value" => "错误的电子邮件格式", "gelanguage_id" => "3"],
            ["key" => "EMAIL_FIELD_REQUIRED", "value" => "电子邮件要求", "gelanguage_id" => "3"],
            ["key" => "PASSWORD_FIELD_LABEL", "value" => "密码", "gelanguage_id" => "3"],
            ["key" => "PASSWORD_FIELD_REQUIRED", "value" => "需要密码", "gelanguage_id" => "3"],
            ["key" => "LOGIN_BTN_LABEL", "value" => "登录", "gelanguage_id" => "3"],
            ["key" => "INVALID_CREDENTIALS", "value" => "错误的电子邮件地址或密码", "gelanguage_id" => "3"],
            ["key" => "FIELD_REQUIRED", "value" => "这是必填栏", "gelanguage_id" => "3"],
            ["key" => "SEARCH_PLACEHOLDER", "value" => "搜索...", "gelanguage_id" => "3"],
            ["key" => "LOGOUT_BTN_LABEL", "value" => "Log Out", "gelanguage_id" => "1"],
            ["key" => "LOGOUT_BTN_LABEL", "value" => "Cerrar Sesión", "gelanguage_id" => "2"],
            ["key" => "LOGOUT_BTN_LABEL", "value" => "登出", "gelanguage_id" => "3"],
            ["key" => "CIUDADES_LABEL", "value" => "Cities", "gelanguage_id" => "1"],
            ["key" => "CIUDADES_LABEL", "value" => "Ciudades", "gelanguage_id" => "2"],
            ["key" => "CIUDADES_LABEL", "value" => "城市", "gelanguage_id" => "3"],
            ["key" => "CLIENTES_LABEL", "value" => "Customers", "gelanguage_id" => "1"],
            ["key" => "CLIENTES_LABEL", "value" => "Clientes", "gelanguage_id" => "2"],
            ["key" => "CLIENTES_LABEL", "value" => "客戶", "gelanguage_id" => "3"],
            ["key" => "CONCEPTOS_LABEL", "value" => "Concepts", "gelanguage_id" => "1"],
            ["key" => "CONCEPTOS_LABEL", "value" => "Conceptos", "gelanguage_id" => "2"],
            ["key" => "CONCEPTOS_LABEL", "value" => "概念", "gelanguage_id" => "3"],
            ["key" => "CONTACTOS_LABEL", "value" => "Contacts", "gelanguage_id" => "1"],
            ["key" => "CONTACTOS_LABEL", "value" => "Cotactos", "gelanguage_id" => "2"],
            ["key" => "CONTACTOS_LABEL", "value" => "聯繫", "gelanguage_id" => "3"],
            ["key" => "DEPARTAMENTOS_LABEL", "value" => "Departments", "gelanguage_id" => "1"],
            ["key" => "DEPARTAMENTOS_LABEL", "value" => "Departamentos", "gelanguage_id" => "2"],
            ["key" => "DEPARTAMENTOS_LABEL", "value" => "部門", "gelanguage_id" => "3"],
            ["key" => "INCOTERMS_LABEL", "value" => "Incoterms", "gelanguage_id" => "1"],
            ["key" => "INCOTERMS_LABEL", "value" => "Incoterms", "gelanguage_id" => "2"],
            ["key" => "INCOTERMS_LABEL", "value" => "Incoterms", "gelanguage_id" => "3"],
            ["key" => "LANG_LABEL", "value" => "Language", "gelanguage_id" => "1"],
            ["key" => "LANG_LABEL", "value" => "Idioma", "gelanguage_id" => "2"],
            ["key" => "LANG_LABEL", "value" => "語言", "gelanguage_id" => "3"],
            ["key" => "CURRENCY_LABEL", "value" => "Currency", "gelanguage_id" => "1"],
            ["key" => "CURRENCY_LABEL", "value" => "Moneda", "gelanguage_id" => "2"],
            ["key" => "CURRENCY_LABEL", "value" => "貨幣", "gelanguage_id" => "3"],
            ["key" => "COUNTRIES_LABEL", "value" => "Countries", "gelanguage_id" => "1"],
            ["key" => "COUNTRIES_LABEL", "value" => "Paises", "gelanguage_id" => "2"],
            ["key" => "COUNTRIES_LABEL", "value" => "國家", "gelanguage_id" => "3"],
            ["key" => "PROVIDERS_LABEL", "value" => "Providers", "gelanguage_id" => "1"],
            ["key" => "PROVIDERS_LABEL", "value" => "Proveedores", "gelanguage_id" => "2"],
            ["key" => "PROVIDERS_LABEL", "value" => "提供商", "gelanguage_id" => "3"],
            ["key" => "TARIFF_LABEL", "value" => "Tariff", "gelanguage_id" => "1"],
            ["key" => "TARIFF_LABEL", "value" => "Tarifa", "gelanguage_id" => "2"],
            ["key" => "TARIFF_LABEL", "value" => "關稅", "gelanguage_id" => "3"],
            ["key" => "TERMINAL_LABEL", "value" => "Terminal", "gelanguage_id" => "1"],
            ["key" => "TERMINAL_LABEL", "value" => "Terminal", "gelanguage_id" => "2"],
            ["key" => "TERMINAL_LABEL", "value" => "終奌站", "gelanguage_id" => "3"],
            ["key" => "SERVICES_TYPE_LABEL", "value" => "Services Type", "gelanguage_id" => "1"],
            ["key" => "SERVICES_TYPE_LABEL", "value" => "Tipo  de Servicio", "gelanguage_id" => "2"],
            ["key" => "SERVICES_TYPE_LABEL", "value" => "服務型", "gelanguage_id" => "3"],
            ["key" => "USER_ROLES_LABEL", "value" => "User Roles", "gelanguage_id" => "1"],
            ["key" => "USER_ROLES_LABEL", "value" => "Roles de Usuario", "gelanguage_id" => "2"],
            ["key" => "USER_ROLES_LABEL", "value" => "用戶角色", "gelanguage_id" => "3"],
            ["key" => "VALIDATE_LABEL", "value" => "Validate", "gelanguage_id" => "1"],
            ["key" => "VALIDATE_LABEL", "value" => "Validar", "gelanguage_id" => "2"],
            ["key" => "VALIDATE_LABEL", "value" => "驗證", "gelanguage_id" => "3"],
            ["key" => "SOLICITUDES_REALIZADAS_LABEL", "value" => "Solicitudes Realizadas", "gelanguage_id" => "2"],
            ["key" => "SOLICITUDES_REALIZADAS_LABEL", "value" => "Realized Applications", "gelanguage_id" => "1"],
            ["key" => "SOLICITUDES_REALIZADAS_LABEL", "value" => "实现应用程序", "gelanguage_id" => "3"],
            ["key" => "SOLICITUD_COTIZACION_LABEL", "value" => "Solicitud de Cotización", "gelanguage_id" => "2"],
            ["key" => "SOLICITUD_COTIZACION_LABEL", "value" => "詢價", "gelanguage_id" => "3"],
            ["key" => "SOLICITUD_COTIZACION_LABEL", "value" => "Call for Pricing", "gelanguage_id" => "1"],
            ["key" => "CLIENTE_LABEL", "value" => "Client", "gelanguage_id" => "1"],
            ["key" => "TIPO_MOVIMIENTO_LABEL", "value" => "Movement Type", "gelanguage_id" => "1"],
            ["key" => "MODALIDAD_SERV_LABEL", "value" => "Modalidad Servicio", "gelanguage_id" => "2"],
            ["key" => "MODALIDAD_SERV_LABEL", "value" => "Service Modality", "gelanguage_id" => "1"],
            ["key" => "GUARDAR_LABEL", "value" => "Save", "gelanguage_id" => "1"],
            ["key" => "GUARDAR_LABEL", "value" => "Guardar", "gelanguage_id" => "2"],
            ["key" => "CANCELAR_LABEL", "value" => "Cancel", "gelanguage_id" => "1"],
            ["key" => "CANCELAR_LABEL", "value" => "Cancelar", "gelanguage_id" => "2"],
            ["key" => "REQ_CUSTOMS", "value" => "Req. Customs", "gelanguage_id" => "1"],
            ["key" => "REQ_CUSTOMS", "value" => "Req. Customs", "gelanguage_id" => "2"],
            ["key" => "REQ_INLAND_LABEL", "value" => "Req. in Land", "gelanguage_id" => "1"],
            ["key" => "REQ_INLAND_LABEL", "value" => "Req. in Land", "gelanguage_id" => "2"],
            ["key" => "REQ_INLAND_LABEL", "value" => "Req. in Land", "gelanguage_id" => "3"],
            ["key" => "CLOSE_LABEL", "value" => "Cerrar", "gelanguage_id" => "2"],
            ["key" => "CLOSE_LABEL", "value" => "Close", "gelanguage_id" => "1"],
            ["key" => "PROVIDER_LABEL", "value" => "Proveedor", "gelanguage_id" => "2"],
            ["key" => "PROVIDER_LABEL", "value" => "Provider", "gelanguage_id" => "1"],
            ["key" => "ESP_TECNICA_LABEL", "value" => "Technical Spec.", "gelanguage_id" => "1"],
            ["key" => "ESP_TECNICA_LABEL", "value" => "Esp. Técnica", "gelanguage_id" => "2"],
            ["key" => "QUANTITY_LABEL", "value" => "Quantity", "gelanguage_id" => "1"],
            ["key" => "QUANTITY_LABEL", "value" => "Cantidad", "gelanguage_id" => "2"],
            ["key" => "ORIGEN_LABEL", "value" => "Origen", "gelanguage_id" => "2"],
            ["key" => "ORIGEN_LABEL", "value" => "Origin", "gelanguage_id" => "1"],
            ["key" => "DESTINO_LABEL", "value" => "Destination", "gelanguage_id" => "1"],
            ["key" => "DESTINO_LABEL", "value" => "Destino", "gelanguage_id" => "2"],
            ["key" => "FECHA_HORA_LABEL", "value" => "Date Receipt", "gelanguage_id" => "1"],
            ["key" => "UNI_EMP_LABEL", "value" => "Uni/Emp", "gelanguage_id" => "2"],
            ["key" => "UNI_EMP_LABEL", "value" => "Uni/Pack", "gelanguage_id" => "1"],
            ["key" => "REQ_EMBARQUE_LABEL", "value" => "Req. Embarque", "gelanguage_id" => "2"],
            ["key" => "REQ_TERRESTRE", "value" => "Req. Land", "gelanguage_id" => "1"],
            ["key" => "TERMINALES_LABEL", "value" => "Terminals", "gelanguage_id" => "1"],
            ["key" => "WEB_PAGE_LABEL", "value" => "Pagina Web", "gelanguage_id" => "2"],
            ["key" => "CIUDAD_ORIGEN_LABEL", "value" => "Ciudad Origen", "gelanguage_id" => "2"],
            ["key" => "CIUDAD_ORIGEN_LABEL", "value" => "Hometown", "gelanguage_id" => "1"],
            ["key" => "ROW_LABEL", "value" => "Row", "gelanguage_id" => "1"],
            ["key" => "VALUE_LABEL", "value" => "Valor", "gelanguage_id" => "2"],
            ["key" => "VALUE_LABEL", "value" => "Value", "gelanguage_id" => "1"],
            ["key" => "MESSAGE_LABEL", "value" => "Mensaje", "gelanguage_id" => "2"],
            ["key" => "MESSAGE_LABEL", "value" => "Message", "gelanguage_id" => "1"],
            ["key" => "UTILIDAD_LABEL", "value" => "Utilidad", "gelanguage_id" => "2"],
            ["key" => "UTILIDAD_LABEL", "value" => "Profit", "gelanguage_id" => "1"],
            ["key" => "USER_LABEL", "value" => "User", "gelanguage_id" => "1"],
            ["key" => "DASHBOARD_LABEL", "value" => "DASHBOARD", "gelanguage_id" => "1"],
            ["key" => "DASHBOARD_LABEL", "value" => "DASHBOARD", "gelanguage_id" => "2"],
            ["key" => "PRICING_LABEL", "value" => "Pricing", "gelanguage_id" => "2"],
            ["key" => "PRICING_LABEL", "value" => "Pricing", "gelanguage_id" => "1"],
            ["key" => "VENTA_MINIMA_LABEL", "value" => "Venta Minima", "gelanguage_id" => "2"],
            ["key" => "VENTA_MINIMA_LABEL", "value" => "Sale Min.", "gelanguage_id" => "1"],
            ["key" => "FECHA_VIGENCIA", "value" => "Fecha Vigencia", "gelanguage_id" => "2"],
            ["key" => "VALOR_LABEL", "value" => "Valor", "gelanguage_id" => "2"],
            ["key" => "FECHA_VIGENCIA", "value" => "Effective Date", "gelanguage_id" => "1"],
            ["key" => "VALOR_LABEL", "value" => "Price", "gelanguage_id" => "1"],
            ["key" => "TIPO_CREACION_LABEL", "value" => "Tipo de Creación", "gelanguage_id" => "2"],
            ["key" => "TIPO_CREACION_LABEL", "value" => "Type Creation", "gelanguage_id" => "1"],
            ["key" => "TEMPLATE_LABEL", "value" => "Plantilla", "gelanguage_id" => "2"],
            ["key" => "TEMPLATE_LABEL", "value" => "Template", "gelanguage_id" => "1"],
            ["key" => "CLIENTE_LABEL", "value" => "Cliente", "gelanguage_id" => "2"],
            ["key" => "MODALIDAD_SERV_LABEL", "value" => "服務方式", "gelanguage_id" => "3"],
            ["key" => "TIP_CARGA_LABEL", "value" => "Load Type", "gelanguage_id" => "1"],
            ["key" => "TIP_CARGA_LABEL", "value" => "負載類型", "gelanguage_id" => "3"],
            ["key" => "PAYMETHOD_LABEL", "value" => "Método de Pago", "gelanguage_id" => "2"],
            ["key" => "PAYMETHOD_LABEL", "value" => "Pay Method", "gelanguage_id" => "1"],
            ["key" => "PAYMETHOD_LABEL", "value" => "付費方式", "gelanguage_id" => "3"],
            ["key" => "TIPO_MOVIMIENTO_LABEL", "value" => "Tipo de Movimiento", "gelanguage_id" => "2"],
            ["key" => "DESCRIPTION_LABEL", "value" => "Description", "gelanguage_id" => "1"],
            ["key" => "DESCRIPTION_LABEL", "value" => "說明", "gelanguage_id" => "3"],
            ["key" => "LUGAR_RECOGIDA_LABEL", "value" => "Lugar Recogida", "gelanguage_id" => "2"],
            ["key" => "LUGAR_RECOGIDA_LABEL", "value" => "Pick-up", "gelanguage_id" => "1"],
            ["key" => "LUGAR_ENTREGA_LABEL", "value" => "Delivery Place", "gelanguage_id" => "1"],
            ["key" => "LUGAR_ENTREGA_LABEL", "value" => "Lugar Entrega", "gelanguage_id" => "2"],
            ["key" => "LUGAR_ENTREGA_LABEL", "value" => "交貨地點", "gelanguage_id" => "3"],
            ["key" => "langDef", "value" => "Definición de Lenguajes", "gelanguage_id" => "2"],
            ["key" => "langDef", "value" => "Definition Languages", "gelanguage_id" => "1"],
            ["key" => "SEND_MAIL_LABEL", "value" => "Send E-Mail", "gelanguage_id" => "1"],
            ["key" => "SEND_MAIL_LABEL", "value" => "Enviar E-Mail", "gelanguage_id" => "2"],
            ["key" => "IMPORT_LABEL", "value" => "Importar", "gelanguage_id" => "2"],
            ["key" => "TRADUCCIONES_LABEL", "value" => "Traducciones", "gelanguage_id" => "2"],
            ["key" => "TRADUCCIONES_LABEL", "value" => "Translations", "gelanguage_id" => "1"],
            ["key" => "IMPORT_LABEL", "value" => "Import", "gelanguage_id" => "1"],
            ["key" => "TER_ORIGEN_LABEL", "value" => "Terminal Origen", "gelanguage_id" => "2"],
            ["key" => "TER_ORIGEN_LABEL", "value" => "Origin Terminal", "gelanguage_id" => "1"],
            ["key" => "TER_DESTINO_LABEL", "value" => "Terminal Destination", "gelanguage_id" => "1"],
            ["key" => "TER_DESTINO_LABEL", "value" => "TERMINAL DESTINO", "gelanguage_id" => "2"],
            ["key" => "NACIONAL_LABEL", "value" => "Nacional", "gelanguage_id" => "2"],
            ["key" => "NACIONAL_LABEL", "value" => "National", "gelanguage_id" => "1"],
            ["key" => "INTERNACIONAL_LABEL", "value" => "International", "gelanguage_id" => "1"],
            ["key" => "CIUDAD_DESTINO_LABEL", "value" => "Ciudad de Destino", "gelanguage_id" => "2"],
            ["key" => "CIUDAD_DESTINO_LABEL", "value" => "Destination City", "gelanguage_id" => "1"],
            ["key" => "CONCEPTO_LABEL", "value" => "Concepto", "gelanguage_id" => "2"],
            ["key" => "CONCEPTO_LABEL", "value" => "Concept", "gelanguage_id" => "1"],
            ["key" => "TIPO_CARGA_LABEL", "value" => "Type of Load", "gelanguage_id" => "1"],
            ["key" => "TIPO_CARGA_LABEL", "value" => "Tipo de Carga", "gelanguage_id" => "2"],
            ["key" => "MODALIDAD_TRANSPORTE_LABEL", "value" => "Mod. Transporte", "gelanguage_id" => "2"],
            ["key" => "MODALIDAD_TRANSPORTE_LABEL", "value" => "Transport Mode", "gelanguage_id" => "1"],
            ["key" => "ESTADO_LABEL", "value" => "Estado", "gelanguage_id" => "2"],
            ["key" => "ESTADO_LABEL", "value" => "State", "gelanguage_id" => "1"],
            ["key" => "FRECUENCIA_LABEL", "value" => "Frecuencia", "gelanguage_id" => "2"],
            ["key" => "FRECUENCIA_LABEL", "value" => "Frequency", "gelanguage_id" => "1"],
            ["key" => "DIAS_LIBRES_DEVOLUCION_LABEL", "value" => "Days Free Return", "gelanguage_id" => "1"],
            ["key" => "NAME_LABEL", "value" => "Nombre", "gelanguage_id" => "2"],
            ["key" => "NAME_LABEL", "value" => "Name", "gelanguage_id" => "1"],
            ["key" => "DEPARTAMENTO_LABEL", "value" => "Departamento", "gelanguage_id" => "2"],
            ["key" => "DEPARTAMENTO_LABEL", "value" => "Department", "gelanguage_id" => "1"],
            ["key" => "COUNTRY_LABEL", "value" => "País", "gelanguage_id" => "2"],
            ["key" => "COUNTRY_LABEL", "value" => "Country", "gelanguage_id" => "1"],
            ["key" => "LUGAR_RECOGIDA_LABEL", "value" => "Pick-up", "gelanguage_id" => "3"],
            ["key" => "ORIGEN_Y_ DESTINO_LABEL", "value" => "Origin and Destination", "gelanguage_id" => "1"],
            ["key" => "ORIGEN_Y_ DESTINO_LABEL", "value" => "Origen y Destino", "gelanguage_id" => "2"],
            ["key" => "PRODUCTO_LABEL", "value" => "Producto", "gelanguage_id" => "2"],
            ["key" => "PRODUCTO_LABEL", "value" => "Product", "gelanguage_id" => "1"],
            ["key" => "FECHA_HORA_LABEL", "value" => "Fecha Recibo", "gelanguage_id" => "2"],
            ["key" => "REQ_EMBARQUE_LABEL", "value" => "Req. Shipment", "gelanguage_id" => "1"],
            ["key" => "REQ_INHOUSE_LABEL", "value" => "Req. Inhouse", "gelanguage_id" => "1"],
            ["key" => "REQ_INHOUSE_LABEL", "value" => "Req. Inhouse", "gelanguage_id" => "2"],
            ["key" => "CIUDAD_ORIGEN_LABEL", "value" => "家乡", "gelanguage_id" => "3"],
            ["key" => "ROW_LABEL", "value" => "Fila", "gelanguage_id" => "2"],
            ["key" => "DISPLAY", "value" => "Display", "gelanguage_id" => "1"],
            ["key" => "DISPLAY", "value" => "Mostrar", "gelanguage_id" => "2"],
            ["key" => "ENTRIES", "value" => "Entries", "gelanguage_id" => "1"],
            ["key" => "ENTRIES", "value" => "Registros", "gelanguage_id" => "2"],
            ["key" => "ADD_LABEL", "value" => "Add entry", "gelanguage_id" => "1"],
            ["key" => "ADD_LABEL", "value" => "Agregar", "gelanguage_id" => "2"],
            ["key" => "EDIT_LABEL", "value" => "Edit", "gelanguage_id" => "1"],
            ["key" => "EDIT_LABEL", "value" => "Editar", "gelanguage_id" => "2"],
            ["key" => "DELETE_LABEL", "value" => "Delete", "gelanguage_id" => "1"],
            ["key" => "DELETE_LABEL", "value" => "Eliminar", "gelanguage_id" => "2"],
            ["key" => "GES_GEOGRAFICA_LABEL", "value" => "Geographical Management", "gelanguage_id" => "1"],
            ["key" => "GES_GEOGRAFICA_LABEL", "value" => "Gestión Geográfica", "gelanguage_id" => "2"],
            ["key" => "GES_GEOGRAFICA_LABEL", "value" => "地理管理", "gelanguage_id" => "3"],
            ["key" => "GRUPOS_CONCEPTO_LABEL", "value" => "Servicios", "gelanguage_id" => "2"],
            ["key" => "GRUPOS_CONCEPTO_LABEL", "value" => "Services", "gelanguage_id" => "1"],
            ["key" => "GRUPOS_CONCEPTO_LABEL", "value" => "服務", "gelanguage_id" => "3"],
            ["key" => "GRUPOS_X_CONCEPTO_LABEL", "value" => "Service Concepts", "gelanguage_id" => "1"],
            ["key" => "GRUPOS_X_CONCEPTO_LABEL", "value" => "Servicios por Conceptos", "gelanguage_id" => "2"],
            ["key" => "GRUPOS_X_CONCEPTO_LABEL", "value" => "服務理念", "gelanguage_id" => "3"],
            ["key" => "GES_PROVEEDORES_LABEL", "value" => "Gestión Proveedores", "gelanguage_id" => "2"],
            ["key" => "GES_PROVEEDORES_LABEL", "value" => "Providers Management ", "gelanguage_id" => "1"],
            ["key" => "GES_PROVEEDORES_LABEL", "value" => "供應商管理", "gelanguage_id" => "3"],
            ["key" => "GES_CONCEPTOS", "value" => "Gestión Conceptos", "gelanguage_id" => "2"],
            ["key" => "GES_CONCEPTOS", "value" => "Concepts Management ", "gelanguage_id" => "1"],
            ["key" => "GES_CONCEPTOS", "value" => "理念管理", "gelanguage_id" => "3"],
            ["key" => "GENERAL_LABEL", "value" => "Parámetros Generales", "gelanguage_id" => "2"],
            ["key" => "GENERAL_LABEL", "value" => "General parameters", "gelanguage_id" => "1"],
            ["key" => "GENERAL_LABEL", "value" => "常規參數", "gelanguage_id" => "3"],
            ["key" => "TIP_CARGA_LABEL", "value" => "Tipo Carga", "gelanguage_id" => "2"],
            ["key" => "DESCRIPTION_LABEL", "value" => "Descripción", "gelanguage_id" => "2"],
            ["key" => "REQ_TERRESTRE", "value" => "Req. Terrestre", "gelanguage_id" => "2"],
            ["key" => "INTERNACIONAL_LABEL", "value" => "Internacional", "gelanguage_id" => "2"],
            ["key" => "TERMINALES_LABEL", "value" => "Terminales", "gelanguage_id" => "2"],
            ["key" => "CODIGO_LABEL", "value" => "Código", "gelanguage_id" => "2"],
            ["key" => "CODIGO_LABEL", "value" => "Code", "gelanguage_id" => "1"],
            ["key" => "CODIGOS_LABEL", "value" => "Códigos", "gelanguage_id" => "2"],
            ["key" => "CODIGOS_LABEL", "value" => "Codes", "gelanguage_id" => "1"],
            ["key" => "NAMES_LABEL", "value" => "Nombres", "gelanguage_id" => "2"],
            ["key" => "NAMES_LABEL", "value" => "Names", "gelanguage_id" => "1"],
            ["key" => "CIUDAD_LABEL", "value" => "Ciudad", "gelanguage_id" => "2"],
            ["key" => "CIUDAD_LABEL", "value" => "City", "gelanguage_id" => "1"],
            ["key" => "PHONE_FIELD_LABEL", "value" => "Telefono", "gelanguage_id" => "2"],
            ["key" => "PHONE_FIELD_LABEL", "value" => "Phone", "gelanguage_id" => "1"],
            ["key" => "WEB_PAGE_LABEL", "value" => "Web Page", "gelanguage_id" => "1"],
            ["key" => "ESTADO_COTIZACION_LABEL", "value" => "Estado de la Cotizacion", "gelanguage_id" => "2"],
            ["key" => "STR_VAL_LABEL", "value" => "Valor String", "gelanguage_id" => "2"],
            ["key" => "STR_VAL_LABEL", "value" => "String Value", "gelanguage_id" => "1"],
            ["key" => "TIP_MOV_LABEL", "value" => "Tipo Movimiento", "gelanguage_id" => "2"],
            ["key" => "TIP_MOV_LABEL", "value" => "Movement Type", "gelanguage_id" => "1"],
            ["key" => "APROBAR_LABEL", "value" => "Aprobar", "gelanguage_id" => "2"],
            ["key" => "APROBAR_LABEL", "value" => "Approve", "gelanguage_id" => "1"],
            ["key" => "FILE_TOO_LARGE_LABEL", "value" => "File Too Large", "gelanguage_id" => "1"],
            ["key" => "VER_COTIZACION_LABEL", "value" => "Ver Cotización", "gelanguage_id" => "2"],
            ["key" => "FILE_TOO_LARGE_LABEL", "value" => "Archivo muy Pesado", "gelanguage_id" => "2"],
            ["key" => "VER_COTIZACION_LABEL", "value" => "See Quote", "gelanguage_id" => "1"],
            ["key" => "ANULAR_LABEL", "value" => "Anular", "gelanguage_id" => "2"],
            ["key" => "ANULAR_LABEL", "value" => "Annular", "gelanguage_id" => "1"],
            ["key" => "RECHAZAR_LABEL", "value" => "Rechazar", "gelanguage_id" => "2"],
            ["key" => "DETALLE_COT_LABEL", "value" => "Detalle de Cotización", "gelanguage_id" => "2"],
            ["key" => "DETALLE_COT_LABEL", "value" => "Quotation Detail", "gelanguage_id" => "1"],
            ["key" => "SEND_LABEL", "value" => "Send", "gelanguage_id" => "1"],
            ["key" => "SEND_LABEL", "value" => "Enviar", "gelanguage_id" => "2"],
            ["key" => "USER_LABEL", "value" => "Usuario", "gelanguage_id" => "2"],
            ["key" => "CIUDAD_PUERTO_ORIGEN_LABEL", "value" => "Ciudad/Pto. Origen", "gelanguage_id" => "2"],
            ["key" => "CIUDAD_PUERTO_DESTINO_LABEL", "value" => "Ciudad/Pto. Destino", "gelanguage_id" => "2"],
            ["key" => "AGENTE_LABEL", "value" => "Agente", "gelanguage_id" => "2"],
            ["key" => "AGENTES_LABEL", "value" => "Agentes", "gelanguage_id" => "2"],
            ["key" => "TIEMPO_TRANSITO_LABEL", "value" => "Tiempo en Transito", "gelanguage_id" => "2"],
            ["key" => "CURRENCIES_LABEL", "value" => "Monedas", "gelanguage_id" => "2"],
            ["key" => "CIUDADES_PUERTOS_DESTINO_LABEL", "value" => "Ciudades/Ptos Destino", "gelanguage_id" => "2"],
            ["key" => "CIUDADES_PUERTOS_ORIGEN_LABEL", "value" => "Ciudades/Ptos Origen", "gelanguage_id" => "2"],
            ["key" => "TIPOS_CARGA_LABEL", "value" => "Tipos de Carga", "gelanguage_id" => "2"],
            ["key" => "UNIDAD_EMPAQUE_LABEL", "value" => "Unidad de Empaque", "gelanguage_id" => "2"],
            ["key" => "UNIDADES_NEGOCIO_LABEL", "value" => "Unidades de Negocio", "gelanguage_id" => "2"],
            ["key" => "COSTO_LABEL", "value" => "Costo", "gelanguage_id" => "2"],
            ["key" => "ACCIONES_LABEL", "value" => "Acciones", "gelanguage_id" => "2"],
            ["key" => "ACCIONES_LABEL", "value" => "Actions", "gelanguage_id" => "1"],
            ["key" => "SELECCIONE_LABEL", "value" => "Seleccione", "gelanguage_id" => "2"],
            ["key" => "VENCIMIENTO_POLIZA_LABEL", "value" => "Vencimiento Póliza", "gelanguage_id" => "2"],
            ["key" => "NOMBRE_ASEGURADORA_LABEL", "value" => "Nombre Aseguradora", "gelanguage_id" => "2"],
            ["key" => "POLIZA_CLIENTE_LABEL", "value" => "Num. póliza Cliente", "gelanguage_id" => "2"],
            ["key" => "REQ_SEGURO_LABEL", "value" => "Requiere Seguro", "gelanguage_id" => "2"],
            ["key" => "ALTO_LABEL", "value" => "Alto", "gelanguage_id" => "2"],
            ["key" => "ANCHO_LABEL", "value" => "Ancho", "gelanguage_id" => "2"],
            ["key" => "LARGO_LABEL", "value" => "Largo", "gelanguage_id" => "2"],
            ["key" => "REQ_PERMISO_LABEL", "value" => "Requiere Permiso", "gelanguage_id" => "2"],
            ["key" => "CUADRILLA_LABEL", "value" => "Cuadrilla", "gelanguage_id" => "2"],
            ["key" => "VALOR_DECLARADO_LABEL", "value" => "Valor Declarado", "gelanguage_id" => "2"],
            ["key" => "VOLUMEN_MERCANCIA_M3_LABEL", "value" => "Volumen Mercancía (M3)", "gelanguage_id" => "2"],
            ["key" => "PESO_KGS_LABEL", "value" => "Peso Mercancía (Kg.)", "gelanguage_id" => "2"],
            ["key" => "CANT_ESCOLTA_LABEL", "value" => "Cantidad de Escoltas", "gelanguage_id" => "2"],
            ["key" => "MOD_DESPACHO_LABEL", "value" => "Modalidad del Despacho", "gelanguage_id" => "2"],
            ["key" => "UNIDAD_EMPAQUE_LABEL", "value" => "Unidad Empaque", "gelanguage_id" => "2"],
            ["key" => "TELEFONO_DESTINATARIO_LABEL", "value" => "Telefono Destinatario", "gelanguage_id" => "2"],
            ["key" => "CONTACTO_DESTINATARIO_LABEL", "value" => "Contacto Destinatario", "gelanguage_id" => "2"],
            ["key" => "NOMBRE_DESTINATARIO_LABEL", "value" => "Nombre Destinatario", "gelanguage_id" => "2"],
            ["key" => "FECHA_ENTREGA_LABEL", "value" => "Fecha Entrega", "gelanguage_id" => "2"],
            ["key" => "FECHA_RECOGIDA_LABEL", "value" => "Fecha Recogida", "gelanguage_id" => "2"],
            ["key" => "UNIDAD_FACTURA_LABEL", "value" => "Unidad Factura", "gelanguage_id" => "2"],
            ["key" => "SISTEMA_FACTURACION_LABEL", "value" => "Sistema de Facturacion", "gelanguage_id" => "2"],
            ["key" => "PROM_UM_PEDIDOS_LABEL", "value" => "Prom. U/M por Pedidos", "gelanguage_id" => "2"],
            ["key" => "NUMERO_PEDIDOS_LABEL", "value" => "Numero Pedidos", "gelanguage_id" => "2"],
            ["key" => "NUMERO_REFERENCIAS_LABEL", "value" => "Numero de Referencias", "gelanguage_id" => "2"],
            ["key" => "REQ_PICKING_LABEL", "value" => "Requiere Picking", "gelanguage_id" => "2"],
            ["key" => "VALOR_ESTIMADO_MCIA_LABEL", "value" => "Valor Estimado", "gelanguage_id" => "2"],
            ["key" => "UNIDAD_DESPACHO_LABEL", "value" => "Unidad de Despacho", "gelanguage_id" => "2"],
            ["key" => "ALMACENAMIENTO_LABEL", "value" => "Almacenamiento", "gelanguage_id" => "2"],
            ["key" => "UNIDAD_CARGA_LABEL", "value" => "Unidad de Carga", "gelanguage_id" => "2"],
            ["key" => "NATURALEZA_CARGA_LABEL", "value" => "Naturaleza de la Carga", "gelanguage_id" => "2"],
            ["key" => "ENTIDAD_CONTROL_LABEL", "value" => "Entidad de Control", "gelanguage_id" => "2"],
            ["key" => "SECTOR_LABEL", "value" => "Sector", "gelanguage_id" => "2"],
            ["key" => "PERMISOS_LABEL", "value" => "Permisos", "gelanguage_id" => "2"],
            ["key" => "ROLES_LABEL", "value" => "Roles", "gelanguage_id" => "2"],
            ["key" => "END_POINTS_LABEL", "value" => "End Points", "gelanguage_id" => "2"],
            ["key" => "USERS_LABEL", "value" => "Usuarios", "gelanguage_id" => "2"],
            ["key" => "CAMBIAR_CONTRASEÑA_LABEL", "value" => "Cambiar Contraseña", "gelanguage_id" => "2"],
            ["key" => "USERS_LABEL", "value" => "Usuarios", "gelanguage_id" => "2"],
            ["key" => "USERS_LABEL", "value" => "Usuarios", "gelanguage_id" => "2"],
            ["key" => "ROL_LABEL", "value" => "Rol", "gelanguage_id" => "2"],
            ["key" => "MENU_LABEL_LABEL", "value" => "Menú Label", "gelanguage_id" => "2"],
            ["key" => "PAGE_TITLE_LABEL", "value" => "Titulo Pagina", "gelanguage_id" => "2"],
            ["key" => "PAGE_DESC_LABEL", "value" => "Descripción Pagina", "gelanguage_id" => "2"],
            ["key" => "ICON_LABEL", "value" => "Icono", "gelanguage_id" => "2"],
            ["key" => "SEGURIDAD_LABEL", "value" => "Seguridad", "gelanguage_id" => "2"],
            ["key" => "CREAR_CUENTA_LABEL", "value" => "Crear cuenta", "gelanguage_id" => "2"],
            ["key" => "LAST_NAMES_LABEL", "value" => "Apellidos", "gelanguage_id" => "2"],
            ["key" => "REGISTRARSE_LABEL", "value" => "Registrarse", "gelanguage_id" => "2"],
        );

        foreach ($langDefs as $langdef) {
            GelangDef::create($langdef);
        }

        echo "Creando endpoint sin endpoint parent\n";
        $endPoints = array(["state" => "dashboard", "menu_label" => "DASHBOARD_LABEL", "show" => "1",
            "page_desc" => "dashboard", "seendpoint_id" => null, "icon" => "dashboard", "sort" => "0"],
            ["state" => "userroles", "menu_label" => "ROLES_USUARIO_LABEL", "show" => "0",
                "page_desc" => "ROLES_USUARIO_LABEL", "seendpoint_id" => null],
            ["state" => "langs", "menu_label" => "LANG_LABEL", "show" => "0", "page_desc" => "LANG_LABEL",
                "seendpoint_id" => null],
            ["state" => "lang", "menu_label" => "LANG_LABEL", "show" => "0", "page_desc" => "LANG_LABEL",
                "seendpoint_id" => null],
            ["state" => "login", "menu_label" => "LOGIN_BTN_LABEL", "show" => "0", "page_desc" => "LOGIN_BTN_LABEL",
                "seendpoint_id" => null],
            ["state" => "logout", "menu_label" => "LOGOUT_BTN_LABEL", "show" => "0", "page_desc" => "LOGOUT_BTN_LABEL",
                "seendpoint_id" => null],
            ["state" => "validar", "menu_label" => "VALIDATE_LABEL", "show" => "0",
                "page_desc" => "VALIDATE_LABEL", "seendpoint_id" => null],
            ["state" => "plantilla", "menu_label" => "Gestion de Plantilla", "show" => "0",
                "page_desc" => "Gestion de Plantilla", "seendpoint_id" => null],
            ["state" => "convertirMoneda", "menu_label" => "CONVERTIR_MONEDA_LABEL", "show" => "0",
                "page_desc" => "CONVERTIR_MONEDA_LABEL", "seendpoint_id" => null]
        );

        foreach ($endPoints as $endp) {
            Seendpoint::create($endp);
        }

        echo "Creando endpoints para general\n";
        $endPointGeneral = Seendpoint::create(["state" => "", "menu_label" => "GENERAL_LABEL", "show" => "1",
            "page_desc" => "GENERAL_LABEL"]);

        $generalGroup = array(
            ["state" => "moneda", "menu_label" => "CURRENCY_LABEL", "show" => "1", "page_desc" => "CURRENCY_LABEL",
                "seendpoint_id" => $endPointGeneral->id],
            ["state" => "estados", "menu_label" => "ESTADOS_LABEL", "show" => "1", "page_desc" => "ESTADOS_LABEL",
                "seendpoint_id" => $endPointGeneral->id],
            ["state" => "modulos", "menu_label" => "MODULOS_LABEL", "show" => "1", "page_desc" => "MODULOS_LABEL",
                "seendpoint_id" => $endPointGeneral->id],
            ["state" => "langDef", "menu_label" => "TRADUCCIONES_LABEL", "show" => "1", "page_desc" => "LANG_DEF_LABEL",
                "seendpoint_id" => $endPointGeneral->id],
            ["state" => "geparameters", "menu_label" => "GEPARAMETERS_LABEL", "show" => "1",
                "page_desc" => "GEPARAMETERS_LABEL", "seendpoint_id" => $endPointGeneral->id],
            ["state" => "template", "menu_label" => "TEMPLATE_LABEL", "show" => "1",
                "page_desc" => "Administracion de plantillas", "seendpoint_id" => $endPointGeneral->id],
            ["state" => "alerta", "menu_label" => "ALERTAS_LABEL", "show" => "1",
                "page_desc" => "permiso para administrar alertas", "seendpoint_id" => $endPointGeneral->id],
            ["state" => "geparametersServ", "menu_label" => "GEPARAMETERS_LABEL", "show" => "0",
                "page_desc" => "EndPoint para el servicio de parametros del sistema",
                "seendpoint_id" => $endPointGeneral->id],
            ["state" => "tipoAlerta", "menu_label" => "TIPO_ALERTAS_LABEL", "show" => "1",
                "page_desc" => "permiso para administrar tipos de alertas", "seendpoint_id" => $endPointGeneral->id]
        );

        foreach ($generalGroup as $endp) {
            Seendpoint::create($endp);
        }

        echo "Creando endpoints para Gestion Geografica\n";
        $endPointGeografica = Seendpoint::create(["state" => "", "menu_label" => "GES_GEOGRAFICA_LABEL", "show" => "1",
            "page_desc" => "GES_GEOGRAFICA_LABEL"]);

        $geograficaGroup = array(["state" => "departamentos", "menu_label" => "DEPARTAMENTOS_LABEL", "show" => "1",
            "page_desc" => "DEPARTAMENTOS_LABEL", "seendpoint_id" => $endPointGeografica->id],
            ["state" => "ciudades", "menu_label" => "CIUDADES_LABEL", "show" => "1", "page_desc" => "CIUDADES_LABEL",
                "seendpoint_id" => $endPointGeografica->id],
            ["state" => "paises", "menu_label" => "COUNTRIES_LABEL", "show" => "1", "page_desc" => "COUNTRIES_LABEL",
                "seendpoint_id" => $endPointGeografica->id]);

        foreach ($geograficaGroup as $endp) {
            Seendpoint::create($endp);
        }

        echo "Creando endpoints para seguridad\n";
        $endPointSeguridad = Seendpoint::create(["state" => "", "menu_label" => "SEGURIDAD_LABEL", "show" => "1",
            "page_desc" => "SEGURIDAD_LABEL"]);

        $seguridadGroup = array(["state" => "roles", "menu_label" => "PERMISOS_LABEL", "show" => "1", "page_desc" => "PERMISOS_LABEL", "seendpoint_id" => $endPointSeguridad->id],
            ["state" => "endPoints", "menu_label" => "END_POINTS_LABEL", "show" => "1", "page_desc" => "END_POINTS_LABEL", "seendpoint_id" => $endPointSeguridad->id],
            ["state" => "endPointsMethod", "menu_label" => "END_POINT_METHODS_LABEL", "show" => "0", "page_desc" => "END_POINT_METHODS_LABEL", "seendpoint_id" => $endPointSeguridad->id],
            ["state" => "usersCRUD", "menu_label" => "USERS_LABEL", "show" => "1", "page_desc" => "EndPoint para hacer CRUD a los usuarios", "seendpoint_id" => $endPointSeguridad->id],
            ["state" => "users", "menu_label" => "USERS_LABEL", "show" => "0", "page_desc" => "EndPoint para consultar la informaciòn de un usuario", "seendpoint_id" => $endPointSeguridad->id],
        );

        foreach ($seguridadGroup as $endp) {
            Seendpoint::create($endp);
        }

        echo "Creando metodos \n";
        $endPoints = Seendpoint::all();
        foreach ($endPoints as $endPoint) {

            switch ($endPoint->state) {
                /*END POINT CON PERMISOS DE POST*/
                case "login":
                case "convertirMoneda":
                case "logout":
                    SeendpointMethod::create(["method" => "POST", "seendpoint_id" => $endPoint->id, "serole_id" => "1"]);
                    break;

                /*END POINT CON PERMISOS DE GET*/
                case "langs":
                case "actualizaMoneda":
                case "geparametersServ":
                case "users":
                    SeendpointMethod::create(["method" => "GET", "seendpoint_id" => $endPoint->id, "serole_id" => "1"]);
                    break;

                case "":
                    break;

                default:
                    SeendpointMethod::create(["method" => "*", "seendpoint_id" => $endPoint->id, "serole_id" => "1"]);
            }
        }

        echo "Creando monedas\n";
        $monedas = array(["nombre" => "DOLAR USD", "simbolo" => '$', "abreviatura" => "USD", "factor_conversion" => 1],
            ["nombre" => "PESO COL", "simbolo" => '$', "abreviatura" => "COP", "factor_conversion" => 0.000333],
            ["nombre" => "EURO", "simbolo" => '€', "abreviatura" => "EUR", "factor_conversion" => 1.1285]);
        foreach ($monedas as $moneda) {
            Gemoneda::create($moneda);
        }

        echo "Creando modulo GENERAL\n";
        $modulo = Gemodulo::create(["nombre" => "GENERAL"]);

        echo "Creando GEPARAMETERS para url de la app\n";
        $url = array(
            "key" => "URL_APP",
            "strval" => "app.control.dev",
            "observation" => "URL de actual de la app",
            "gemodulo_id" => $modulo->id
        );
        Geparameters::create($url);

        echo "Creando el GEPARAMETERS para el usuario control " . __LINE__ . "\n";
        \App\Util\SystemParameters::setNumSysParam("USUARIO_LOG_CONTROL", $userLogControl->id,
            "Usuario control, sin permisos para registros de logs", $modulo->id);

        echo "Creando el getemplate para cuando falla la actualizacion de la moneda " . __LINE__ . "\n";
        $templateMoneda = \App\Models\GE\Getemplate::create([
            "nombre" => 'ERROR_ACTUALIZA_MONEDA',
            "asunto" => 'Error al actualizar la moneda',
            "cuerpomsg" => '"<p>Error al actualizar la moneda&lt;br&gt;</p><pre>[cuerpo]</pre>"',
        ]);

        echo "Creando el tipoalerta para cuando falla la actualizacion de la moneda " . __LINE__ . "\n";
        $tipoAlertaMoneda = \App\Models\GE\GetipoAlerta::create([
            "nombre" => "ERROR_ACTUALIZA_MONEDA",
            "obs" => "Alerta que se ejecuta cuando la moneda no pudo ser actualizada",
        ]);

        echo "Creando la alerta para cuando falla la actualizacion de la moneda " . __LINE__ . "\n";
        \App\Models\GE\Gealerta::create([
            "isuserlogged" => 0,
            "getipo_alerta_id" => $tipoAlertaMoneda->id,
            "getemplate_id" => $templateMoneda->id,
            "siemprecc" => 'sistemas@valleygroups.com'
        ]);

        echo "Creando el tipo alerta para la recuperación de contraseñas RECORDAR_CONTRASEÑA " . __LINE__ . "\n";
        $tipoAlerta = \App\Models\GE\GetipoAlerta::create([
            "nombre" => "RECORDAR_CONTRASEÑA",
            "obs" => "Email enviado al momento de presionar recordar contraseña"
        ]);

        echo "Creando la plantilla para recuperar contraseñas " . __LINE__ . "\n";
        $template = \App\Models\GE\Getemplate::create([
            "nombre" => "RECORDAR_CONTRASEÑA",
            "asunto" => "Recordatorio de Contraseña Control",
            "cuerpomsg" => '<html>
	<head>
		<title></title>
	</head>
	<body>
		<p>
			<span style="box-sizing: border-box; font-size: 17px; font-family: &quot;Trebuchet MS&quot;, Verdana, Arial; white-space: nowrap; background-color: rgb(255, 255, 255); font-weight: bold;">Contrase&ntilde;a restablecida</span><span style="font-family: &quot;Trebuchet MS&quot;, Verdana, Arial; font-size: 11.2378px; white-space: nowrap; background-color: rgb(255, 255, 255);">&nbsp;</span></p>
		<p style="box-sizing: border-box; margin: 0px 0px 20px; font-family: &quot;Trebuchet MS&quot;, Verdana, Arial; white-space: nowrap; background-color: rgb(255, 255, 255); font-size: 0.987em !important;">
			Su contrase&ntilde;a ha sido restablecida, sus datos de acceso son:</p>
		<ul style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0px; font-family: &quot;Trebuchet MS&quot;, Verdana, Arial; white-space: nowrap; background-color: rgb(255, 255, 255); list-style-type: square; font-size: 0.987em !important;">
			<li style="box-sizing: border-box; font-size: 0.987em !important; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(239, 239, 239); border-left-color: initial; border-image: initial; padding: 0px 0px 0.5em;">
				Nombre Registrado: [NOMUSER]</li>
			<li style="box-sizing: border-box; font-size: 0.987em !important; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: solid; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(239, 239, 239); border-left-color: initial; border-image: initial; padding: 0.5em 0px;">
				Correo Usuario: [CORRUSER]</li>
			<li style="box-sizing: border-box; font-size: 0.987em !important; border-top: 0px; border-right: 0px; border-bottom: none; border-left: 0px; border-image: initial; padding: 0.5em 0px;">
				Clave Usuario: [CLAUSER]</li>
		</ul>
		<p style="box-sizing: border-box; border-top: 0px; border-right: 0px; border-bottom: none; border-left: 0px; border-image: initial; padding: 0.5em 0px; font-size: 0.987em !important;">
			Puede iniciar sesi&oacute;n en la siguiente direcci&oacute;n:&nbsp;<a href="http://[URLAPP]/login?email=[CORRUSER]" style="background-color: transparent; box-sizing: border-box; color: rgb(51, 122, 183); text-decoration-line: none; text-shadow: none; font-size: 0.987em !important;" target="_blank">[URLAPP]</a><span style="font-size: 0.987em;">.</span></p>
	</body>
</html>']);

        echo "Creando la alerta " . __LINE__ . "\n";
        \App\Models\GE\Gealerta::create([
            "getipo_alerta_id" => $tipoAlerta->id,
            "getemplate_id" => $template->id,
            "isuserlogged" => 0,
        ]);

    }
}
