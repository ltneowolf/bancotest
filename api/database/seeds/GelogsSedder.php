<?php

use Illuminate\Database\Seeder;

class GelogsSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "Corriendo el Seeder " . __CLASS__ . "\n";

        echo "Creando traducciones " . __LINE__ . "\n";
        $newLangDefs = array(
            /***********************************************************************************************************/
            ["key" => "REPORTES_MENU_LABEL", "value" => "Reportes", "gelanguage_id" => "2"],
            ["key" => "DATE_LABEL", "value" => "Fecha", "gelanguage_id" => "2"],
            ["key" => "DATE_LABEL", "value" => "Date", "gelanguage_id" => "1"],
            ["key" => "GENERAR_REPORTE_LABEL", "value" => "Generar reporte", "gelanguage_id" => "2"],
            ["key" => "GENERAR_REPORTE_LABEL", "value" => "Generate report", "gelanguage_id" => "1"],
            ["key" => "FECHA_INICIAL_LABEL", "value" => "Fecha inicial", "gelanguage_id" => "2"],
            ["key" => "FECHA_INICIAL_LABEL", "value" => "Initial date", "gelanguage_id" => "1"],
            ["key" => "FECHA_FINAL_LABEL", "value" => "Fecha final", "gelanguage_id" => "2"],
            ["key" => "FECHA_FINAL_LABEL", "value" => "Final date", "gelanguage_id" => "1"],
            ["key" => "REPORTES_MENU_LABEL", "value" => "Reports", "gelanguage_id" => "1"],
            ["key" => "TIPOS_LABEL", "value" => "Tipos", "gelanguage_id" => "1"],
            /***********************************************************************************************************/
        );

        foreach ($newLangDefs as $newLangDef) {
            \App\Models\GE\GelangDef::firstOrCreate($newLangDef);
        }

        echo "Consultando el id del menu de reportes " . __LINE__ . "\n";
        $menuReportes = \App\Models\SE\Seendpoint::firstOrCreate([
            'menu_label' => 'REPORTES_MENU_LABEL',
            'page_desc' => 'REPORTES_MENU_LABEL',
            'show' => 1,
            'sort' => 7,
        ]);

        //ENDPOINTS
        echo "Creando el endPoint logReporte " . __LINE__ . "\n";
        $endPointLogReporte = \App\Models\SE\Seendpoint::create([
            'seendpoint_id' => $menuReportes->id,
            'page_title' => 'LOG_REPORTE',
            'menu_label' => 'LOG_REPORTE',
            'state' => 'logReporte',
            'page_desc' => 'LOG_REPORTE_DESC',
            'show' => '1',
            'sort' => 17
        ]);

        //PERMISOS
        echo "Otorgando permisos " . __LINE__ . "\n";
        \App\Models\SE\SeendpointMethod::create([
            'seendpoint_id' => $endPointLogReporte->id,
            'serole_id' => 1,
            'method' => 'GET'
        ]);

        echo "Creando endpoints para tipos \n";
        $spid = \App\Models\SE\Seendpoint::where('menu_label', 'GENERAL_LABEL')->get()->first()->id;

        $endPoint = ["state" => "tipos", "menu_label" => "TIPOS_LABEL", "page_title" => "",
            "page_desc" => "Modulo para gestion de tipos", "seendpoint_id" => $spid, "show" => "1"];

        $newEndPoint = \App\Models\SE\Seendpoint::create($endPoint);

        echo "Creando metodos de Endpoint en tipo \n";
        \App\Models\SE\SeendpointMethod::create(["method" => "*", "seendpoint_id" => $newEndPoint->id, "serole_id" => 1]);

        //TRADUCCIONES
        echo "Creando traducciones " . __LINE__ . "\n";
        $langs = [
            [
                'gelanguage_id' => '2',
                'key' => 'LOG_REPORTE',
                'value' => 'Log Reporte',
            ], [
                'gelanguage_id' => '2',
                'key' => 'LOG_REPORTE_DESC',
                'value' => 'Ventana para generar el reporte de LOGS',
            ], [
                'gelanguage_id' => '2',
                'key' => 'GETIPO_APLICA_ESTATUS_NOT_FOUND',
                'value' => 'Configurar el Getipo Aplica estatus con breviatura PRC_APL_ESTATUS y Grupo PROCESO_MODULOS',
            ], [
                'gelanguage_id' => '2',
                'key' => 'PROCESO_LABEL',
                'value' => 'Proceso',
            ], [
                'gelanguage_id' => '2',
                'key' => 'CONFIGURAR_GETIPOS_GRUPO_PROCESO_MODULOS',
                'value' => 'Para la lista de procesos debe configurar los Getipos con el grupo PROCESO_MODULOS',
            ]
        ];
        foreach ($langs as $lang) {
            \App\Models\GE\GelangDef::create($lang);
        }

        //GETIPOS, LOS GETIPOS PARA
        /* echo "Registrando GETIPOS " . __LINE__ . "\n";
         \App\Models\GE\Getipo::create([
             'NOMBRE' => 'Aplicar estatus',
             'GEMODULO_ID' => \App\Util\SystemParameters::getNumSysParam('MODULO_ESTATUS'),
             'GRUPO' => 'PROCESO_MODULOS',
             'ABREVIATURA' => 'PRC_APL_ESTATUS',
         ]);*/
    }
}
