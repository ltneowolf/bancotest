<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeemailUpdateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geemails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('funcion');
            $table->string("grupo")->nullable();
            $table->string("asunto")->nullable();
            $table->timestamps();
        });

        //Tabla de correos del shipment//
        Schema::create('geemail_enviados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asunto');
            $table->string('remitente')->nullable();
            $table->string('para')->nullable();
            $table->string('cc')->nullable();
            $table->string('cco')->nullable();
            $table->text('cuerpo');
            $table->string('objectgroup')->nullable();
            $table->string('objectid')->nullable();
            $table->timestamps();
        });

        //Tabla de adjuntos de los correos//
        Schema::create('geemail_enviado_adjuntos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::table('geemail_enviado_adjuntos', function (Blueprint $table) {
            $table->fastForeign('geemail_enviados');
            $table->fastForeign('geadjuntos');
        });

        Schema::table('geadjuntos', function (Blueprint $table) {
            $table->string('ruta')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('geemail_enviado_adjuntos');
        Schema::drop('geemail_enviados');
        Schema::drop('geemails');
    }
}
