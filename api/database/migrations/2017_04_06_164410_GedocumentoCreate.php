<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GedocumentoCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Corriendo migracion " . __CLASS__ . "\n";

        //Tabla de status de la operacion//
        Schema::create('geadjuntos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('mimetype');
            $table->string('extension');
            $table->string('nombrearchivo');
            $table->timestamps();
        });

        //Tabla de status de la operacion//
        Schema::create('gedocumentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('funcion')->nullable();
            $table->string('popup')->nullable();
            $table->string('grupo')->nullable();
            $table->string('tipo');
            $table->timestamps();
        });

        echo "Fin create gedocumentos, row " . __LINE__ . " \n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('geadjuntos');
        Schema::drop('gedocumentos');
    }
}
