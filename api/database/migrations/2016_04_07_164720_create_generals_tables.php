<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Inicio de la migración";
        // Usuarios

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password', 60);
            $table->rememberToken();
            $table->unique('email');
            $table->boolean('cambiar_password')->nullable();
            $table->timestamps();
        });

        echo "Fin create user, row 28 \n";

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at');
        });

        echo "Fin create passwords_resets, row 36\n";

        //Roles y permisos

        Schema::create('seroles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        echo "Fin create seroles, row 46\n";

        Schema::create('seuser_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        echo "Fin create seuser_roles, row 53\n";

        Schema::create('seendpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->string('state')->nullable()->default(null);
            $table->string('menu_label');
            $table->string('page_title')->nullable()->default(null);
            $table->string('page_desc');
            $table->integer('seendpoint_id')->unsigned()->nullable()->default(null);
            $table->boolean('show')->default(true);
            $table->string('icon')->nullable()->default(null);
            $table->timestamps();
        });

        echo "Fin create seendpoints, row 67\n";

        Schema::create('seendpoint_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('method');
            $table->timestamps();
        });

        echo "Fin create seendpoints_methods, row 75\n";

        //Relaciones de roles y permisos
        Schema::table('seendpoints', function (Blueprint $table) {
            $table->foreign('seendpoint_id')->references('id')->on('seendpoints');
        });

        echo "Fin create seendpoints, row 82\n";

        Schema::table('seendpoint_methods', function (Blueprint $table) {
            $table->fastForeign('seendpoints');
            $table->fastForeign('seroles');
        });

        echo "Fin create seendpoint_methods, row 89\n";

        Schema::table('seuser_roles', function (Blueprint $table) {
            $table->fastForeign('users');
            $table->fastForeign('seroles');
        });

        echo "Fin create seuser_roles, row 96\n";

        //Lenguaje
        Schema::create('gelanguages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang');
            $table->string('label');
            $table->string('icon')->nullable();
            $table->timestamps();
        });

        echo "Fin create gelanguages, row 107\n";

        Schema::create('gelang_defs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->string('value');
            $table->timestamps();
        });

        echo "Fin create gelang_defs, row 116\n";

        //Lenguaje relaciones
        Schema::table('gelang_defs', function (Blueprint $table) {
            $table->fastForeign('gelanguages');
        });

        echo "Fin create gelang_defs, row 123\n";

        //Gestion geografica
        Schema::create('gepaises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('abreviatura', 10)->nullable();
            $table->unique('abreviatura');
            $table->timestamps();
        });

        echo "Fin create gepaises, row 123\n";

        Schema::create('gedepartamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('abreviatura', 10)->nullable();
            $table->unique('abreviatura');
            $table->timestamps();
        });

        echo "Fin create gedepartamentos, row 145\n";

        Schema::create('geciudades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('abreviatura', 10)->nullable();
            $table->unique('abreviatura');
            $table->timestamps();
        });

        echo "Fin create geciudades, row 155\n";

        //relaciones

        Schema::table('gedepartamentos', function (Blueprint $table) {
            $table->fastForeign('gepaises');
        });

        echo "Fin create gedepartamentos, row 172\n";

        Schema::table('geciudades', function (Blueprint $table) {
            $table->fastForeign('gedepartamentos');
        });

        echo "Fin create geciudades, row 178\n";

        //Estados - Modulos
        Schema::create('geestados', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->timestamps();
        });

        echo "Fin create geestados, row \n";

        Schema::create('gemodulos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->timestamps();
        });

        echo "Fin create gemodulos, row\n";

        //Relaciones
        Schema::table('geestados', function (Blueprint $table) {
            $table->fastForeign('gemodulos');
        });

        echo "Fin create geestados, row\n";

        //Monedas
        Schema::create('gemonedas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('simbolo');
            $table->string('abreviatura');
            $table->double('factor_conversion', 14, 6);
            $table->double('ajuste', 14, 6)->nullable();
            $table->unique('abreviatura');
            $table->timestamps();
        });

        echo "Fin create gemonedas, row\n";

        Schema::create('geparameters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->double('numval', 14, 6)->nullable();
            $table->string('strval', 4000)->nullable();
            $table->string('observation', 4000);
            $table->unique('key');
            $table->timestamps();
        });

        echo "Fin create geparameters, row\n";

        //Relaciones Geparametros
        Schema::table('geparameters', function (Blueprint $table) {
            $table->fastForeign('gemodulos');
        });

        echo "Fin relaciones geparameters\n";

        echo "CREANDO getemplates\n";
        Schema::create('getemplates', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->string('asunto');
            $table->string('cuerpomsg', 4000);
            $table->unique('nombre');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('getemplates');
        Schema::drop('GEPARAMETERS');
        Schema::drop('gemonedas');
        Schema::drop('geestados');
        Schema::drop('gemodulos');
        Schema::drop('geciudades');
        Schema::drop('gedepartamentos');
        Schema::drop('gepaises');
        Schema::drop('gelang_defs');
        Schema::drop('gelanguages');
        Schema::drop('seendpoint_methods');
        Schema::drop('seendpoints');
        Schema::drop('seuser_roles');
        Schema::drop('seroles');
        Schema::drop('password_resets');
        Schema::drop('users');

    }
}
