<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BancoTest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Corriendo la migracion " . __CLASS__ . "\n";

        echo "Creando la tabla CLIENTES" . __LINE__ . "\n";

        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('direccion');
            $table->string('telefono');
            $table->string('identificacion');
            $table->string('email')->unique();
            $table->fastForeign('getipos', false, 'tipo_identificacion');
            $table->fastForeign('geciudades');
            $table->fastForeign('users', false, 'asesor_id');
            $table->timestamps();
        });

        echo "Creando la tabla CUENTA_AHORROS" . __LINE__ . "\n";

        Schema::create('cuenta_ahorros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_cuenta', 8)->unique();
            $table->string('clave', 4);
            $table->boolean('estado')->default(true);
            $table->fastForeign('clientes');
            $table->fastForeign('users', false, 'asesor_id');
            $table->timestamps();
        });

        echo "Creando la tabla CUENTA_AHORRO_TRANSACCIONES" . __LINE__ . "\n";

        Schema::create('ctaaho_transacciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->double('monto', 15, 2);
            $table->fastForeign('getipos', false, 'tipo_transaccion');
            $table->fastForeign('users', false, 'cajero_id');
            $table->fastForeign('cuenta_ahorros');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "Corriendo la migracion " . __CLASS__ . "\n";

        echo "Eliminando la tabla CTAAHO_TRANSACCIONES" . __LINE__ . "\n";
        Schema::drop('ctaaho_transacciones');

        echo "Eliminando la tabla CUENTA_AHORROS" . __LINE__ . "\n";
        Schema::drop('cuenta_ahorros');

        echo "Eliminando la tabla CLIENTES" . __LINE__ . "\n";
        Schema::drop('clientes');
    }
}
