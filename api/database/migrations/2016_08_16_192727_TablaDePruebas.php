<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablaDePruebas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("pruebas", function (Blueprint $table) {
            $table->increments('id');
            $table->string('text')->nullable();
            $table->integer('numero')->nullable();
            $table->boolean('checkbox')->nullable();
            $table->string('radio')->nullable();
            $table->string('password')->nullable();
            $table->string('color')->nullable();
            $table->date('fecha')->nullable();
            $table->dateTime('fechatiempo')->nullable();
            $table->string('email')->nullable();
            $table->integer('listadevalor')->unsigned()->nullable();
            $table->string('listamultiple')->nullable();
            $table->string('switch_c')->nullable();
            $table->string('loaddata')->nullable();
            $table->string('custom')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pruebas');
    }
}
