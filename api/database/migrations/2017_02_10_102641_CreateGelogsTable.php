<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGelogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Corriendo la migración " . __CLASS__ . "\n";

        echo "Creando la tabla gelogs " . __LINE__ . "\n";
        Schema::create('gelogs', function (Blueprint $table) {
            $table->increments("id");
            $table->string("ref_registro");
            $table->fastForeign('users');
            $table->integer("proceso_id", false, true);
            $table->string("mensaje", 4000);
            $table->timestamps();
        });

        Schema::table('gelogs', function (Blueprint $table) {
            $table->foreign("proceso_id")->references("id")->on("getipos");
        });

        echo "Agregando la columna abreviatura a la tabla getipos " . __LINE__ . "\n";
        Schema::table('getipos', function (Blueprint $table) {
            $table->string('abreviatura', 50)->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "Rollback de la migración " . __CLASS__ . "\n";

        echo "Eliminando la tabla gelogs " . __LINE__ . "\n";
        Schema::drop("gelogs");

        echo "Eliminando la columna abreviatura de la tabla getipos " . __LINE__ . "\n";
        Schema::table('getipos', function (Blueprint $table) {
            $table->dropColumn('abreviatura');
        });
    }
}
