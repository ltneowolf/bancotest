<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUniqueCiudadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Eliminando uniques en departamentos y ciudades\n";

        Schema::table('gedepartamentos', function (Blueprint $table) {
            $table->dropUnique('gedepartamentos_abreviatura_unique');
        });

        Schema::table('geciudades', function (Blueprint $table) {
            $table->dropUnique('geciudades_abreviatura_unique');
        });

        echo "Fin eliminacion uniques\n";

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
