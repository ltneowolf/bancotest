<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeendpoitsAddMobile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Corriendo la migracion " . __CLASS__ . "\n";

        echo "Agregando la columna is_mobile a la tabla Seendpoints " . __LINE__ . "\n";
        Schema::table('seendpoints', function (Blueprint $table) {
            $table->boolean("is_mobile")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "Corriendo rollback de la migracion " . __CLASS__ . "\n";

        echo "Borrando la columna is_mobile de la tabla Seendpoints " . __LINE__ . "\n";
        Schema::table('seendpoints', function (Blueprint $table) {
            $table->dropColumn("is_mobile");
        });

    }
}
