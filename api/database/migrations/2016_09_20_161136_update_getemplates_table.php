<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGetemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('getemplates', function (Blueprint $table){
            $table->longText('foo')->nullable();
        });
        $templates =  \App\Models\GE\Getemplate::all();
        for($i = 0; $i < count($templates); $i++){
            $templates[$i]->foo = $templates[$i]->cuerpomsg;
            $templates[$i]->save();
        }

        Schema::table('getemplates', function (Blueprint $table){
            $table->dropColumn('cuerpomsg');
            $table->renameColumn('foo', 'cuerpomsg');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
