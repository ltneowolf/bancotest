<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EndPointADDSortColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Corriendo migracion " . __CLASS__ . "\n";

        echo "Agregando la columna sort a la tabla seendpoints " . __LINE__ . "\n";
        Schema::table('seendpoints', function (Blueprint $table) {
            $table->integer('sort', false, true)->default('1')->nullable();
        });

        echo "Asignando el valor 1 a todos los registros de seendpoint ".__LINE__."\n";
        DB::statement('update seendpoints set sort=1');

        echo "Convirtiendo la columna sort a not null ".__LINE__."\n";
        Schema::table('seendpoints', function (Blueprint $table) {
            $table->integer('sort', false, true)->default('1')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "Rollback de la migracion " . __CLASS__ . "\n";

        echo "Eliminando la columna sort de seendpoint " . __LINE__ . "\n";
        Schema::table('seendpoints', function (Blueprint $table) {
            $table->dropColumn('sort');
        });
    }
}
