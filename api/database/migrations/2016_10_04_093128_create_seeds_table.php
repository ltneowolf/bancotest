<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Inicio de la migracion\n";
        echo "Creando tabla seeds\n";

        Schema::create('seeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
        });

        echo "Fin creacion tabla seeds\n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "Eliminando tabla seeds\n";

        Schema::dropIfExists('seeds');

        echo "Fin eliminacion tabla seeds\n";
    }
}
