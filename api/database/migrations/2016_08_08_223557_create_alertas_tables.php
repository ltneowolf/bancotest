<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertasTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Creando getipo_alertas\n";
        Schema::create('getipo_alertas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->unique('nombre');
            $table->string('obs');
            $table->timestamps();
        });

        echo "Creando gealertas\n";
        Schema::create('gealertas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->boolean('isUserLogged');
            $table->fastForeign('getipo_alertas');
            $table->fastForeign('getemplates');
            $table->string('siemprecc')->nullable();
            $table->string('siemprecco')->nullable();
            $table->timestamps();
        });

        Schema::create('getasks', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('alerta');
            $table->text('datos');
            $table->integer('intentos');
            $table->boolean('finalizado');
            $table->boolean('error');
            $table->text('traza')->nullable();
            $table->string('loggedusermail')->nullable();
            $table->integer('getemplate_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gealertas');
        Schema::drop('getipo_alertas');
        Schema::drop('getasks');
    }
}
