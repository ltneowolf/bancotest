<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GeTipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo "Creando tabla getipos \n";
        //Estados - Modulos
        Schema::create('getipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('grupo')->nullable();
            $table->timestamps();
        });

        //Relaciones
        Schema::table('getipos', function (Blueprint $table) {
            $table->fastForeign('gemodulos');
        });

        echo "Fin create getipos, row " . __LINE__ . " \n";
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo "Eliminando getipos\n";
        Schema::drop('getipos');
    }
}
