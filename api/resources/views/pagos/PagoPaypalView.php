<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <style>


        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-size: 16px;
            font-family: 'Open Sans', sans-serif;
            color: #4A4A4A;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            -webkit-box-shadow: 0px 2px 37px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 2px 37px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 0px 2px 37px 0px rgba(0, 0, 0, 0.75);
            border-radius: 30px 30px 30px 30px;
            -moz-border-radius: 30px 30px 30px 30px;
            -webkit-border-radius: 30px 30px 30px 30px;
            border: 0px solid #000000;
            height: 200px;
            width: 400px;
            font-size: 13px;
        }

        #container {
            height: 100%;
            width: 100%;
            border-collapse: collapse;
            display: table;
        }

        .myButton {
            -moz-box-shadow: 0px 10px 14px -7px #1d5861;
            -webkit-box-shadow: 0px 10px 14px -7px #1d5861;
            box-shadow: 0px 10px 14px -7px #1d5861;
            background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #599bb3), color-stop(1, #213894));
            background:-moz-linear-gradient(top, #599bb3 5%, #213894 100%);
            background:-webkit-linear-gradient(top, #599bb3 5%, #213894 100%);
            background:-o-linear-gradient(top, #599bb3 5%, #213894 100%);
            background:-ms-linear-gradient(top, #599bb3 5%, #213894 100%);
            background:linear-gradient(to bottom, #599bb3 5%, #213894 100%);
            filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#599bb3', endColorstr='#213894',GradientType=0);
            background-color:#599bb3;
            -moz-border-radius:8px;
            -webkit-border-radius:8px;
            border-radius:8px;
            display:inline-block;
            cursor:pointer;
            color:#ffffff;
            font-family:Arial;
            font-size:12px;
            font-weight:bold;
            padding:10px 15px;
            text-decoration:none;
            text-shadow:0px 1px 0px #293da3;
        }


        .foot {
            display: table-row;
            vertical-align: bottom;
            height: 1px;
            margin-bottom: 10px;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="content">


        <div class="title">


            <div id="container">
                <div style="
                    margin-top: 15px;"
                ><?php
                    echo $msg
                    ?></div>

                <div class="foot">
                    <button type="button" class="myButton"
                            onclick="window.location.replace('<?php
                            echo env('APP_URL', 'http://localhost:4200/')
                            ?>ordenpago/')">Aceptar
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>