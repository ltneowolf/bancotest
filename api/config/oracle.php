<?php

return [
    'oracle' => [
        'driver'   => 'oracle',
        'tns'      => env('DB_TNS', ''),
        'host'     => env('DB_HOST', ''),
        'port'     => env('DB_PORT', ''),
        'database' => env('DB_DATABASE', 'xe'),
        'username' => env('DB_USERNAME', ''),
        'password' => env('DB_PASSWORD', ''),
        'charset'  => 'AL32UTF8',
        'prefix'   => '',
    ]
];