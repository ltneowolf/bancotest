<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use DB;
use Response;

class EndpointChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //procesamos los demás middlewares
        //$result = $next($request);
        //consultamos la información de la ruta
        $ruta = $request->route()->uri();
        //Debido a que las rutas del put o patch llegan users/{users} hago un split o explode
        $ruta = explode('/', $ruta)[0];

        if ($ruta == 'login') {
            return $next($request);
        }

        $method = $request->route()->methods()[0];

        $userLogin = JWTAuth::toUser();

        $permisos = DB::table('users')
            ->where('users.id', $userLogin->id)
            ->join('seuser_roles', 'users.id', '=', 'seuser_roles.user_id')
            ->join('seendpoint_methods', 'seuser_roles.serole_id', '=', 'seendpoint_methods.serole_id')
            ->join('seendpoints', 'seendpoint_methods.seendpoint_id', '=', 'seendpoints.id')
            ->where('seendpoints.state', $ruta)
            ->where(function ($query) use ($method) {
                $query->whereRaw('upper(seendpoint_methods.method) = ?', array($method))
                    ->orWhere('seendpoint_methods.method', '*');
            })
            ->count();

        if ($permisos > 0)
            return $next($request);

        return Response::make(['message' => 'UNAUTHORIZED'], 401);
    }
}
