<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 10/08/2016
 * Time: 4:43 PM
 * Rutas de las app que utilizan el Core
 */

$customResource('clientes', 'TEST\ClienteController');
Route::post('clientes/reporte', 'WHR\ClienteController@reporte');

$customResource('cuentas', 'TEST\CuentaAhorroController');
Route::get('cuentas/reporte', 'WHR\CtaAhoTransaccionesController@reporte');
$customResource('transacciones', 'TEST\CtaAhoTransaccionesController');
Route::get('transacciones/reporte', 'WHR\CtaAhoTransaccionesController@reporte');