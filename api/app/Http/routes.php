<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$customResource = function($resource, $controller){
    Route::get("$resource/dt", "$controller@dt");
    Route::get("$resource/lv", "$controller@lv");
    Route::get("$resource/lv2", "$controller@lv2");
    Route::get("$resource/xls", "$controller@xls");
    Route::resource("$resource", "$controller");
};

Route::get('/', function () {
    return view('welcome');
});

Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
Route::post('recordar', 'AuthController@recordar');
Route::get('users/{id}', 'UsersController@show');
$customResource('usersCRUD', 'UsersController');
$customResource('roles', 'Core\SE\RolesController');
$customResource('userroles', 'Core\SE\UserRolesController');
$customResource('endPoints', 'Core\SE\EndPointController');
$customResource('endPointsMethod', 'Core\SE\EndPointMethodController');

$customResource('lang', 'Core\GE\LangController');
Route::get('langs', 'Core\GE\LangController@languages');
$customResource('langDef', 'Core\GE\LangDefController');
$customResource('moneda', 'Core\GE\MonedaController');
Route::post('convertirMoneda', 'Core\GE\MonedaController@convertirMonedaEndPoint');
$customResource('modulos', 'Core\GE\ModuloController');
$customResource('geparameters', 'Core\GE\GeparametersController');
Route::get('geparametersServ', 'Core\GE\GeparametersController@index');
$customResource('template', 'Core\GE\TemplatesController');
$customResource('alerta', 'Core\GE\AlertaController');
$customResource('tipoAlerta', 'Core\GE\TipoAlertaController');

$customResource('paises', 'GE\PaisesController');
Route::get('consultaCiudad/buscar/{texto}', 'GE\CiudadeController@buscarCiudad');
$customResource('consultaCiudad', 'GE\CiudadeController');
$customResource('departamentos', 'GE\DepartamentoController');
$customResource('estados', 'GE\EstadoController');
$customResource('tipos', 'GE\TiposController');
$customResource('tiposOcult', 'GE\TiposController');

Route::get('pruebasinc', 'PruebasController@probarAsincrono');

$customResource('pruebas', 'PruebasController');

Route::get('logReporte', 'Core\GE\LogController@reporte');

Route::put('changepassword', 'UsersController@cambiarPassword');
Route::put('editarperfil', 'UsersController@editPerfil');
Route::post('emailprueba', 'UsersController@emailPrueba');

include('appRoutes.php');