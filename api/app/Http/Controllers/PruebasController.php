<?php

namespace App\Http\Controllers;

use App\Models\GE\Geagencia;
use App\Util\EventDispatcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class PruebasController extends BaseController
{


    /**
     * PruebasController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    function setModel()
    {
        // TODO: Implement setModel() method.
        $this->model = '\App\Models\Prueba';
    }

    function setEager()
    {
        $this->eager = array('paises');
    }

    public function probarAsincrono(Request $request){
        EventDispatcher::dispatchEvent("ALERTA_PRUEBA", array());
        EventDispatcher::dispatchEvent("MANUAL_MAIL", array(),"luygui.oficinaz@gmail.com", 4);
        return "Ok";
    }

    public function probarCheckPass(Request $request){

        $agen = Geagencia::find($request->agencia);

        $agen->claveagencia = $request->clave;

        $agen->save();

        /*if (!Hash::check($request->clave, $agen->claveagencia)) {
            return "NO Paso";
        }
            return "Si paso";*/



    }

}
