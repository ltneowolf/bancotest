<?php

namespace App\Http\Controllers\GE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EstadoController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array ('nombre',  'gemodulo_id' );
        $this->onlyUpdate = array('nombre',  'gemodulo_id' );
    }

    public function setModel()
    {
        $this->model = '\App\Models\GE\Geestado';
    }

    public function setEager()
    {
        $this->eager = array('modulo');
    }

}