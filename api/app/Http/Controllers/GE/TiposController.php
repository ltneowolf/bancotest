<?php

namespace App\Http\Controllers\GE;

use App\Http\Controllers\BaseController;

class TiposController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('nombre', 'gemodulo_id', 'grupo', 'abreviatura');
        $this->onlyUpdate = array('nombre', 'gemodulo_id', 'grupo', 'abreviatura');
    }

    public function setModel()
    {
        $this->model = '\App\Models\GE\Getipo';
    }

    public function setEager()
    {
        $this->eager = array('modulo');
    }

    public function setWhereable()
    {
        $this->whereable = array('gemodulo_id', 'grupo');
    }
}
