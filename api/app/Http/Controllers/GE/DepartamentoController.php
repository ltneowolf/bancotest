<?php

namespace App\Http\Controllers\GE;

use App\Http\Controllers\BaseController;

class DepartamentoController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('nombre', 'abreviatura', 'gepaise_id');
        $this->onlyUpdate = array('nombre', 'abreviatura', 'gepaise_id');
    }

    public function setModel()
    {
        $this->model = '\App\Models\GE\Gedepartamento';
    }

    public function setEager()
    {
        $this->eager = array('pais');
    }

    public function getcustomRules($request)
    {
        return array(
            array('nombre' => 'required|alpha_spaces',
                'gepaise_id' => 'required',
                'abreviatura' => 'alpha|unique:gedepartamentos,abreviatura,id|max:10'),
            array('nombre.required' => 'REQUIRED_:attribute',
                'nombre.alpha_spaces' => 'ALPHA_SPACE_:attribute',
                'gepaise_id.required' => 'REQUIRED_PAIS_ID_:attribute',
                'abreviatura.alpha' => 'ALPHA_:attribute',
                'abreviatura.unique' => 'UNIQUE_:attribute')
        );
    }
}
