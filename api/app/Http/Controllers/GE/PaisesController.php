<?php

namespace App\Http\Controllers\GE;

use App\Http\Controllers\BaseController;

class PaisesController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('nombre', 'abreviatura');
        $this->onlyUpdate = array('nombre', 'abreviatura');
    }

    public function setModel(){
        $this->model = '\App\Models\GE\Gepaise';
    }

    public function setEager(){
        //requisito necesario para hacer herencia de base controller
        
    }

    public function getcustomRules($request)
    {
        return array(
            array('nombre' => 'required|alpha_spaces',
                    'abreviatura' => 'required|alpha_spaces|unique,gepaises,abreviatura,:id'
                    ),
            array('nombre.required' => 'REQUIRED_:attribute',
                'abreviatura.required' => 'REQUIRED_:attribute',
                'abreviatura.alpha_spaces' => 'ALPHA_SPACE_:attribute',
                'abreviatura.unique' => 'UNIQUE_:attribute',
                'nombre.alpha_spaces' => 'ALPHA_SPACE_:attribute')
        );
    }
}


