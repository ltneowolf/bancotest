<?php

namespace App\Http\Controllers\GE;

use App\Http\Controllers\BaseController;
use App\Models\GE\Geciudade;
use Input;

class CiudadeController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('nombre', 'abreviatura', 'gedepartamento_id');
        $this->onlyUpdate = array('nombre', 'abreviatura', 'gedepartamento_id');
    }

    public function setModel()
    {
        $this->model = '\App\Models\GE\Geciudade';
    }

    public function setEager()
    {
        $this->eager = array('departamento', 'departamento.pais');

    }

    public function buscarCiudad($texto)
    {
        $listado = Geciudade::with('departamento', 'departamento.pais')
            ->whereRaw('lower(nombre) like (?)',["%{$texto}%"])->get();

        $ciudades = array();

        foreach ($listado as $ciudad) {
            $ciudades[] = array(
                'ciudad' => array(
                    'gedepartamento_id' => $ciudad->gedepartamento_id,
                    'id' => $ciudad->id,
                    'nombre' => $ciudad->nombre,
                ),
                'ciudad_id' => $ciudad->id,
                'estado' => $ciudad->departamento,
                'estado_id' => $ciudad->departamento->id,
                'pais' => $ciudad->departamento->pais,
                'pais_id' => $ciudad->departamento->pais->id,
                'zona' => '',
            );
        }

        return response()->json($ciudades, 200);
    }

    public function getcustomRules($request)
    {
        return array(
            array('nombre' => 'required|alpha_spaces',
                'gedepartamento_id' => 'required',
                'abreviatura' => 'required|alpha|unique:geciudades,abreviatura,:id|max:10'),
            array('nombre.required' => 'REQUIRED_:attribute',
                'nombre.alpha_spaces' => 'ALPHA_SPACE_:attribute',
                'gedepartamento_id.required' => 'REQUIRED_DEPT_ID_:attribute',
                'abreviatura.required' => 'REQUIRED_:attribute',
                'abreviatura.alpha' => 'ALPHA_:attribute',
                'abreviatura.unique' => 'UNIQUE_:attribute')
        );
    }
}
