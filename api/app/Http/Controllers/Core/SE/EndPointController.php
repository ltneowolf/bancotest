<?php
/**
 * Created by PhpStorm.
 * Date: 29/04/2016
 * Time: 10:50 AM
 */

namespace App\Http\Controllers\Core\SE;


use App\Http\Controllers\BaseController;

class EndPointController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('state', 'menu_label', 'page_title', 'page_desc', 'seendpoint_id', 'show', 'icon',
            'sort', 'is_mobile');
        $this->onlyUpdate = $this->onlyStore;
    }

    function setModel()
    {
        $this->model = "\App\Models\SE\Seendpoint";
    }

    function setEager()
    {
        $this->eager = array("endPointParent");
    }

}