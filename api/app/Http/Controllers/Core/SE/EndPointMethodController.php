<?php
/**
 * Created by PhpStorm.
 * Date: 29/04/2016
 * Time: 01:35 PM
 */

namespace App\Http\Controllers\Core\SE;


use App\Http\Controllers\BaseController;

class EndPointMethodController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('method', 'seendpoint_id', 'serole_id');
        $this->onlyUpdate = array('method', 'seendpoint_id', 'serole_id');
    }

    function setModel()
    {
        $this->model = "\App\Models\SE\SeendpointMethod";

    }

    function setEager()
    {
        $this->eager = array("rol", "endpoint");
    }

    public function setWhereable()
    {
      $this->whereable = array('serole_id');
    }
}