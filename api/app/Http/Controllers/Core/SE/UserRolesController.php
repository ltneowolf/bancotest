<?php

    namespace App\Http\Controllers\Core\SE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\SE\SeuserRole;
use stdClass;
class UserRolesController extends BaseController
{

    //TODO agregar custom rules
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('user_id', 'serole_id');
        $this->onlyUpdate = array('user_id', 'serole_id');
        //$this->customRules();
    }

    public function setModel(){
        $this->model = '\App\Models\SE\SeuserRole';
    }

    public function setEager(){
        //requisito necesario para hacer herencia de base controller
        $this->eager = array('user','rol');
    }

    public function setWhereable()
    {
        $this->whereable = array('user_id');
    }
}
