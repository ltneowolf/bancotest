<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;


class AlertaController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('isuserlogged', 'getipo_alerta_id', 'getemplate_id', 'siemprecc', 'siemprecco');
        $this->onlyUpdate = array('isuserlogged', 'getipo_alerta_id', 'getemplate_id', 'siemprecc', 'siemprecco');
    }

    function setModel()
    {
        $this->model = "\App\Models\GE\Gealerta";
    }

    function setEager()
    {
        $this->eager = array("tipoAlerta", "template");
    }

    public function getcustomRules($request)
    {
        return array(
            array('isuserlogged' => 'required',
                'getipo_alerta_id' => 'required',
                'siemprecc' => 'max:4000',
                'siemprecco' => 'max:4000',
                'getemplate_id' => 'required'
            ),
            array('isuserlogged.required' => 'REQUIRED_:attribute',
                'getipo_alerta_id.required' => 'REQUIRED_:attribute',
                'siemprecc.max' => 'MAX_:attribute',
                'siemprecco.max' => 'MAX_:attribute',
                'getemplate_id.required' => 'REQUIRED_:attribute')
        );
    }


}
