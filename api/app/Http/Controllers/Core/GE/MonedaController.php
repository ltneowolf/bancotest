<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use App\Models\GE\Gemoneda;
use Illuminate\Http\Request;
use App\Http\Requests;


class MonedaController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('nombre', 'simbolo', 'abreviatura', 'factor_conversion', 'ajuste', 'gemoneda_id');
        $this->onlyUpdate = array('nombre', 'simbolo', 'abreviatura', 'factor_conversion', 'ajuste', 'gemoneda_id');
    }

    public function setModel()
    {
        $this->model = '\App\Models\GE\Gemoneda';
    }

    public function setEager()
    {

    }

    public function getcustomRules($request)
    {
        return array(
            array('nombre' => 'required|alpha_spaces',
                'simbolo' => 'required',
                'abreviatura' => 'required|unique:gemonedas,abreviatura,:id',
                'factor_conversion' => 'required'),
            array('nombre.required' => 'REQUIRED_:attribute',
                'simbolo.required' => 'REQUIRED_:attribute',
                'abreviatura.required' => 'REQUIRED_:attribute',
                'abreviatura.unique' => 'UNIQUE_:attribute',
                'factor_conversion.required' => 'REQUIRED_:attribute',
                'nombre.alpha_spaces' => 'ALPHA_SPACE_:attribute')
        );
    }

    /**
     * Recibe un objeto con la siguiente estructura:
     *{
     *   "aMonedaTodas":2, //Este campo me dice a que moneda se va a convertir todas las cantidades del vector
     *   "monedas": [
     *       {
     *           "cantidad": 310,
     *           "demoneda":"1",
     *           "amoneda":"3" //Si en el vector existe este campo entonces se toma esta moneda y no la de aMonedaTodas
     *       },
     *       {
     *           "cantidad": 310,
     *           "demoneda":"1"
     *       }
     *   ]
     *}
     */
    public function convertirMonedaEndPoint(Request $request)
    {
        $data = $request->all();
        $aMonedaTodas = isset($data['aMonedaTodas']) ? $data['aMonedaTodas'] : '';
        $resultado = [];

        //Recorro las monedas para convertirlas
        foreach ($data["monedas"] as $moneda) {

            //En caso de que la moneda no tenga el campo amoneda entonces se utiliza el campo aMonedasTodas
            if (!isset($moneda["amoneda"])) {
                $moneda["amoneda"] = $aMonedaTodas;
            }

            //Si no fue seteada la moneda a la que se desea convertir entonces se retorna el error
            if ($moneda["amoneda"] == '') {
                $conversion = array("error" => "CURRENCY_NOT_FOUND");
            } else {
                $conversion = $this->convertirMoneda($moneda["cantidad"], $moneda["demoneda"], $moneda["amoneda"]);
            }

            array_push($resultado, $conversion);

        }


        return response()->json($resultado, 200);

    }

    public static function convertirMoneda($cantidad, $demoneda, $amoneda, $sumaAjuste = false)
    {

        $whereAmoneda = is_numeric($amoneda) ? "id" : "abreviatura";
        $whereDemoneda = is_numeric($demoneda) ? "id" : "abreviatura";

        $consultaAMoneda = Gemoneda::Where($whereAmoneda, "=", strtoupper($amoneda))
            ->get(["id", "factor_conversion", "abreviatura", "nombre", "simbolo"])->first();

        if ($demoneda != $amoneda) {

            $consultaDeMoneda = Gemoneda::Where($whereDemoneda, "=", strtoupper($demoneda))
                ->get(["id", "factor_conversion", "abreviatura", "nombre", "simbolo", "ajuste"])->first();

            if (is_null($consultaAMoneda) || is_null($consultaDeMoneda)) {
                return array("error" => "CURRENCY_NOT_FOUND");
            }

            $factorConvAmoneda = $consultaAMoneda["factor_conversion"];
            $factorConvDemoneda = $consultaDeMoneda["factor_conversion"];

            if ($factorConvDemoneda == 0) {
                return array("error" => "CURRENCY_VALUE_IS_CERO");
            }

            if ($sumaAjuste) {
                $factorConvDemoneda += $consultaDeMoneda["ajuste"];
            }

            //Cambiar hacia Dolares
            if ($consultaAMoneda["abreviatura"] == 'USD') {
                $nuevaCantidad = $cantidad / $factorConvDemoneda;
            } elseif ($consultaDeMoneda["abreviatura"] == 'USD') { //Desde dolares
                $nuevaCantidad = $cantidad * $factorConvAmoneda;
            } else { //Hacia una divisa diferente de dolares
                $nuevaCantidad = ($cantidad / $factorConvDemoneda) * $factorConvAmoneda;
            }

            return array("cantidad" => round($nuevaCantidad, 2),
                "id" => $consultaAMoneda["id"],
                "nombre" => $consultaAMoneda["nombre"],
                "moneda" => $consultaAMoneda["abreviatura"],
                "simbolo" => $consultaAMoneda["simbolo"]);
        }


        return array("cantidad" => round($cantidad, 2),
            "id" => $consultaAMoneda["id"],
            "nombre" => $consultaAMoneda["nombre"],
            "moneda" => $consultaAMoneda["abreviatura"],
            "simbolo" => $consultaAMoneda["simbolo"]);

    }

}
