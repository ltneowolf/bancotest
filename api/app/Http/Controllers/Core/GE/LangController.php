<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\GE\Gelanguage;

use Input;

class LangController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('lang', 'label', 'icon');
        $this->onlyUpdate = array('lang', 'label', 'icon');
        unset($this->middleware);
        $this->middleware = array();
        //dd( $this->middleware);
    }

    public function setModel(){
        $this->model = '\App\Models\GE\Gelanguage';
    }

    public function setEager(){
        //requisito necesario para hacer herencia de base controller
        $this->eager = array();
    }

    public function languages(){

        return parent::index();
    }
    public function index()
    {   
        if(Input::has('lang')){
            return $this->show(Input::get('lang'));
        }
        $langs = parent::index();
        $result = array();
        foreach ($langs as $lang) {
            $result[$lang->lang] = $lang;
        }
        return $result;
    }

    public function show($id){
        $lang = Gelanguage::where('lang', $id)->first();
        if( $lang ){
            $result = array();
            $defs = $lang->defs()->get();
            
            foreach ($defs as $item) {
                $result[$item->key] = $item->value;
            }
            return response($result)->header('Cache-Control', 'public, max-age=3600');
        }else{
            return array();
        }
    }
}
