<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GeparametersController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('key', 'numval', 'strval', 'observation','gemodulo_id');

        $this->onlyUpdate = array('key', 'numval', 'strval', 'observation','gemodulo_id');

    }


    function setModel()
    {
        $this->model = '\App\Models\GE\Geparameters';
    }


    function setEager()
    {
        $this->eager = array('modulo');
    }

    public function getcustomRules($request)
    {
        return array(
            array('key' => 'required|unique:geparameters,key,:id',
                'observation' => 'required'),

            array('key.required' => 'REQUIRED_:attribute',
                'observation.required' => 'REQUIRED_:attribute',
                'key.unique' => 'UNIQUE_:attribute')
        );
    }

}
