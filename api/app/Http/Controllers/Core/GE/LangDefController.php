<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LangDefController extends BaseController
{
     public function __construct()
    {
        parent::__construct();
        $this->onlyStore =  array('key', 'value', 'gelanguage_id');
        $this->onlyUpdate = array('key', 'value', 'gelanguage_id');
    }

    public function setModel(){
        $this->model = '\App\Models\GE\GelangDef';
    }

    public function setEager(){
        //requisito necesario para hacer herencia de base controller
        $this->eager = array('language');
    }

    

}