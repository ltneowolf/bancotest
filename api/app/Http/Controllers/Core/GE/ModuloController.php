<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ModuloController  extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array ('nombre' );
        $this->onlyUpdate = array('nombre' );
    }

    public function setModel()
    {
        $this->model = '\App\Models\GE\Gemodulo';
    }

    public function setEager()
    {
        // $this->eager = array('pais');
    }


}