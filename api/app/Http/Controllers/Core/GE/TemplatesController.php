<?php

namespace App\Http\Controllers\Core\GE;

use App\Models\GE\Getemplate;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;

class TemplatesController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = ['nombre', 'asunto', 'cuerpomsg'];
        $this->onlyUpdate = ['nombre', 'asunto', 'cuerpomsg'];
    }

    public function setWhereable()
    {
        $this->whereable = ['nombre'];
    }

    public function setModel()
    {
        $this->model = '\App\Models\GE\Getemplate';
    }

    public function setEager()
    {
        $this->eager = array();
    }

    public function getcustomRules($request)
    {
        return array(
            array('nombre' => 'required|unique:getemplates,nombre,:id|max:255',
                'asunto' => 'required|max:255',
                'cuerpomsg' => 'required|max:20000'),
            array('nombre.required' => 'REQUIRED_:attribute',
                'nombre.size' => 'MAX_:attribute',
                'asunto.size' => 'MAX_:attribute',
                'asunto.required' => 'REQUIRED_:attribute',
                'nombre.unique' => 'UNIQUE_:attribute',
                'siemprecc.size' => 'MAX_:attribute',
                'siemprecco.size' => 'MAX_:attribute',
                'cuerpomsg.size' => 'MAX_:attribute',
                'cuerpomsg.required' => 'REQUIRED_:attribute')
        );
    }

    public function store(Request $request)
    {
        $val = $this->isValid($request);
        $data = $request->all();
        if ($val !== true) {
            return $this->makeResponse($val, 400);
        }
        $busqueda = Getemplate::where("UPPER(nombre)", strtoupper($data['nombre']))->first();
        if ($busqueda)
            return response()->json(['result' => 'RECORD_EXISTS', "status" => false], 200);


        else
            try {
                $record = Getemplate::create($data);
            } catch (\Exception $ex) {
                return $this->handleEx($ex, $data, $request);
            }
        return $record;
    }

}
