<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use App\Models\GE\Gelog;
use Illuminate\Http\Request;

use JWTAuth;
use Validator;
use Log;
use DB;

class LogController extends BaseController
{
    function __construct()
    {
        parent::__construct();

        $this->onlyStore = array();
        $this->onlyUpdate = array();
    }

    function store(Request $request)
    {
        return $this->makeResponse(array("ERROR" => "Method Not Allowed"), 405);
    }

    function update(Request $request, $id)
    {
        return $this->makeResponse(array("ERROR" => "Method Not Allowed"), 405);
    }

    function destroy(Request $request, $id)
    {
        return $this->makeResponse(array("ERROR" => "Method Not Allowed"), 405);
    }

    function reporte(Request $request)
    {
        $rqData = $request->all();

        $rules = array(
            array('fechaInicio' => 'required',
                'fechaFin' => 'required'
            ),
            array('fechaInicio.required' => 'REQUIRED_:attribute',
                'fechaFin.required' => 'REQUIRED_:attribute'
            )
        );

        $validator = Validator::make($rqData,
            $rules[0],
            $rules[1]);

        if ($validator->fails()) {
            $errores = $validator->errors()->toArray();
            return $this->makeResponse($errores, 400);
        }

        $rqData["fechaInicio"] .= ' 00:00:00';
        $rqData["fechaFin"] .= ' 23:59:59';

        $glogModel = DB::table('gelogs')
            ->join('getipos gt', 'gelogs.proceso_id', '=', 'gt.id')
            ->join('gemodulos gm', 'gt.gemodulo_id', '=', 'gm.id')
            ->join('users usr', 'gelogs.user_id', '=', 'usr.id');

        $glogModel->whereBetween('gelogs.created_at', [$rqData["fechaInicio"], $rqData["fechaFin"]]);

        if (isset($rqData["ref_registro"]) && $rqData["ref_registro"] != "") {
            $glogModel->where("gelogs.ref_registro", "=", $rqData["ref_registro"]);
        }

        if (isset($rqData["proceso_id"]) && $rqData["proceso_id"] != "") {
            if ($rqData["proceso_id"] != "-1234567") {
                $glogModel->where("gelogs.proceso_id", "=", $rqData["proceso_id"]);
            }
        }

        $glogModel->select([
            "gelogs.created_at fecha",
            "gelogs.ref_registro",
            "gelogs.mensaje",
            "gm.nombre modulo",
            "gt.nombre proceso",
            "usr.name usuario",
        ]);

        return $this->makeResponse($glogModel->get(), 200);

    }

    function setModel()
    {
        $this->model = '\App\Models\GE\Gelog';
    }

    function setEager()
    {
        $this->eager = array('modulo', 'user', 'proceso', 'proceso.modulo');
    }

}
