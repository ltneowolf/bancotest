<?php

namespace App\Http\Controllers\Core\GE;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Requests;

class TipoAlertaController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('nombre', 'obs');
        $this->onlyUpdate = array('nombre', 'obs');
    }

    function setModel()
    {
        $this->model = '\App\Models\GE\GetipoAlerta';
    }

    function setEager()
    {
        // TODO: Implement setEager() method.
    }

    public function getcustomRules($request)
    {
        return array(
            array('nombre' => 'required|unique:getipo_alertas,nombre,:id|max:255',
                'obs' => 'required|max:255'
            ),
            array('nombre.required' => 'REQUIRED_:attribute',
                'nombre.unique' => 'UNIQUE_:attribute',
                'nombre.max' => 'MAX_:attribute',
                'obs.size' => 'MAX_:attribute',
                'obs.required' => 'REQUIRED_:attribute')
        );
    }

}
