<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Util\EventDispatcher;
use App\Util\SystemParameters;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Response;

//JWT
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function login(Request $request)
    {
        //$results = DB::select('select 1 from dual');
        $credentials = $request->only('email', 'password');
        try {
            // verify the credentials and create a token for the user
            $credentials['email'] = strtolower($credentials['email']);
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['message' => 'INVALID_CREDENTIALS'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['message' => 'COULD_NOT_CREATE_TOKEN'], 500);
        }

        // if no errors are encountered we can return a JWT
        $result = array('token' => $token,
            'userId' => JWTAuth::toUser($token)->id);
        return response()->json($result);
    }

    public function logout(Request $request)
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->json('SESSION_FINISHED');
    }

    public function recordar(Request $request)
    {
        $nuevaClave = strtolower(str_random(7));
        $user = User::where('email', $request->get('email'))->first();

        $user->password = $nuevaClave;
        $user->cambiar_password = true;
        try {
            $user->save();
        } catch (\Exception $ex) {
            return Response::make(["message" => "INTERNAL_ERROR"], 500);
        }

        $params = array(
            "[NOMUSER]" => $user->name,
            "[CORRUSER]" => $user->email,
            "[CLAUSER]" => $nuevaClave,
            //"[URLBOXEX]" => url() . SystemParameters::getStrSysParam('LOGO_APP'),//'/BOXEX_EMAIL.png',
            "[URLAPP]" => SystemParameters::getStrSysParam('URL_APP'),
            "[CORREO_CONTACTO]" => SystemParameters::getStrSysParam('CORREO_SERVICIO_CLIENTE')
        );

        EventDispatcher::dispatchEvent("RECORDAR_CONTRASEÑA", $params, $user->email);

        return Response::make(["OK"], 200);
    }
}
