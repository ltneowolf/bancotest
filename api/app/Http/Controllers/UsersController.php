<?php

namespace App\Http\Controllers;

use App\Util\UtilApp\UserUtil;
use Illuminate\Http\Request;


//JWT
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Log;

class UsersController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->onlyStore = array('email', 'name', 'password', 'password_confirmation');
        $this->onlyUpdate = array('name', 'password', 'password_confirmation');
    }

    public function setModel()
    {
        $this->model = '\App\Models\User';
    }

    public function setEager()
    {
        //requisito necesario para hacer herencia de base controller
        $this->eager = array('seroles');
    }

    public function getcustomRules($request)
    {

        if ($this->isStore($request)) {
            return array(
                array('name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users,email,:id',
                    'password' => 'required|confirmed|min:6'),

                array('name.required' => 'REQUIRED_:attribute',
                    'email.required' => 'REQUIRED_:attribute',
                    'email.max' => 'MAX_:attribute',
                    'email.email' => 'EMAIL_:attribute',
                    'email.unique' => 'UNIQUE_:attribute',
                    'password.required' => 'REQUIRED_:attribute',
                    'password.min' => 'MIN_:attribute')
            );

        } else if ($this->isUpdate($request)) {

            return array(
                array('name' => 'required'),
                array('name.required' => 'REQUIRED_:attribute')
            );

        }
    }

    public function index()
    {
        $usuario = JWTAuth::toUser();
        if ($usuario->isAdmin()) {
            return parent::index();
        }
        return $this->makeResponse(['message' => 'UNAUTHORIZED'], 401);
    }

    public function show($id)
    {
        $usuario = JWTAuth::toUser();

        if ($usuario->id == $id || $usuario->isAdmin() != false) {
            //array_push($this->eager, 'getEndpointsAttribute');
            $this->model = '\App\Models\UserDetailed';
            $respShow = parent::show($id);

            return $respShow;
        }

        return $this->makeResponse(['message' => 'UNAUTHORIZED'], 401);
    }

    public function cambiarPassword(Request $request)
    {
        $newClienteWebRules = array(
            array(
                'oldPass' => 'required',
                'newPass' => 'required|min:6|regex:/^(?=.*[A-Z])(?=.*\d).+$/',
                'confirmPass' => 'required'
            ),
            array(
                'oldPass.max' => 'FIELD_REQUIRED',
                'newPass.required' => 'FIELD_REQUIRED',
                'confirmPass.max' => 'FIELD_REQUIRED',
                'newPass.min' => 'PASS_MIN_ERROR',
                'newPass.regex' => 'PASS_RUL_BAS_ERROR',
            )
        );

        $validador = Validator::make($request->all(), $newClienteWebRules[0], $newClienteWebRules[1]);

        if ($validador->fails()) {

            $errores = $validador->errors();

            return response()->json($errores, 400);
        }

        $cambio = UserUtil::changePassword($request->oldPass, $request->newPass);
        if (array_key_exists("ERROR", $cambio)) {
            return $this->makeResponse($cambio["ERROR"], 400);
        }

        $user = JWTAuth::toUser();

        $user->cambiar_password = false;
        $user->save();

        return $this->makeResponse(array("data" => $cambio["SUCCESS"]), 200);
    }

    /*
     * Funcion de edicion de perfil del usuario,
     * */
    public function editPerfil(Request $request)
    {
        $usuario = JWTAuth::toUser();

        $valid = array(
            array('name' => 'required',
                'email' => 'required|unique:users,email,' . $usuario->id
            ),
            array('name.required' => 'REQUIRED_:attribute',
                'email.required' => 'REQUIRED_:attribute',
                'email.unique' => 'INVALID_:attribute'
            )
        );

        $modificar = array('name', 'email');

        $validate = Validator::make($request->all(),
            $valid[0],
            $valid[1]);

        if ($validate->fails()) {
            return $this->makeResponse($validate->errors(), 400);
        }

        $usuario = JWTAuth::toUser();

        try {
            $usuario->fill($request->only($modificar));

            $usuario->save();

            return $this->makeResponse('INFO_EDIT_LABEL', 200);

        } catch (\Exception $ex) {
            return $this->makeResponse(['message' => $ex->getMessage()], 400);
        }
    }

}