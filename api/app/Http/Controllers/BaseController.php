<?php

namespace App\Http\Controllers;

use App\Models\GE\Geparameters;
use App\Util\SystemParameters;
use Illuminate\Http\Request;

use App\Http\Requests;
use Log;
use Response;
use Validator;
use Input;

use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\Route;

use App\Util\ValidationUtils;
use Tymon\JWTAuth\Facades\JWTAuth;

abstract class BaseController extends Controller
{
    protected $model;
    protected $eager;

    protected $onlyStore;
    protected $onlyUpdate;

    /* Parametros que pueden ser utilziados para filtrar el recurso en el index */
    protected $whereable;

    abstract function setModel();

    abstract function setEager();

    public function setWhereable()
    {
        $this->whereable = array();
    }

    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth');
        $this->middleware('endpointChecker');
        //inicializo el modelo para el codigo dinamico
        $this->setModel();
        $this->setEager();
        $this->setWhereable();
    }

    function value_is_not_null($val)
    {
        return !(is_null($val) || $val == '');
    }

    /**
     * Display a listing of the resource.
     *
     * Si se envian los parametros q_max y q_page el sistema pagina la respuesta
     *
     * q_max    -Opcional, indica la cantidad de registros a recibir.
     * q_page   -Opcional, indica la pagina que se debe envíar.
     *
     * @return \Illuminate\Http\Response
     * Si la respuesta está paginada devuelve el siguiente objeto:
     *
     * total            Integer, cantidad de registros en la consulta
     * current_page     Integer, pagina actual.
     * last_page        Integer, ultima pagina.
     * per_page         Integer, ultima pagina.
     * data             array, registros de la consulta
     *
     * Si no se pagina la respuesta entonces el sistema retorna toda la data.
     */
    public function index()
    {
        $result = array();

        /**
         * Metodo encargado de generar paginacion a una consulta.
         * @param $result
         * @return array
         */
        $paginar = function ($result) {
            $max = Input::get('q_max');
            $page = Input::get('q_page');
            $skip = $max * ($page - 1);
            $total = $result->count();
            $max_page = ceil(($total / $max));
            return [
                "total" => $total,
                "current_page" => $page,
                "last_page" => $max_page,
                "per_page" => $max * 1,
                "data" => $result->skip($skip)->take($max)->get(),
            ];
        };

        if (empty($this->eager)) {
            if (empty($this->whereable)) {
                if (Input::has("q_max") && Input::has("q_page")) {
                    $result = $paginar(new $this->model);
                } else {
                    $result = call_user_func($this->model . '::all');
                }
            } else {
                $filterValues = array_filter(Input::only($this->whereable), array(__CLASS__, 'value_is_not_null'));
                if (!empty($filterValues)) {
                    $i = 0;
                    foreach ($filterValues as $col => $value) {
                        if ($i == 0) {
                            $i = 1;
                            $result = call_user_func($this->model . '::where', $col, $value);
                        } else {
                            $result = $result->where($col, $value);
                        }
                    }

                    if (Input::has("q_max") && Input::has("q_page")) {
                        $result = $paginar($result);
                    } else {
                        $result = $result->get();
                    }
                } else {
                    if (Input::has("q_max") && Input::has("q_page")) {
                        $result = $paginar(new $this->model);
                    } else {
                        $result = call_user_func($this->model . '::all');
                    }
                }
            }
        } else {
            $result = call_user_func($this->model . '::with', $this->eager);
            $filterValues = array_filter(Input::only($this->whereable), array(__CLASS__, 'value_is_not_null'));
            if (!empty($filterValues)) {
                foreach ($filterValues as $col => $value) {
                    $result = $result->where($col, $value);
                }
            }
            if (Input::has("q_max") && Input::has("q_page")) {
                $result = $paginar($result);
            } else {
                $result = $result->get();
            }
        }

        return $result;
    }

    /**
     * Metodo utilizado por las datatables para optimizar peticiones
     * @param Request $request
     */
    public function dt(Request $request)
    {
        //Se espera la configuración por cada columna:
        /*
         * {
            "data": "fechatiempo",
            "name": "",
            "searchable": "true",
            "orderable": "true",
            "search": {
              "value": "",
              "regex": "false"
            }
          }
         * */
        if (!$request->has("draw")) {
            return $this->makeResponse("MISSING_DRAW", 400);
        }

        $colConfig = $request->get("columns");
        /* Columnas para el select */
        /* Debe ignorar: dtactions y las que tengan puntos tabla.foranea */
        $selectColumns = array();
        $whereCols = array();
        foreach ($colConfig as $col) {
            if ($col["data"] !== "dtactions" && strpos($col["data"], ".") == false) {
                array_push($selectColumns, $col["data"]);

                //Preparo al mismo tiempo las columnas que se pueden buscar
                if ($col["searchable"] === "true") {
                    array_push($whereCols, $col["data"]);
                }
            }
        }
        //Se consulta el parametro de busqueda
        $search = $request->get("search");
        $search = $search["value"];

        //Calculo la pagina solicitada
        $page = ($request->get("start") / $request->get("length")) + 1;

        //Agrego la pagina a la peticion. la pagina la lee laravel automaticamente
        //https://laravel.com/docs/5.2/pagination buscar ?page
        $request->merge(["page" => $page]);

        //preparo el orden porque las datatable siempre están ordenadas
        $orderConfig = $request->get("order");
        //llega del modo: order[0][column]:0 y order[0][dir]:asc
        $orderCol = $colConfig[$orderConfig[0]["column"]]["data"];
        $orderDir = $orderConfig[0]["dir"];

        //Preparo un modelo con el select
        $model = call_user_func($this->model . '::select', $selectColumns);

        //Valida los eager
        if (!empty($this->eager)) {
            $model = $model->with($this->eager);
        }

        //Debe validar si tiene un padre
        if ($request->has("parent")) {
            //parent es un arreglo con las columnas y los valores que debe filtrar ejemplo
            // [ 'gepaises_id' => 55, 'otro_id' => 35 ]
            $parent = $request->get("parent");

            $model->where(function ($query) use ($parent) {
                foreach ($parent as $col => $val) {
                    $query->whereRaw("lower($col) = ?", array(strtolower($val)));
                }
            });

        }

        if ($search !== '') {
            //Debe agregar el where a la consulta
            $model->where(function ($query) use ($whereCols, $search) {
                $primer = true;
                foreach ($whereCols as $col) {
                    if ($primer) {
                        $primer = false;
                        $query->whereRaw("lower($col) LIKE '%'||?||'%'", array(strtolower($search)));
                    } else {
                        $query->orWhereRaw("lower($col) LIKE '%'||?||'%'", array(strtolower($search)));
                    }
                }
            });
        }

        if (strpos($orderCol, ".") == false) {
            $model = $model->orderBy($orderCol, $orderDir);
        }

        $datos = $model->paginate($request->get("length"));

        $respuesta = array(
            "draw" => $request->get("draw") * 1,
            "iTotalDisplayRecords" => $datos->total(),
            "iTotalRecords" => $datos->count(),
            //"recordsTotal" => $datos->total(),
            //"recordsFiltered" => $datos->count(),
            "data" => $datos->items()
        );
        //Nota error en la documentación:
        //debe ser iTotalRecords en vez de recordsTotal
        //y iTotalDisplayRecords en vez de recordsFiltered
        //esto aparece en la documentación vieja.
        //documentación más recientea la fecha:
        //https://datatables.net/manual/server-side

        return $respuesta;
    }


    public function lv(Request $request)
    {
        /*
         * Formato de peticion
         * draw: contador para que siempre use la ultima respuesta
         * select: columnas que debe retornar - Obligatorio, array.
         * filter: es un array con la siguiente estructura: 'col' => value // se usa para filtrar las listas aniadas
         * Formato de respuesta:
         * [ 'draw' => $request->get("draw"), data => $model->get() ]
         */

        if (!$request->has("select")) {
            return $this->makeResponse(["message" => "MISSING_SELECT_PARAMS"], 400);
        }

        if (!$request->has("draw")) {
            return $this->makeResponse(["message" => "MISSING_DRAW_PARAMS"], 400);
        }

        if (!is_array($request->get("select"))) {
            return $this->makeResponse(["message" => "WRONG_SELECT_PARAMS"], 400);
        }

        $model = call_user_func($this->model . '::select', $request->get("select"));

        if ($request->has("filter")) {
            if (!is_array($request->get("filter"))) {
                return $this->makeResponse(["message" => "WRONG_FILTER_PARAMS"], 400);
            }
            $filter = $request->get("filter");
            foreach ($filter as $col => $value) {
                $model->where($col, $value);
            }
        }

        return array('draw' => $request->get("draw"), "data" => $model->get());
    }

    /**
     * Funcion parra filtrar listas de valores select2
     * Espera los siguientes parametros:
     * - select: las columnas que desea seleccionar
     * - filter: columnas filtro para listas aniadadas. formato: { col: value } parametro opcional
     * - search: objeto para filtrar los resultados, ejemplo: { nombre: 'dan' } donde el query queda nombre like 'dan%'
     *
     */
    public function lv2(Request $request)
    {
        if (!$request->has("select")) {
            return $this->makeResponse(["message" => "MISSING_SELECT_PARAMS"], 400);
        }

        if (!is_array($request->get("select"))) {
            return $this->makeResponse(["message" => "WRONG_SELECT_PARAMS"], 400);
        }

        $model = call_user_func($this->model . '::select', $request->get("select"));

        if ($request->has("filter") && !is_array($request->get("filter"))) {
            return $this->makeResponse(["message" => "WRONG_FILTER_PARAMS"], 400);
        }

        if (!$request->has("search") || !is_array($request->get("search"))) {
            return $this->makeResponse(["message" => "WRONG_FILTER_PARAMS"], 400);
        }

        $search = $request->get("search");
        if (count($search) != 1) {
            return $this->makeResponse(["message" => "WRONG_FILTER_PARAMS"], 400);
        }

        foreach ($search as $col => $value) {
            if (empty($value)) {
                return $this->makeResponse(["message" => "WRONG_FILTER_PARAMS"], 400);
            }
            $model->where('lower(' . $col . ")", 'like', strtolower($value) . '%');
        }

        if ($request->has("filter")) {
            $filter = $request->get("filter");
            foreach ($filter as $col => $value) {
                $model->where('lower(' . $col . ")", strtolower($value));
            }
        }

        return $model->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $val = $this->isValid($request);

        if ($val !== true) {
            return $this->makeResponse($val, 400);
        }

        $record = new $this->model(isset($this->onlyStore) ? $request->only($this->onlyStore) : $request->all());
        try {
            $record->save();
            if (!empty($this->eager)) {
                $record->load($this->eager);
            }
            return $record;
        } catch (\Exception $ex) {
            return $this->handleEx($ex, $record, $request);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = $this->findRecord($id);
        if (!isset($record)) {
            return $this->makeResponse(['message' => 'RECORD_DOES_NOT_EXISTS'], 400);
        }
        return $record;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $val = $this->isValid($request, $id);

        if ($val !== true) {
            return $this->makeResponse($val, 400);
        }

        $record = $this->findRecord($id);
        if (!isset($record)) {
            return $this->makeResponse(['message' => 'RECORD_DOES_NOT_EXISTS'], 400);
        }

        try {
            $record->fill($this->getAvailableReuqestParamsForUpdate($request));
            $record->save();
            $record = $this->findRecord($id);
            return $record;
        } catch (\Exception $ex) {
            return $this->handleEx($ex, $record, $request);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $record = $this->findRecord($id);
        if (!isset($record)) {
            return $this->makeResponse(['message' => 'RECORD_DOES_NOT_EXISTS'], 400);
        }
        try {
            $record->delete();
            return $record;
        } catch (\Exception $ex) {
            return $this->handleEx($ex, $record, $request);
        }
    }

    protected function findRecord($id)
    {
        if (empty($this->eager)) {
            return call_user_func($this->model . '::find', $id);
        } else {
            return call_user_func($this->model . '::with', $this->eager)->find($id);
        }
    }

    /**
     * Reglas de validacion personalizadas, debe retornar un vector con 2 posiciones.
     * La primera el arreglo de validaciones, la segunda el arreglo de mensajes.
     */
    protected function getcustomRules($request)
    {
        return array();
    }

    /*
     * @param  \Illuminate\Http\Request  $request
    *  @return Boolean
    *   Retorna un array de errores en caso de que no sea valida la peticion.
    */
    protected function isValid(Request $request, $id = null)
    {

        if (!$this->isStore($request) && !$this->isUpdate($request)) {
            return true;
        }

        if (!isset($this->onlyUpdate) && !isset($this->onlyStore)) {
            return true;
        }

        if ($this->isStore($request) && isset($this->onlyStore)) {
            $validationRules = ValidationUtils::getRulesFromArray($this->onlyStore);
        } else if ($this->isUpdate($request) && isset($this->onlyUpdate)) {
            $validationRules = ValidationUtils::getRulesFromArray($this->onlyUpdate);
        } else {
            $validationRules = ValidationUtils::getRulesFromArray($this->getValidableRequestParams($request));
        }

        $customRules = $this->getcustomRules($request);

        /**
         * Si el id no se está enviando, entonces se asigna un ID comodin para poder realizar la consulta
         * y validar que el campo unique se cumpla.
         */
        if (!isset($id)) {
            $id = -12345745678910; //Este ID es comodin.
        }

        array_walk($customRules, function (&$v, $k) use ($id) {
            $v = str_replace(':id', $id, $v);
        });

        if (count($customRules) > 0) {
            $validationRules[0] = $customRules[0];
            $validationRules[1] = $customRules[1];
        }

        if ($this->isStore($request)) {
            $validate = Validator::make (
                $request->only($this->onlyStore),
                $validationRules[0],
                $validationRules[1]
            );
        }
        elseif ($this->isUpdate($request)) {
            $validate = Validator::make (
                $request->only($this->onlyUpdate),
                $validationRules[0],
                $validationRules[1]
            );
        }

        if ($validate->fails()) {
            return $validate->errors();
        }

        return true;
    }

    protected function makeResponse($data, $statusCode)
    {
        return Response::make($data, $statusCode);
    }

    public function isStore($request)
    {
        return preg_match('/store$/', Route::getCurrentRoute()->getName());
    }

    public function isUpdate($request)
    {
        return preg_match('/update$/', Route::getCurrentRoute()->getName());
    }

    //Retorna un array con las keys de la peticion que están permitidas en 
    //only update
    private function getValidableRequestParams($request)
    {

        if (!isset($this->onlyUpdate) || !$this->isUpdate($request)) {
            return array_keys($request->all());
        }
        //Store y Update son los unicos metodos que usan los parametros de la petición los demás lo ignoran
        return (array_keys(array_filter($request->only($this->onlyUpdate))));
    }

    //Debido a que en el momento de actualizar un registro solo se envian algunos valores en la peticion
    //cuando se hace $request->only retorna null para los atributos que no llegan en la petición lo que hace
    //que los objetos guarden null en la base de datos o genera errores indeseados
    //para eso se consultan los atributos de la petición y se filtran los nulos
    protected function getAvailableReuqestParamsForUpdate(Request $request)
    {
        if (!isset($this->onlyUpdate)) {
            return $request->all();
        }

        /*
         * Con este codigo se quitan los valores nulos dejados por el metodo only
        */

        return array_where($request->only($this->onlyUpdate), function ($key, $value) {
            if (is_null($value))
                return false;
            else
                return true;
        });
    }

    protected function handleEx(\Exception $ex, $record = null, Request $request = null)
    {
        $mensaje = "Exception: " . $ex->getMessage();
        $mensaje .= "\n" . "Record: " . ($record == null ? "Null" : json_encode($record->toArray()));
        $mensaje .= "\n" . "Request: " . ($request == null ? "Null" : json_encode($request->all()));
        $mensaje .= "\n" . "Usuario: " . JWTAuth::toUser();
        $mensaje .= "\n" . "Traza: " . $ex->getTraceAsString();
        Log::critical($mensaje);
        return $this->makeResponse(["message" => "INTERNAL_SERVER"], 500);
    }

    public static function logEx(\Exception $ex)
    {
        $mensaje = "Exception: " . $ex->getMessage();
        try {
            $mensaje .= "\n" . "Usuario: " . JWTAuth::toUser();
        } catch (\Exception $te) {
            $mensaje .= "\n" . "Usuario: No fue posible consultar";
        }
        $mensaje .= "\n" . "Traza: " . $ex->getTraceAsString();
        Log::critical($mensaje);
    }

    /**
     * Retorna un parametro del sistema
     * @param $key contiene el parametro del sistema.
     * @return string Parametro del sistema del tipo String. Retorna VALUE_NOT_FOUND si no encuentra el valor.
     */
    public function getStrSysParam($key)
    {
        return SystemParameters::getStrSysParam($key);
    }

    /**
     * Retorna un parametro del sistema
     * @param $key String Llave que contiene el parametro del sistema.
     * @return Double Parametro del sistema del tipo numerico. Retorna VALUE_NOT_FOUND si no encuentra el valor.
     */
    public static function getNumSysParam($key)
    {
        return SystemParameters::getNumSysParam($key);
    }

    public function xls(Request $request)
    {
        $result = json_decode($request->get('columns'));

        $modelo = new $this->model;

        $columns = array();

        foreach ($result as $data) {
            if ($data->name !== "dtactions" && strpos($data->name, ".") == false) {
                array_push($columns, $data->name);
            }
        }

        $fileName = substr(class_basename($modelo), 2) . "s";

        Excel::create($fileName, function ($excel) use ($columns, $modelo) {
            $excel->sheet('Hoja1', function ($sheet) use ($columns, $modelo) {

                $sheet->fromModel($modelo::all($columns));

            });
        })->store('xlsx', public_path() . "/downloadDocs");

        return response()->download(public_path() . "/downloadDocs/" . $fileName . ".xlsx", $fileName . ".xlsx");
    }

    /**
     * NO BORRAR ESTE METODO ES PARA HISTORICO DE SELEC2
     * @param Request $request
     * @return mixed
     */
    /*
    public function lv(Request $request){
    */
    /*
     * Formato de peticion
     * siempre recibe los siguientes parametros
     * search: texto que busca el usuario - Opcional
     * page: pagina deseada - Laravel la detecta automaticamente para el paginador
     * -- Adicionales definidos por la directiva select
     * select: columnas que debe retornar - Obligatorio, array.
     * filter: columnas que se deben agregar en el where - Obligatorio, array.
     * Formato de respuesta: el estandar de laravel.
     * * NO BORRAR ESTE METODO ES PARA HISTORICO DE SELEC2
     */
    //* NO BORRAR ESTE METODO ES PARA HISTORICO DE SELEC2
    /*
       if( !$request->has("select") ){
           $this->makeResponse("MISSING_SELECT_PARAMS", 400);
       }
       if( !$request->has("filter") ){
           $this->makeResponse("MISSING_FILTER_PARAMS", 400);
       }

       if( !is_array($request->get("select"))){
           $this->makeResponse("WRONG_SELECT_PARAMS", 400);
       }

       if( !is_array($request->get("filter"))){
           $this->makeResponse("WRONG_FILTER_PARAMS", 400);
       }

       $model = call_user_func($this->model . '::select', $request->get("select"));
       if( $request->has("search") && $request->get("search") !== "" ){
           $filter = $request->get("filter");
           $search = $request->get("search");
           $primero = true;
           foreach($filter as $col){
               if( $primero ){
                   $model->where("lower($col)", 'LIKE', strtolower($search)."%");
                   $primero = false;
               }else{
                   $model->orWhere("lower($col)", 'LIKE', strtolower($search)."%");
               }
           }
       }
       return $model->paginate(25);
       //* NO BORRAR ESTE METODO ES PARA HISTORICO DE SELEC2
   }
    */

}
