<?php

namespace App\Models;

use App\Models\GE\Gedepartamento;
use Illuminate\Database\Eloquent\Model;

class Prueba extends Model
{
    protected $fillable = [
        'text',
        'numero',
        'checkbox',
        'radio',
        'password',
        'color',
        'fecha',
        'fechatiempo',
        'email',
        'range',
        'slider',
        'listadevalor',
        'listamultiple',
        'switch_c'
    ];

    protected $casts = [
        'id' => 'integer',
        'numero' => 'integer',
        'checkbox' => 'boolean',
        'slider' => 'integer',
        'listadevalor' => 'integer',
        'switch_c' => 'boolean'
    ];

    protected $appends = ['departamentos'];

    //protected $dates = ['created_at', 'updated_at', 'deleted_at', 'fecha', 'fechatiempo'];

    public function paises()
    {
        return $this->belongsTo('App\Models\GE\Gepaise', 'listadevalor', 'id');
    }

    public function setListamultipleAttribute($value)
    {
        if (is_array($value)) {
            $this->attributes['listamultiple'] = implode(",", $value);
        } else {
            $this->attributes['listamultiple'] = $value;
        }

    }

    public function getListamultipleAttribute($value)
    {
        if (is_array($value)) {
            return $value;
        } else {
            return explode(",", $value);
        }

    }

    public function getDepartamentosAttribute()
    {
        return Gedepartamento::wherein('id', $this->listamultiple)->get();
    }


}
