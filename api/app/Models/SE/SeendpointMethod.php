<?php

namespace App\Models\SE;

use Illuminate\Database\Eloquent\Model;

class SeendpointMethod extends Model
{

    protected $fillable = ['method','seendpoint_id','serole_id'];
    protected $appends = array('objMethod');

    public function rol()
    {
        return $this->belongsTo("App\Models\SE\Serole", "serole_id", "id");
    }

    public function endpoint()
    {
        return $this->belongsTo("App\Models\SE\Seendpoint", "seendpoint_id", "id");
    }

    public function getObjMethodAttribute()
    {
        return ["nombre"=>$this->method];
    }
}
