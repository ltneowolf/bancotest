<?php

namespace App\Models\SE;

use Illuminate\Database\Eloquent\Model;

class Serole extends Model
{

    protected $fillable = ['name'];

    public function seendpoints()
    {
    	return $this->belongsToMany('App\Models\SE\Seendpoint', 'seendpoint_methods');
    }
}
