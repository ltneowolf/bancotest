<?php

namespace App\Models\SE;

use Illuminate\Database\Eloquent\Model;

class SeuserRole extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'serole_id'];

    //
    public function seendpointMethods()
    {
        return $this->hasMany('App\Models\SE\SeendpointMethod');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function rol()
    {
        return $this->belongsTo('App\Models\SE\Serole', 'serole_id', 'id');
    }
}
