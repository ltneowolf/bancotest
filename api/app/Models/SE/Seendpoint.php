<?php

namespace App\Models\SE;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
use DB;
use PDO;

class Seendpoint extends Model
{
    protected $fillable = ['id', 'state', 'menu_label', 'show', 'icon', 'page_tittle', 'page_desc', 'seendpoint_id',
        'sort', 'is_mobile'];

    protected $appends = [];

    protected $casts = [
        'show' => 'boolean',
        'is_mobile' => 'boolean',
        'sort' => 'integer'
    ];

    public function methods()
    {
        return $this->hasMany('App\Models\SE\SeendpointMethod', 'seendpoint_id', 'id');
    }

    public function seendpoints()
    {
        return $this->hasMany('App\Models\SE\Seendpoint');
    }

    public function endPointParent()
    {
        return $this->belongsTo("App\Models\SE\Seendpoint", "seendpoint_id", "id");
    }

    public function getAvailMethodsAttribute()
    {
        $usuario = JWTAuth::toUser();
        $metodos = array();
        $result = SeendpointMethod::where('seendpoint_id', $this->id)
            ->select('method')
            ->join('seuser_roles', 'seendpoint_methods.serole_id', '=', 'seuser_roles.serole_id')
            ->where('seuser_roles.user_id', $usuario->id)
            ->distinct()
            ->get()
            ->toArray();
        foreach ($result as $key => $value) {
            array_push($metodos, $value['method']);
        }
        return $metodos;
    }
}
