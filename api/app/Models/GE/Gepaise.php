<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gepaise extends Model
{
    //protected $table = 'paises';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','nombre', 'abreviatura'];

}


