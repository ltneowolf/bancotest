<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Geestado extends Model
{
    //protected $table="geestados";
    protected $fillable = ['nombre', 'gemodulo_id'];

    public function modulo()
    {
        return $this->belongsTo('App\Models\GE\Gemodulo','gemodulo_id','id');
    }
}
