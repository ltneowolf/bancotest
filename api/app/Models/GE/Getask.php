<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Getask extends Model
{
    protected $fillable = ["alerta", "datos", "intentos", "finalizado", "error", "traza"];

    protected $casts = array(
        'getemplate_id' => 'integer'
    );
}
