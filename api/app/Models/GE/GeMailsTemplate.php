<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class GeMailsTemplate extends Model
{
    protected $table = 'gemailsTemplates';
    protected $fillable = ['nombre','asunto','siemprecc','siemprecco','cuerpomsg'];
}
