<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Geparameters extends Model
{
    protected $fillable = ['key',
        'numval',
        'strval',
        'observation',
        'gemodulo_id'
    ];

    protected $casts = [
        'key' => 'string',
        'numval' => 'double',
        'strval' => 'string',
        'observation' => 'string'
    ];

    public function modulo()
    {
        return $this->belongsTo('App\Models\GE\Gemodulo', 'gemodulo_id', 'id');
    }

    public function setKeyAttribute($value)
    {
        $this->attributes['key'] = trim(strtoupper($value));
    }
}
