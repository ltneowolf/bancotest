<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gemodulo extends Model
{
    protected $table="Gemodulos";
    protected $fillable = ['nombre'];

}

