<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class GeprintTemplate extends Model
{
    protected $fillable = ["template", "nombre"];
}
