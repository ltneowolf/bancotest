<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Getipo extends Model
{
    protected $fillable = ['nombre', 'gemodulo_id', 'grupo', 'abreviatura'];

    public function modulo()
    {
        return $this->belongsTo('App\Models\GE\Gemodulo', 'gemodulo_id', 'id');
    }
}
