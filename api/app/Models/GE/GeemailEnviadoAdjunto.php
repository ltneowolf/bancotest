<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class GeemailEnviadoAdjunto extends Model
{
    protected $fillable = ['geemail_enviado_id', 'geadjunto_id'];

    protected $casts = [
        'geemail_enviado_id' => 'integer',
        'geadjunto_id' => 'integer'
    ];
}
