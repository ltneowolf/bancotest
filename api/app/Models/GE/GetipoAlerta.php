<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class GetipoAlerta extends Model
{
    protected $fillable = ['nombre', 'obs'];

    public function alertas()
    {
        return $this->hasMany("App\Models\GE\Gealerta");
    }
}
