<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Geciudade extends Model
{
	protected $table='geciudades';
    protected $fillable = ['nombre', 'abreviatura', 'gedepartamento_id'];

    protected $casts = [
        'nombre' => 'string',
        'departamento_id' => 'integer',
        'abreviatura' => 'string'
    ];

    public function departamento(){
        return $this->belongsTo('App\Models\GE\Gedepartamento', 'gedepartamento_id','id');
    }

}
