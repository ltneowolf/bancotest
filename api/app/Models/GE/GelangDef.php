<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class GelangDef extends Model
{
	
    protected $fillable = ['key', 'value', 'gelanguage_id'];

    public function language(){
        return $this->belongsTo('App\Models\GE\Gelanguage', 'gelanguage_id','id');
    }
}
