<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gemoneda extends Model
{

    protected $fillable = ['nombre','simbolo','abreviatura','factor_conversion','ajuste'];

    protected $casts = [
        'factor_conversion' => 'double',
        'ajuste' => 'double'
    ];
}




