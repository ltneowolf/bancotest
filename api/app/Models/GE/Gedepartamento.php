<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gedepartamento extends Model
{
    protected $table = 'gedepartamentos';


    protected $fillable = ['id', 'nombre', 'abreviatura', 'gepaise_id'];

    protected $casts = [
        'nombre' => 'string',
        'gepaise_id' => 'integer',
        'abreviatura' => 'string'
    ];

    public function pais()
    {
        return $this->belongsTo('App\Models\GE\Gepaise', 'gepaise_id', 'id');
    }

}
