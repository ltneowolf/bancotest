<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gealerta extends Model
{
    protected $fillable = ["isuserlogged", "getipo_alerta_id", "getemplate_id","siemprecc","siemprecco"];

    protected $casts = [
        "isuserlogged" => "boolean"
    ];


    public function tipoAlerta()
    {
        return $this->belongsTo("App\Models\GE\GetipoAlerta", "getipo_alerta_id","id");
    }

    public function template()
    {
        return $this->belongsTo("App\Models\GE\Getemplate", "getemplate_id","id");
    }
}
