<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Geagente extends Model
{
    protected $fillable = ['nombre','email', 'direccion'];

    public function contactos()
    {
        return $this->hasMany('App\Models\GE\Gecontacto', 'geagente_id');
    }

}
