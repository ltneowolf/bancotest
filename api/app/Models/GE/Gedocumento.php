<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gedocumento extends Model
{
    protected $fillable = ['nombre', 'funcion', 'tipo', 'popup', 'grupo'];

    protected $appends = ['tipolabel'];

    public function getTipolabelAttribute(){
        switch( $this->tipo ){
            case "FRM": return array('id' => "FRM", 'nombre' => "FORMULARIO_LABEL");
            case "DIN": return array('id' => "DIN", 'nombre' => "DINAMICO_LABEL");
            case "ADJ": return array('id' => "ADJ", 'nombre' => "ADJUNTO_LABEL");
        }
        return array();
    }
}
