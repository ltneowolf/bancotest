<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class GeemailEnviado extends Model
{
    protected $fillable = ['asunto', 'cuerpo', 'remitente', 'para', 'cc', 'cco', 'objectgroup', 'objectid'];

    protected $casts = [
        'asunto' => 'string',
        'cuerpo' => 'string',
        'remitente' => 'string',
        'para' => 'string',
        'cc' => 'string',
        'cco' => 'string',
    ];

    public function adjuntos()
    {
        return $this->belongsToMany('App\Models\GE\Geadjunto', 'geemail_enviado_adjuntos');
    }

}
