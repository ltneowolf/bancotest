<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Geadjunto extends Model
{
    protected $fillable = ['nombre','mimetype','extension', 'nombrearchivo', "ruta"];
}
