<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Getemplate extends Model
{
    protected $fillable = ['nombre', 'asunto', 'cuerpomsg'];
}
