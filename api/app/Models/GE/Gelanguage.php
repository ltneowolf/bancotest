<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gelanguage extends Model
{

    protected $fillable = ['lang', 'label', 'icon'];


    public function defs()
    {
    	return $this->hasMany('App\Models\GE\GelangDef');
    }
}
