<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Geemail extends Model
{
    protected $fillable = ['nombre', 'funcion', 'getemplate_id'];

    public function template(){

        return $this->belongsTo('App\Models\GE\Getemplate', 'getemplate_id');
    }
}
