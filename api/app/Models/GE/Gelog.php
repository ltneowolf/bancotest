<?php

namespace App\Models\GE;

use Illuminate\Database\Eloquent\Model;

class Gelog extends Model
{
    protected $fillable = ["mensaje", "user_id", "proceso_id", "ref_registro"];

    public function user()
    {
        return $this->belongsTo('App\Models\User', "user_id", "id");
    }

    public function guia()
    {
        return $this->belongsTo('App\Models\MA\Maguia', "maguia_id", "id");
    }

    public function proceso()
    {
        return $this->belongsTo('App\Models\GE\Getipo', 'proceso_id', 'id');
    }
}
