<?php

namespace App\Models;

use App\Models\SE\Seendpoint;
use App\Models\SE\Serole;
use DB;
use Illuminate\Support\Facades\Log;

class UserDetailed extends User
{
    protected $table = 'users';

    protected $appends = ['seendpoints', 'semobileendpoints', 'all_states'];

    private function getMenu($isMobile)
    {
        $menu = [];

        //Obtenemos todos los endpoind a los que el usuario puede acceder
        $sqlStm = 'SELECT SE.ID, '
            . 'SE.STATE, '
            . 'SE.MENU_LABEL, '
            . 'SE.PAGE_TITLE, '
            . 'SE.PAGE_DESC, '
            . 'SE.SEENDPOINT_ID,  '
            . 'SE.SHOW,  '
            . 'SE.ICON, '
            . 'SE.SORT '
            . 'FROM USERS USR,  '
            . 'SEUSER_ROLES USRR,  '
            . 'SEROLES SR,  '
            . 'SEENDPOINT_METHODS SEMT,  '
            . 'SEENDPOINTS SE '
            . 'WHERE USR.ID =? '
            . 'AND SE.IS_MOBILE =? '
            . 'AND USRR.USER_ID   = USR.ID '
            . 'AND SR.ID          = USRR.SEROLE_ID '
            . 'AND SEMT.SEROLE_ID = SR.ID '
            . 'AND SE.ID          = SEMT.SEENDPOINT_ID '
            . 'GROUP BY SE.ID,  '
            . 'SE.STATE,  '
            . 'SE.MENU_LABEL,  '
            . 'SE.PAGE_TITLE,  '
            . 'SE.PAGE_DESC,  '
            . 'SE.SEENDPOINT_ID,  '
            . 'SE.SHOW,  '
            . 'SE.ICON, '
            . 'SE.SORT '
            . 'ORDER BY SE.SORT';

        $endpoints = DB::select($sqlStm, [$this->id, $isMobile]);

        //Esto es para quitar el error Cannot use object of type stdClass as array
        $endpoints = array_map(function ($value) {
            return (array)$value;
        }, $endpoints);

        /**
         * Del listado de endPoint traemos todos los endpoints que no tienen padre, es el primer nivel del menu
         */
        $nodosPadres = Seendpoint::whereNull('seendpoint_id')
            ->where("is_mobile", $isMobile)
            ->orderBy('sort')
            ->get()
            ->toArray();

        /*
         * Recorremos los nodos padres y vamos obteniendo sus hijos (Niveles mas abajo del menu)
         * */
        foreach ($nodosPadres as $nodosPadre) {
            $nodosPadre["seendpoints"] = self::getNodoHijos($nodosPadre, $endpoints);
            //Si encontró hijos entonces agrego el nodo al menu o si tiene permisos en el endpoint methods como en el
            //caso del dashboard no tiene hijos pero tiene permisos.
            if (count($nodosPadre["seendpoints"]) == 0) {
                $inEndpoints = array_where($endpoints, function ($key, $value) use ($nodosPadre) {
                    return $value["ID"] == $nodosPadre["id"];
                });

                if (count($inEndpoints) > 0) {
                    array_push($menu, $nodosPadre);
                }

            } else {

                array_push($menu, $nodosPadre);
            }
        }

        return $menu;
    }

    public function getSemobileendpointsAttribute()
    {
        return $this->getMenu(true);
    }

    public function getSeendpointsAttribute()
    {
        return $this->getMenu(false);
    }

    /*
     * Obtiene todos los endpoints a los que el usuario tiene permisos
     */
    public function getAllStatesAttribute()
    {
        $query = 'SELECT '
            . 'SP.STATE '
            . 'FROM USERS US '
            . 'JOIN SEUSER_ROLES USR  '
            . 'ON USR.USER_ID = US.ID '
            . 'JOIN SEENDPOINT_METHODS SEM '
            . 'ON SEM.SEROLE_ID = USR.SEROLE_ID '
            . 'JOIN SEENDPOINTS SP '
            . 'ON SP.ID = SEM.SEENDPOINT_ID '
            . 'WHERE US.ID=? '
            . 'GROUP BY SP.STATE';

        $endPoints = DB::select($query, [$this->id]);
        $endPoints = array_map(function ($value) {
            return (array)$value;
        }, $endPoints);

        return array_flatten($endPoints);
    }

    /**
     * Funcion encargada de obtener todos los hijos de un endPoint
     * @param $padre_nodo
     * @param $endpoints
     * @return array
     */
    private static function getNodoHijos($padre_nodo, $endpoints)
    {
        $nodoHijos = [];

        //Filtramos todos los endpoints en donde el padre sea el $padre_nodo
        $endpHijos = array_filter($endpoints, function ($endPoint) use ($padre_nodo) {
            return ($padre_nodo["id"] == $endPoint["SEENDPOINT_ID"] && $endPoint["SHOW"]);
        });

        //Si encontro hijos debo volver a llamar a la funcion recursiva para obtener el siguiente nivel del menu
        foreach ($endpHijos as $nodoHijo) {
            $nodoHijo["seendpoints"] = self::getNodoHijos($nodoHijo, $endpoints);
            array_push($nodoHijos, $nodoHijo);
        }

        return $nodoHijos;
    }
}
