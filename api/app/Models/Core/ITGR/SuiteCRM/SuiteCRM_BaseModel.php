<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 5/07/2017
 * Time: 10:33 AM
 */
namespace App\Models\Core\ITGR\SuiteCRM;

use App\Util\SuiteCRM\SuiteCRM_Utils;

class SuiteCRM_BaseModel
{
    public function __construct($authKey, $moduleName, $columnas = null)
    {
        if (isset($moduleName) && isset($authKey)) {
            $this->moduleName = $moduleName;
            $this->authKey = $authKey;

            if (isset($columnas)) {
                $this->columns = $columnas;
            } else {
                $this->moduleFields = $this->getModuleFields();
                $this->columns = array_keys(get_object_vars($this->moduleFields->module_fields));
            }
        } else {
            throw new \Exception("El parametro authKey y moduleName es requerido");
        }
    }

    /**
     * @return Object con la siguiente estructura:
     *  String module_name
     *  String table_name
     *  Object module_fields (Contiene todos los campos del modulo)
     *  Object link_fields  (Contiene las relaciones del modulo)
     */
    public function getModuleFields()
    {
        $get_module_fields = array(
            "session" => $this->authKey,
            "module_name" => $this->moduleName,
            "fields" => array()
        );

        return SuiteCRM_Utils::callMethod("get_module_fields", $get_module_fields);
    }

    /**
     * Si se envía una matriz se pueden agregar multiples registros al tiempo
     *
     * @param $record
     * @return mixed
     */
    public function create($record)
    {
        $nvl = array();

        foreach ($this->columns as $column) {
            array_push($nvl, array(
                "name" => $column,
                "value" => array_key_exists($column, $record) ? $record[$column] : ''
            ));
        }

        $set_entry_parameters = array(
            "session" => $this->authKey,
            "module_name" => $this->moduleName, //nombre del modulo
            "name_value_list" => $nvl
        );

        $set_entry_result = SuiteCRM_Utils::callMethod("set_entry", $set_entry_parameters);

        return $set_entry_result;
    }

    /**
     * Se envía una matriz para agregar multiples registros al tiempo
     *
     * @param $record
     * @return mixed
     */
    public function createMultiple($record)
    {
        $nvl = array();

        foreach ($this->columns as $column) {
            array_push($nvl, array(
                "name" => $column,
                "value" => array_key_exists($column, $record) ? $record[$column] : ''
            ));
        }

        $set_entry_parameters = array(
            "session" => $this->authKey,
            "module_name" => $this->moduleName, //nombre del modulo
            "name_value_list" => $nvl
        );

        $set_entry_result = SuiteCRM_Utils::callMethod("set_entries", $set_entry_parameters);

        return $set_entry_result;
    }


    public function all() {
        $get_entry_list_parameters = array(
            'session' => $this->authKey,
            'module_name' => $this->moduleName,
            //The SQL WHERE clause without the word "where".
            'query' => "",
            //The SQL ORDER BY clause without the phrase "order by".
            'order_by' => "",
            'offset' => '0',
            //Optional. A list of fields to include in the results.
            'select_fields' => $this->columns,
            'link_name_to_fields_array' => array(),
            //The maximum number of results to return.
            'max_results' => "9999999",
            //To exclude deleted records
            'deleted' => '0',
            //If only records marked as favorites should be returned.
            //'Favorites' => true,
        );

        $get_entry_list_result = SuiteCRM_Utils::callMethod("get_entry_list", $get_entry_list_parameters);

        if( isset($get_entry_list_result->entry_list) ){
            return $get_entry_list_result->entry_list;
        }

        return SuiteCRM_Utils::throwError($get_entry_list_result);
    }

}