<?php

namespace App\Models\TEST;

use Illuminate\Database\Eloquent\Model;

class CtaahoTransaccione extends Model
{
    protected $fillable = ['descripcion', 'monto', 'tipo_transaccion', 'cajero_id', 'cuenta_ahorro_id'];

    protected $casts = [
        'descripcion' => 'string',
        'monto' => 'double'
    ];

    public function cuentaahorro(){
        return $this->belongsTo('App\Models\TEST\CuentaAhorro', 'cuenta_ahorro_id','id');
    }

    public function cajero(){
        return $this->belongsTo('App\Models\User', 'cajero_id','id');
    }

    public function tipotrs(){
        return $this->belongsTo('App\Models\GE\Getipo', 'tipo_transaccion','id');
    }
}
