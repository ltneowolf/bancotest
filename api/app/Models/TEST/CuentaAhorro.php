<?php

namespace App\Models\TEST;

use App\Util\SystemParameters;
use Illuminate\Database\Eloquent\Model;

class CuentaAhorro extends Model
{
    protected $fillable = ['numero_cuenta', 'clave', 'estado', 'cliente_id', 'asesor_id'];

    protected $casts = [
        'estado' => 'boolean',
        'numero_cuenta' => 'string',
        'clave' => 'string',
    ];

    protected $hidden = ['clave'];

    protected $appends = ['saldo'];

    public function getSaldoAttribute()
    {
        $consignaciones = SystemParameters::getNumSysParam('TIPO_TRS_CONSIGNACION');
        $retiro = SystemParameters::getNumSysParam('TIPO_TRS_RETIRO');

        $totalConsigna = CtaahoTransaccione::where('cuenta_ahorro_id', $this->id)
            ->where('tipo_transaccion', $consignaciones)
            ->sum('monto');

        $totalRetiro = CtaahoTransaccione::where('cuenta_ahorro_id', $this->id)
            ->where('tipo_transaccion', $retiro)
            ->sum('monto');

        return $totalConsigna - $totalRetiro;
    }

    public function setClaveAttribute($value)
    {
        $this->attributes['clave'] = \Hash::make($value);
    }

    public function cliente()
    {
        return $this->belongsTo('App\Models\TEST\Cliente', 'cliente_id', 'id');
    }

    public function asesor()
    {
        return $this->belongsTo('App\Models\User', 'asesor_id', 'id');
    }

    public function transacciones()
    {
        return $this->hasMany('App\Models\TEST\CtaahoTransaccione', 'cuenta_ahorro_id', 'id');
    }
}
