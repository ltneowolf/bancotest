<?php

namespace App\Models\TEST;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['nombres', 'apellidos', 'direccion', 'telefono', 'identificacion', 'email',
        'tipo_identificacion', 'geciudade_id', 'asesor_id'
    ];

    protected $casts = [
        'nombres' => 'string',
        'apellidos' => 'string',
        'direccion' => 'string',
        'telefono' => 'string',
        'identificacion' => 'string',
        'email' => 'string',
    ];

    public function cuentas()
    {
        return $this->hasMany('App\Models\TEST\CuentaAhorro', 'cliente_id', 'id');
    }

    public function tipoid()
    {
        return $this->belongsTo('App\Models\GE\Getipo', 'tipo_identificacion', 'id');
    }

    public function ciudad()
    {
        return $this->belongsTo('App\Models\GE\Geciudade', 'geciudade_id', 'id');
    }

    public function asesor()
    {
        return $this->belongsTo('App\Models\User', 'asesor_id', 'id');
    }
}
