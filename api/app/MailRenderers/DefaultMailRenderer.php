<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 11/04/2017
 * Time: 11:40 AM
 */

namespace App\MailRenderers;


use App\Models\GE\Geemail;
use App\Util\TemplateUtils;

class DefaultMailRenderer
{
    /**
     * Este renderer requiere de manera obligatoria en data los siguientes indices:
     * <ul>
     * <li>[to]</li>
     * <li>Opcionales:
     *      <ul>
     *          <li>[cc]</li>
     *          <li>[cco]</li>
     *      </ul
     * </li>
     * </ul>
     *
     * Ademas este renderer usa TemplateUtils::renderMailTemplate el cual siempre retorna un body y un asunto.
     * Pero ignora el asunto de la respuesta y utiliza el de geemail
     *
     * @param $geemailId
     * @param $data
     *
     * @return array Los mail renderers siempre deben retornar de manera obligatoria la siguiente información:
     * - [ASUNTO]: asunto del correo
     * - [BODY]: el cuerpo html del correo
     * - [to]: destinatario del correo
     * - opcionales: [cc] y [cco]
     */
    public static function render($geemailId, $data){
        $geemailModel = Geemail::find($geemailId);
        $render = TemplateUtils::renderMailTemplate($geemailModel->getemplate_id, $data);

        $soloString = function($var){
            return is_string($var);
        };

        $filtrado = array_filter($data, $soloString);

        $result =  array(
            "[BODY]" => $render["body"],
            "[ASUNTO]" =>  str_replace(array_keys($filtrado), $filtrado, $geemailModel->asunto),
            "[to]" => array_key_exists("[to]", $render)?$render["[to]"]:"",
        );
        if( array_key_exists("[cc]", $data) ){
            $result["[cc]"] = $data["[cc]"];
        }
        if( array_key_exists("[cco]", $data) ){
            $result["[cco]"] = $data["[cco]"];
        }
        return $result;
    }
}