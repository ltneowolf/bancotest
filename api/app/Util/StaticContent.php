<?php
/**
 * Created by PhpStorm.
 * Date: 18/04/2016
 * Time: 12:02 PM
 */

namespace App\Util;


class StaticContent
{

    /**
     * @param $jsonLastError Código retornado por json_last_error().
     * @return array El array contiene el código del error y el mensaje.
     */
    public static function validarJSON($jsonLastError)
    {
        switch ($jsonLastError) {
            case JSON_ERROR_NONE:
                return array('codigo' => 'JSON_ERROR_NONE', 'msj' => 'No ocurrió ningún error');
                break;
            case JSON_ERROR_DEPTH:
                return array('codigo' => 'JSON_ERROR_DEPTH', 'msj' => 'Se ha excedido la profundidad máxima de la pila');
                break;
            case JSON_ERROR_STATE_MISMATCH:
                return array('codigo' => 'JSON_ERROR_STATE_MISMATCH', 'msj' => 'JSON con formato incorrecto o inválido');
                break;
            case JSON_ERROR_CTRL_CHAR:
                return array('codigo' => 'JSON_ERROR_CTRL_CHAR',
                    'msj' => 'Error del carácter de control, posiblemente se ha codificado de forma incorrecta');
                break;
            case JSON_ERROR_SYNTAX:
                return array('codigo' => 'JSON_ERROR_SYNTAX', 'msj' => 'Error de sintaxis');
                break;
            case JSON_ERROR_UTF8:
                return array('codigo' => 'JSON_ERROR_UTF8',
                    'msj' => 'Caracteres UTF-8 mal formados, posiblemente codificados de forma incorrecta');
                break;
            default:
                return array('codigo' => 'JSON_UNKNOWN_ERROR', 'msj' => 'Error desconocido');
                break;
        }
    }

}