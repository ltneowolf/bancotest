<?php
/**
 * Created by PhpStorm.
 * User: sistemas
 * Date: 30/09/2016
 * Time: 7:19 AM
 */

namespace App\Util;


use App\Models\GE\Geadjunto;
use Illuminate\Support\Facades\Storage;
use Validator;

class StorageUtil
{

    public static function descargar($id)
    {
        $adjunto = Geadjunto::find($id);
        if (!is_null($adjunto)) {
            $ruta = $adjunto->ruta != "" ? $adjunto->ruta . "/" : "";
            if (Storage::has($ruta . $adjunto->nombrearchivo)) {
                $filePath = storage_path('app/' . $ruta . $adjunto->nombrearchivo);
                $headers = ['content-type' => $adjunto->mimetype];

                return response()->download($filePath, $adjunto->nombre, $headers);
            }
            return response()->json(['error' => 'FILE_NOT_FOUND'], 400);
        }
        return response()->json(['error' => 'FILE_NOT_FOUND'], 400);
    }

    public static function eliminar($id)
    {
        $adjunto = Geadjunto::find($id);
        $nombrearchivo = $adjunto->nombrearchivo;
        if (Storage::has($adjunto->ruta . "/" . $nombrearchivo)) {
            Storage::delete($adjunto->ruta . '/' . $adjunto->nombrearchivo);
        }
        $adjunto->delete();
        return !Storage::has($adjunto->ruta . "/" . $nombrearchivo);
    }

    public static function consultar($id)
    {
        return Geadjunto::find($id);

    }

    public static function subir($ruta, $file, $prename = "", $sobrescribir = false)
    {
        if ($sobrescribir) {
            $nombrearchivo = $prename . $file->getClientOriginalName();
        } else {
            $nombrearchivo = $prename . md5(microtime()) . "." . $file->getClientOriginalExtension();
        }
        $geadjunto = [
            "nombre" => $file->getClientOriginalName(),
            "nombrearchivo" => $nombrearchivo,//nombre en el storage
            'mimetype' => $file->getMimeType(),
            "extension" => $file->getClientOriginalExtension(),
            "ruta" => $ruta
        ];
        if ($sobrescribir) {
            $geAdjuntoDb = Geadjunto::where("nombrearchivo", $nombrearchivo)->first();
            if (!is_null($geAdjuntoDb)) {
                $geAdjuntoDb->save();
            } else {
                $geAdjuntoDb = Geadjunto::create($geadjunto);
            }
        } else {
            $geAdjuntoDb = Geadjunto::create($geadjunto);
        }
        Storage::put($ruta . '/' . $geadjunto['nombrearchivo'], file_get_contents($file->getRealPath()));
        $exists = Storage::has($ruta . '/' . $geadjunto['nombrearchivo']);
        if (!$exists) {
            Geadjunto::destroy($geAdjuntoDb->id);
            return false;
        }
        return $geAdjuntoDb;
    }

    public static function comprobarArchivos($file)
    {
        $messages = array(
            'file.mimetypes' => 'EXTENSION_ADJUNTO_INVALIDA',
            'file.between' => 'TAMANO_ADJUNTO_INVALIDO',
        );
        $validator = Validator::make(['file' => $file], [
            'file' => 'between:0,5120|mimetypes:image/png,image/bmp,image/jpeg,application/vnd.openxmlformats-officedocument.wordprocessingml.document,'
                . 'application/pdf,application/vnd.openxmlformats-officedocument.presentationml.presentation,'
                . 'application/vnd.ms-powerpointtd,text/plain,application/vnd.ms-excel,application/zip, application/x-compressed-zip,'
                . 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ], $messages);
        if ($validator->fails()) {
            $errores = $validator->errors()->toArray();
            return array("errores" => reset($errores)[0]);
        }
        return array();
    }
}