<?php
/**
 * Created by PhpStorm.
 * Date: 18/04/2016
 * Time: 12:02 PM
 */

namespace App\Util;

use App\Models\GE\Geadjunto;
use App\Models\GE\Geemail;
use App\Models\GE\GeemailEnviado;
use App\Models\GE\GeemailEnviadoAdjunto;
use App\Models\GE\Getemplate;
use Illuminate\Support\Facades\Storage;
use Mail;
use Mandrill;
use Mockery\CountValidator\Exception;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class SendMail
{
    public static $EMAIL_SISTEMAS = "sistemas@valleygroups.com"; //Esto debe cambiarse por un parametro del sistema
    /**
     * @param $nombrePlantilla , $dataJson
     * @return Confirmacion o error del mensaje.
     *
     **/
    /*
    public function sendMail($nombrePlantilla, $dataJson)
    {
        $dataJson = $this->generarTablas($dataJson);

        $template = Getemplate::where('nombre', $nombrePlantilla)->get()->first();

        if ($template) {
            $asunto = $template->asunto;
            $cuerpo = $template->cuerpomsg;
            $tempAsunto = str_replace(array_keys($dataJson), $dataJson, $asunto);
            $tempCuerpo = str_replace(array_keys($dataJson), $dataJson, $cuerpo);
            $arrayDestinatarios = explode(';', $dataJson['destinatario']);
            $arraysiempreCC = explode(';', $template->siemprecc);
            $arraysiempreCCO = explode(';', $template->siemprecco);

            $dataMail = Array(
                "asunto" => $tempAsunto,
                "cuerpo" => $tempCuerpo,
                "destinatario" => $arrayDestinatarios
            );

            if ($template->siemprecc)
                $dataMail["cc"] = $arraysiempreCC;
            if ($template->siemprecco)
                $dataMail["cco"] = $arraysiempreCCO;

            try {
                return $this->envioCorreo($dataMail);
            } catch (Exception $e) {
                throw $e;
            }

        } else
            throw new Exception ('Error, template no encontrada');
    }
    */

    /**
     * @param $dataMail array - debe ser un arreglo con la siguiente estructura
     *          asunto: asunto del mensaje
     *          destinatario: correos en el campo para
     *          body: cuerpo html del mensaje
     *          cc: destinatrios en copia
     *          cco: destinatarios en copia oculta
     *          adjuntos: es un array con el siguietne formato:
     *          array(
     *                array(
     *                   "geadjunto_id" => "",//si llega este id se asocia el adjunto como adjunto de este correo
     *                   "ruta_absoluta" => "" //si no se especifica id se debe especificar la ruta, este archivo se sube al storage.
     *                   "nombre" => "", "extension" => "", "mimetype" => "", "ruta" => "" //solo requeridos si no se especifica un id
     *                   )
     *          )
     * @param $smtpSettings array
     *          Debe ser un array con la siguiente estructura:
     *          [
     *              "host" => "", "port" => "", "seguridad" => "ssl | tls | ''",
     *              "reqAuth" => true | false , "username" => "username", "passsword" => "passsword",
     *              "remitente" => [ "name" => "nombre", "mail" => "remitente@domini.tld" ]
     *          ]
     * @param  $smtpError boolean es utilizado para indicar si ocurrió un error con la configuracion smtp y debe usar la
     * configuracion por defecto. es interno no se debe enviar.
     * @param  $mensaje string es utilizado para
     * @return array
     * @throws \Exception
     */
    public static function sendMail($dataMail, $smtpSettings = null, $smtpError = false, $mensaje = "")
    {
        $smtpConfigBackup = null;
        try {
            if (!isset($dataMail['destinatario'])) {
                $dataMail['destinatario'] = "noreply@valleygroups.com";
            }
            $to = $dataMail['destinatario'];
            $mailsSended = $mensaje . ' Correo(s) Enviado(s) : ';

            if (!isset($dataMail['remitente']) || $dataMail['remitente'] == "") {
                $fromMail = env('MAIL_FROM_EMAIL', 'soporte@valleygroups.com');
                $fromName = env('MAIL_FROM_NAME', 'Valley Groups');
            } else {
                $fromMail = $dataMail['remitente']['mail'];
                $fromName = $dataMail['remitente']['name'];
            }

            if ($smtpSettings != null && !$smtpError) {
                //Llega la configuracion smtp por lo que se debe validar
                if (!array_key_exists("host", $smtpSettings) || $smtpSettings["host"] == "") {
                    return SendMail::sendMail($dataMail, null, true, "Error configuración SMTP: Host requerido");
                }

                if (!array_key_exists("port", $smtpSettings) || $smtpSettings["port"] == "") {
                    return SendMail::sendMail($dataMail, null, true, "Error configuración SMTP: Puerto requerido");
                }

                if (!array_key_exists("seguridad", $smtpSettings) ||
                    ($smtpSettings["seguridad"] != "" && $smtpSettings["seguridad"] != "SSL" && $smtpSettings["seguridad"] != "TLS")
                ) {
                    return SendMail::sendMail($dataMail, null, true, "Error configuración SMTP: Seguridad invalido");
                }

                if (!array_key_exists("remitente", $smtpSettings) ||
                    !is_array($smtpSettings["remitente"]) ||
                    !array_key_exists("mail", $smtpSettings["remitente"]) ||
                    !array_key_exists("name", $smtpSettings["remitente"]) ||
                    $smtpSettings["remitente"]["mail"] == "" ||
                    $smtpSettings["remitente"]["name"] == ""
                ) {
                    return SendMail::sendMail($dataMail, null, true, "Error configuración SMTP: Remitente invalido");
                }

                $fromMail = $smtpSettings["remitente"]["mail"];
                $fromName = $smtpSettings["remitente"]["name"];


                if (!array_key_exists("reqAuth", $smtpSettings) || !is_bool($smtpSettings["reqAuth"])) {
                    return SendMail::sendMail($dataMail, null, true, "Error configuración SMTP: Parametro reqAuth invalido");
                }

                if ($smtpSettings["reqAuth"]) {
                    if (!array_key_exists("username", $smtpSettings) || !array_key_exists("password", $smtpSettings) ||
                        $smtpSettings["username"] == "" || $smtpSettings["password"] == ""
                    ) {
                        return SendMail::sendMail($dataMail, null, true, "Error configuración SMTP: Parametros de autenticación requeridos");
                    }
                }

                //Si pasa todas las validaciones debo crear una nueva configuracion smtp
                $smtpConfigBackup = Mail::getSwiftMailer();

                //Documentación tomada desde: https://stackoverflow.com/questions/26546824/multiple-mail-configurations
                $newTransport = \Swift_SmtpTransport::newInstance($smtpSettings["host"], $smtpSettings["port"], $smtpSettings["seguridad"] == "" ? null : $smtpSettings["seguridad"]);
                if ($smtpSettings["reqAuth"]) {
                    $newTransport->setUsername($smtpSettings["username"]);
                    $newTransport->setPassword($smtpSettings["password"]);
                }
                $sf = new \Swift_Mailer($newTransport);
                Mail::setSwiftMailer($sf);
            }

            $tmp = Mail::send([], [],
                function ($message) use ($dataMail, $to, $fromMail, $fromName) {

                    $productivo = env("ES_PRODUCTIVO", false);
                    if (!$productivo) {
                        $to = SendMail::$EMAIL_SISTEMAS;
                    }
                    $destinatarios = self::formatearCorreo($to);

                    $message->subject($dataMail['asunto'])
                        ->from($fromMail, $fromName)
                        ->to($destinatarios[0], $destinatarios[1]);

                    if (isset($dataMail['replyTo']) && strlen($dataMail['replyTo']) > 0) {
                        $replyTo = self::formatearCorreo($dataMail['replyTo']);
                        $message->replyTo($replyTo[0], $replyTo[1]);
                    }

                    $message->setBody($dataMail['body'], 'text/html');

                    if (isset($dataMail['cc']) && strlen($dataMail['cc']) > 0) {
                        $cc = self::formatearCorreo($dataMail['cc']);
                        if ($productivo) {
                            $message->cc($cc[0], $cc[1]);
                        } else {
                            $message->cc(SendMail::$EMAIL_SISTEMAS);
                        }
                    }

                    if (isset($dataMail['cco']) && strlen($dataMail['cco']) > 0) {
                        $cco = self::formatearCorreo($dataMail['cco']);
                        if ($productivo) {
                            $message->bcc($cco[0], $cco[1]);
                        } else {
                            $message->bcc(SendMail::$EMAIL_SISTEMAS);
                        }
                    }

                    if (isset($dataMail['adjuntos'])) {

                        $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();

                        foreach ($dataMail['adjuntos'] as $key => $adjunto) {
                            if (!isset($adjunto["geadjunto_id"])) {
                                $file = new File($adjunto["ruta_absoluta"]);//OJO con esta clase debe ser Symfony\Component\HttpFoundation\File\File
                                $geadjuntoModel = StorageUtil::subir($adjunto["ruta"], $file);
                                $dataMail["adjuntos"][$key]["geadjunto_id"] = $geadjuntoModel->id;
                            } else {
                                $geadjuntoModel = Geadjunto::find($adjunto["geadjunto_id"]);
                            }

                            $message->attach($storagePath . "/" . $geadjuntoModel->ruta . "/" . $geadjuntoModel->nombrearchivo, [
                                'as' => $geadjuntoModel->nombre . '.' . $geadjuntoModel->extension,
                                'mime' => $geadjuntoModel->mimetype
                            ]);
                        }
                    }
                }
            );

            $mailsSended = $mailsSended . " " . $tmp;

            //Si logró enviar el correo reseteo la configuracion de correo si es necesario
            if ($smtpConfigBackup != null) {
                Mail::setSwiftMailer($smtpConfigBackup);
            }


            //El correo fue enviado correctamente por lo que lo guardo
            $saveMail = env("SAVE_EMAIL", true);

            if ($saveMail) {
                $mailEnviado = GeemailEnviado::create([
                    'asunto' => $dataMail['asunto'],
                    'cuerpo' => $dataMail['body'],
                    'remitente' => "<$fromMail> $fromName",
                    'para' => $to,
                    'cc' => (isset($dataMail['cc']) && strlen($dataMail['cc']) > 0) ? ($dataMail['cc']) : "",
                    'cco' => (isset($dataMail['cco']) && strlen($dataMail['cco']) > 0) ? ($dataMail['cco']) : "",
                    'objectgroup' => (isset($dataMail['objectgroup']) && strlen($dataMail['objectgroup']) > 0) ? ($dataMail['objectgroup']) : "",
                    'objectid' => (isset($dataMail['objectid']) && strlen($dataMail['objectid']) > 0) ? ($dataMail['objectid']) : ""
                ]);

                if (isset($dataMail['adjuntos'])) {
                    foreach ($dataMail['adjuntos'] as $adjunto) {
                        GeemailEnviadoAdjunto::create([
                            'geemail_enviado_id' => $mailEnviado->id,
                            'geadjunto_id' => $adjunto["geadjunto_id"]
                        ]);
                    }
                }
            }

            return $mailsSended;
        } catch (\Exception $e) {
            //En el caso de no poder enviar el correo electronico con la configuración personalizada de smtp
            //envia el correo con la configuracion del sistema
            if ($smtpSettings != null && !$smtpError) {
                //Configuracion personalizada pero falló
                Mail::setSwiftMailer($smtpConfigBackup);
                return SendMail::sendMail($dataMail, null, true, "Error configuración SMTP: " . $e->getMessage());
            }

            throw $e;
        }
    }

    /* Manda correo usando mandrill */
    /*
    public function enviarCorreo($dataMail)
    {
        $mandrill = new Mandrill(getenv('MANDRILL_SECRET'));
        // send transactional email
        $arrayDestinatarios = array();
        //prepararEmails
        foreach ($dataMail['destinatario'] as $emails) {
            array_push($arrayDestinatarios, array("email" => $emails, "type" => "to"));
        }
        if (isset($dataMail['cc']))
            foreach ($dataMail['cc'] as $emails) {
                array_push($arrayDestinatarios, array("email" => $emails, "type" => "cc"));
            }
        if (isset($dataMail['cco']))
            foreach ($dataMail['cco'] as $emails) {
                array_push($arrayDestinatarios, array("email" => $emails, "type" => "bcc"));
            }

        try {
            $message = array(
                'subject' => $dataMail['asunto'],
                'text' => "Valley Group", // O simplemente usar 'html ' para apoyar el formato HTML
                'from_email' => 'analistait1@valleygroups.com',
                'from_name' => 'ValleyPricing', //optional
                'to' => $arrayDestinatarios,
                'html' => $dataMail['cuerpo'],
                'track_opens' => TRUE,
                'track_clicks' => TRUE,
                'auto_text' => TRUE, // auto-converts html formatted emails to text
            );
            $result = $mandrill->messages->send($message);
        } catch (Mandrill_Error $e) {
            throw $e;
        }
        return $result;
    }
    */


    /**
     * @param $geemailId mixed es el código del registro en geemail para generar de manera dinamica
     * @param $data array. Puede ser cualquier dato, ya que se pasa al renderer el cual debe retornar obligatoriamente:
     * - [ASUNTO]: asunto del correo
     * - [BODY]: el cuerpo html del correo
     * - [to]: destinatario del correo
     * - opcionales: [cc] y [cco]
     *
     * - Estos valores de retorno son obligatorios debido a que se utiliza la plantilla generica de correos que solo
     * cuenta con asunto y body
     *
     */
    public static function sendDynamicEmail($geemailId, $data)
    {
        $result = SendMail::renderHtmlMail($geemailId, $data);
        //EventDispatcher::dispatchEvent("MANUAL_MAIL", $result, $result["[to]"], $mailGeneralTemplate->id);
        EventDispatcher::dispatchGeneralMail($result, $result["[to]"]);
    }

    /**
     * Toma un email, los datos y retorna el html del mail.
     * @param $geemailId
     * @param $data
     */
    public static function renderHtmlMail($geemailId, $data)
    {
        $geemailModel = Geemail::find($geemailId);
        if ($geemailModel) {
            $renderer = empty($geemailModel->funcion) ? "DefaultMailRenderer::render" : ($geemailModel->grupo . "\\" . $geemailModel->funcion);
            $result = call_user_func_array("App\\MailRenderers\\" . $renderer, array($geemailId, $data));

            //Obligatorios [asunto], [body], [to]
            if (array_key_exists("[ASUNTO]", $result) && array_key_exists("[BODY]", $result) && array_key_exists("[to]", $result)) {
                return $result;
            } else {
                throw new Exception("El renderer $renderer no retorna los parametros obligatorios");
            }
        } else {
            throw new Exception("GEEMAIL $geemailId no encontrado");
        }
    }

    public static function getFirmaCargo()
    {
        $logo = SystemParameters::getStrSysParam("API_URL") . "UploadsDocs/Fondos/VCAR_small.png";
        $usuario = JWTAuth::toUser()->name;
        $email = JWTAuth::toUser()->email;
        $result = "
        <table>
        <tr>
            <td rowspan=\"10\">
                <img src=\"$logo\" alt=\"\">
            </td>
            <td></td>
        </tr>
            <tr><td>$usuario</td></tr>
            <tr><td>Ocean Traffic</td></tr>
            <tr><td>COLOMBIA: +57 (2) 486 7979 Ext </td></tr>
            <tr><td>Calle 28 Norte 2BN - 68</td></tr>
            <tr><td>USA: +1 (305) 938 5658 Ext </td></tr>
            <tr><td>Email: $email</td></tr>
            <tr><td>ID WCA: 90854</td></tr>
            <tr><td><a href=\"http://valleygroups.com\">www.valleygroups.com</a></td></tr>
            <tr><td>Ver Video Institucional <a href=\"https://www.youtube.com/watch?v=DP2pl0FFB6Q\">Aqui</a></td></tr>
        </table>";
        return $result;
    }

    public static function formatearCorreo($listaCorreos)
    {
        if (substr($listaCorreos, -1) == ';') {
            $listaCorreos = substr($listaCorreos, 0, -1);
        }

        $correos = explode(";", $listaCorreos);

        $arrayResp = array(
            array(), array()
        );

        foreach ($correos as $correo) {
            if (str_contains($correo, "<")) {
                $explode_1 = explode("<", $correo);
                $explode_1[1] = str_replace('>', '', $explode_1[1]);

                $arrayResp[0][] = trim($explode_1[1]);
                $arrayResp[1][] = trim($explode_1[0]);
            } else {
                $arrayResp[0][] = trim($correo);
                $arrayResp[1][] = trim('');
            }
        }

        return $arrayResp;
    }
}