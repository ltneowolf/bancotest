<?php
/**
 * Created by PhpStorm.
 * User: desarrollador
 * Date: 19/04/2017
 * Time: 10:14 AM
 */

namespace App\Util;


class UtilFechas
{
    private static $meses = array(
        "1" => array("nombre" => 'Enero', "abrv" => 'Ene'),
        "2" => array("nombre" => 'Febrero', "abrv" => 'Feb'),
        "3" => array("nombre" => 'Marzo', "abrv" => 'Mar'),
        "4" => array("nombre" => 'Abril', "abrv" => 'Abr' ),
        "5" => array("nombre" => 'Mayo', "abrv" => 'May'),
        "6" => array("nombre" => 'Junio', "abrv" => 'Jun'),
        "7" => array("nombre" => 'Julio', "abrv"=> 'Jul'),
        "8" => array("nombre" => 'Agosto', "abrv" => 'Ago'),
        "9" => array("nombre" => 'Septiembre', "abrv" => 'Sep'),
        "10" => array("nombre" => 'Octubre', "abrv" => 'Oct'),
        "11" => array("nombre" => 'Noviembre', "abrv" => 'Nov'),
        "12" => array("nombre" => 'Diciembre', "abrv" => 'Dic'),
    );

    public static function getMesEsp($mes)
    {
        return self::$meses[$mes];
    }
}