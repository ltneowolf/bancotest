<?php
/**
 * Created by PhpStorm.
 * User: sistemas
 * Date: 01/02/2017
 * Time: 1:45 PM
 */

namespace App\Util;

use Carbon\Carbon;
use Excel;
use Tymon\JWTAuth\Facades\JWTAuth;
use Log;

class ExcelUtil
{

    /*
     * recibe un array con la siguiente estructura
     * $data = [
     *[ //array de n posiciones, una posicion por cada hoja del excel
     * "name" => "Hoja1", //hace referencia al nombre de la hoja
     * "additional" => "", // si se envia la hoja mostrara esta informacion en la quinta fila y agregara un encabezado
     * "data" => [
     * [
     * 'namecolumn1' => 'dato',
     * 'namecolumn2' => 'dato',
     * 'namecolumn3' => 'dato',
     * ]
     * ] //Array de n posiciones con la informacion que se desea mostrar, las columnas enviadas en columns deben existir en este array
     * "columns" => [ existen 3 tipos de datos para las columnas
     * ['name' => 'namecolumn1', 'label' => 'labelcolumn1', 'type' => 'number', 'precision' => 2, 'visible' => true], //en caso de que se envie un tipo number format hace referencia a la precision deseada, si no se envia format este pondra el dato tal y como este
     * ['name' => 'namecolumn2', 'label' => 'labelcolumn2', 'type' => 'date', 'format' => 'Y-m-d', 'visible' => true], //en caso de que se envie un tipo date format hace referencia al formato en carbon , si no se envia format esta se convertira en tipo text
     * ['name' => 'namecolumn3', 'label' => 'labelcolumn3', 'type' => 'text'], //text imprime el dato tal y como le llegue y no le importa format
     *]
     *],
     *[ //array de n posiciones, una posicion por cada hoja del excel
     * "name" => "Hoja2", //hace referencia al nombre de la hoja
     * "additional" => "", // si se envia la hoja comenzara en la tercera linea y mostrara esta informacion en la segunda fila
     * "data" => [
     * [
     * 'namecolumn1' => 'dato',
     * 'namecolumn2' => 'dato',
     * 'namecolumn3' => 'dato',
     * ]
     * ] //Array de n posiciones con la informacion que se desea mostrar, las columnas enviadas en columns deben existir en este array
     * "columns" => [ existen 3 tipos de datos para las columnas, si visible es 'false' || false entonces no se mostrara
     * ['name' => 'namecolumn1', 'label' => 'labelcolumn1', 'type' => 'number', 'precision' => 2, 'visible' => true], //en caso de que se envie un tipo number precision hace referencia a la precision deseada, si no se envia format este pondra el dato tal y como este
     * ['name' => 'namecolumn2', 'label' => 'labelcolumn2', 'type' => 'date', 'format' => 'Y-m-d', 'visible' => true], //en caso de que se envie un tipo date format hace referencia al formato en carbon , si no se envia format esta se convertira en tipo text
     * ['name' => 'namecolumn3', 'label' => 'labelcolumn3', 'type' => 'text'], //text imprime el dato tal y como le llegue y no le importa format
     *]
     *];
     * $languaje = "es"; //recibe el lenguaje seleccionado para la impresion al momento serian "us", "es" o "cn", por defecto que el app tiene por defecto el idioma español
     * $fileName = "archivo"; //recibe el nombre del archivo como se desea guardar y automaticamente le pone un random para evitar que se sobreescriban
     * $totales = false/true; //si se envia true al final de todas las hojas pone los totales de los campos tipo number, no totalizara nunca la primera columna
     */

    public static function generarExcel(array $data, $fileName, $languaje = "es", $totales = false)
    {
        if (empty($languaje) || !is_string($languaje) || empty($fileName) || !is_string($fileName)) {
            return response()->json("REQUEST_NOT_VALID", 400);
        }
        try {
            $languaje = strtolower($languaje);
            $fileName = $fileName . '_' . rand(1000, 10000);
            $resumen = array();
            for ($i = 0; $i < count($data); $i++) {
                array_push($resumen, self::procesarDataExcel($data[$i], $languaje, $totales));
            }

            $transTotales = LanguajeUtil::translate('TOTALES_LABEL', $languaje);

            return Excel::create($fileName, function ($excel) use ($data, $totales, $resumen, $transTotales, $languaje) {
                for ($i = 0; $i < count($data); $i++) {
                    $excel->sheet($data[$i]["name"], function ($sheet) use ($data, $i, $totales, $resumen, $transTotales, $languaje) {
                        if ($totales === true) {
                            array_push($data[$i]["data"], $resumen[$i]);
                        }
                        $comienzaEn = 1;
                        if (isset($data[$i]["additional"]) && !empty($data[$i]["additional"])) {
                            $user = JWTAuth::toUser();
                            $generado_por = LanguajeUtil::translate("GENERADO_LABEL", $languaje) . ": " . $user->name . " (" . $user->email
                                . ") - " . Carbon::now()->toDateTimeString();
                            $comienzaEn = 5;
                            $sheet->mergeCells('A1:H1');
                            $sheet->mergeCells('A2:H2');
                            $sheet->mergeCells('A3:H3');
                            $sheet->row(1, array("Box Express"));
                            $sheet->row(1, function ($row) {
                                $row->setFont(array(
                                    'bold' => true
                                ));
                            });
                            $sheet->row(2, array($data[$i]["additional"]));
                            $sheet->row(3, array($generado_por));
                        }
                        $sheet->fromArray($data[$i]["data"], null, 'A' . $comienzaEn, true);
                        $columns = array();
                        for ($j = 0; $j < count($data[$i]["columns"]); $j++) {
                            $visible = (!isset($data[$i]["columns"][$j]['visible'])) ? true : $data[$i]["columns"][$j]['visible'];
                            if ($visible !== false && $visible !== 'false') {
                                array_push($columns, $data[$i]["columns"][$j]["label"]);
                            }
                        }

                        $sheet->row($comienzaEn, $columns);
                        $sheet->row($comienzaEn, function ($row) {
                            $row->setFont(array(
                                'bold' => true
                            ));
                        });
                        if ($totales === true) {
                            $sheet->row(count($data[$i]["data"]) + $comienzaEn, function ($row) {
                                $row->setFont(array(
                                    'bold' => true
                                ));
                            });
                            $sheet->row(count($data[$i]["data"]) + $comienzaEn, array($transTotales));
                        }
                    });
                }
            })->export('xlsx');/*->store('xlsx', public_path() . "/downloadDocs");*/
            //return response()->download(public_path() . "/downloadDocs/" . $fileName . ".xlsx", $fileName . ".xlsx");
        } catch (\Exception $ex) {
            Log::error($ex);
            return response()->json("INTERNAL_ERROR", 400);
        }
    }

    public static function procesarDataExcel(array &$data, $languaje, $totales = false)
    {

        for ($j = 0; $j < count($data["columns"]); $j++) {
            $visible = (!isset($data["columns"][$j]['visible'])) ? true : $data["columns"][$j]['visible'];
            if ($visible !== false && $visible !== 'false') {
                $data["columns"][$j]["label"] = LanguajeUtil::translate($data["columns"][$j]["label"], $languaje);
            }
        }
        $total = array();
        for ($j = 0; $j < count($data["data"]); $j++) {
            $temp = array();

            for ($k = 0; $k < count($data["columns"]); $k++) {
                $column = $data["columns"][$k]['name'];
                $type = (!isset($data["columns"][$k]['type'])) ? 'text' : $data["columns"][$k]['type'];
                $visible = (!isset($data["columns"][$k]['visible'])) ? true : $data["columns"][$k]['visible'];
                if ($visible !== false && $visible !== 'false') {
                    if ($type == 'number') {
                        if (!isset($data["columns"][$k]['precision'])) {
                            $temp[$column] = $data["data"][$j][$column];
                        } else {
                            $precision = $data["columns"][$k]['precision'];
                            $temp[$column] = round($data["data"][$j][$column], $precision);
                        }
                        if ($totales === true) {
                            if (!isset($total[$column])) {
                                $total[$column] = 0;
                            }
                            $total[$column] += $temp[$column];
                        }
                    } else if ($type == 'date') {
                        if (!isset($data["columns"][$k]['format'])) {
                            $temp[$column] = $data["data"][$j][$column];
                        } else {
                            $format = $data["columns"][$k]['format'];
                            if (!is_null($data["data"][$j][$column]) && !empty($data["data"][$j][$column])) {
                                $temp[$column] = Carbon::parse($data["data"][$j][$column])->format($format);
                            } else {
                                $temp[$column] = $data["data"][$j][$column];
                            }
                        }
                        if ($totales === true) {
                            $total[$column] = '';
                        }
                    } else {
                        $temp[$column] = $data["data"][$j][$column];
                        if ($totales === true) {
                            $total[$column] = '';
                        }
                    }
                }
            }
            $data["data"][$j] = $temp;
        }
        return $total;
    }

    public static function generarXLS(array $data, $languaje = "es", $totales = false)
    {
        if (empty($languaje) || !is_string($languaje)) {
            return response()->json("REQUEST_NOT_VALID", 400);
        }
        $languaje = strtolower($languaje);
        $total = self::procesarDataExcel($data, $languaje, $totales);
        $transTotales = LanguajeUtil::translate('TOTALES_LABEL', $languaje);
        $params = [];
        $params["[TABLA]"] = "<table style='font-family: \"Helvetica Neue\", Arial, Helvetica, sans-serif; width: 100%;'><thead><tr>[TITULOS]</tr></thead><tbody>[DATA]</tbody></table>";
        $params["[TITULOS]"] = "";
        $params["[DATA]"] = "";

        for ($j = 0; $j < count($data["columns"]); $j++) {
            $visible = (!isset($data["columns"][$j]['visible'])) ? true : $data["columns"][$j]['visible'];
            if ($visible !== false && $visible !== 'false') {
                $params["[TITULOS]"] .= "<th style='width: 100%;'>" . $data["columns"][$j]["label"] . "</th>";
            }
        }
        $columns = array();
        for ($j = 0; $j < count($data["columns"]); $j++) {
            $visible = (!isset($data["columns"][$j]['visible'])) ? true : $data["columns"][$j]['visible'];
            if ($visible !== false && $visible !== 'false') {
                array_push($columns, $data["columns"][$j]["label"]);
            }
        }
        for ($i = 0; $i < count($data["data"]); $i++) {
            $params["[DATA]"] .= "<tr>";
            foreach ($data["data"][$i] as $value) {
                $params["[DATA]"] .= "<td>" . str_replace(array("\r\n", "\r", "\n"), "", $value) . "</td>";
            }
            $params["[DATA]"] .= "</tr>";
        }
        if ($totales === true) {
            $params["[DATA]"] .= "<tr>";
            $i = 0;
            foreach ($total as $value) {
                if ($i == 0) {
                    $params["[DATA]"] .= "<td><b>" . $transTotales . "</b></td>";
                    $i++;
                } else {
                    $params["[DATA]"] .= "<td><b>" . $value . "</b></td>";
                    $i++;
                }
            }
            $params["[DATA]"] .= "</tr>";
        }
        $params["[NAME]"] = $data["name"];
        $html = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">'
            . '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>'
            . '<x:Name>[NAME]</x:Name>'
            . '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>'
            . '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>'
            . '[TABLA]'
            . '</body></html>';

        $html = str_replace(array_keys($params), $params, $html);
        return response($html)
            ->header('Content-Type', 'application/vnd.ms-excel')
            ->header('Content-Disposition', 'attachment; filename=excel.xls');
    }

}