<?php
/**
 * Created by PhpStorm.
 * User: SV
 * Date: 21/09/2016
 * Time: 14:20
 */

namespace App\Util;

use Milon\Barcode\DNS1D;
use Milon\Barcode\DNS2D;


class BarCodeUtils
{

    /**
     * Convert string into barcode SVG representation.
     *
     * @param $type (string) type of barcode; only available: C39, C39+, C39E, C39E+, C93, S25, S25+, I25, I25+, C128,
     * C128A, C128B, C128C, EAN2, EAN5, EAN8, EAN13, UPCA, UPCE, MSI, MSI+, POSTNET, PLANET, RMS4CC, KIX, IMB, CODABAR,
     * CODE11, PHARMA, PHARMA2T, DATAMATRIX, PDF417, QRCODE
     * @param $value (string) value to print.
     * @param $width (int) Width of a single bar element in pixels.
     * @param $height (int) Height of a single bar element in pixels.
     * @return $this
     */
    public static function getBarCodeUtil($type, $value, $width, $height)
    {
        $barCod = "";
        $dnsdType = false;

        switch ($type) {
            case "C39":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C39+":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C39E":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C39E+":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C93":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "S25":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "S25+":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "I25":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "I25+":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C128":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C128A":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C128B":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "C128C":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "EAN2":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "EAN5":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "EAN8":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "EAN13":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "UPCA":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "UPCE":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "MSI":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "MSI+":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "POSTNET":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "PLANET":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "RMS4CC":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "KIX":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "IMB":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "CODABAR":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "CODE11":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "PHARMA":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "PHARMA2T":
                $dnsdType = true;
                $barCod = new DNS1D();
                break;
            case "DATAMATRIX":
                $dnsdType = true;
                $barCod = new DNS2D();
                break;
            case "PDF417":
                $dnsdType = true;
                $barCod = new DNS2D();
                break;
            case "QRCODE":
                $dnsdType = true;
                $barCod = new DNS2D();
                break;
        }

        if ($dnsdType) {
            if ($value) {
                if ($width and $height) {
                    return '<img src="data:image/png;base64,' . $barCod->getBarcodePNG($value, $type, $width, $height)
                    . '" />';
                } else {
                    return '<img src="data:image/png;base64,' . $barCod->getBarcodePNG($value, $type)
                    . '" />';
                }
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

}