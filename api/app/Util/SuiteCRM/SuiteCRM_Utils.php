<?php

/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 5/07/2017
 * Time: 11:02 AM
 */
namespace App\Util\SuiteCRM;

use App\Jobs\app\SuiteCRM_SincClientes;
use Illuminate\Support\Facades\Log;

class SuiteCRM_Utils
{
    public static  $SINC_ESTATUS_PARAM = "ESTADO_SINC_CLIENTES_CRM";

    /**
     * @param null $username
     * @param null $password
     * @param null $suiteUrl
     * @return mixed
     * @throws Exception - Quien llama el metodo debe manejar el error
     */
    public static function login($username = null, $password = null, $suiteUrl = null){

        if( is_null($username) ){

            $username = config("controlvalley.crm.crm_user", false);
            if( $username === false ){
                throw  new \Exception("Falta el parametro CRM_USER en el archivo .env o controlvalley.crm.crm_user en config");
            }
        }

        if( is_null($password) ){
            $password = config('controlvalley.crm.crm_pass', false);
            if( $password === false ){
                throw  new \Exception("Falta el parametro CRM_PASS en el archivo .env o controlvalley.crm.crm_pass en config");
            }
        }

        $login_parameters = array(
            "user_auth" => array(
                "user_name" => $username,
                "password" => md5($password),
                "version" => "1"
            ),
            "application_name" => "ControlPlus",
            "name_value_list" => array(),
        );

        $loginResult = SuiteCRM_Utils::callMethod("login", $login_parameters, $suiteUrl);

        if( isset($loginResult->id) ){
            return $loginResult;
        }

        if( isset($loginResult->name) && $loginResult->name == "Invalid Login" ){
            //Login invalido
            return false;
        }

        return SuiteCRM_Utils::throwError($loginResult);
    }

    /**
     * @param $method
     * @param $parameters
     * @param null $suiteUrl
     * @return mixed
     * @throws \Exception - Quien llama el metodo debe manejar el error
     */
    public static function callMethod($method, $parameters, $suiteUrl = null){

        if( is_null($suiteUrl) ){
            $suiteUrl = config("controlvalley.crm.url_crm");
            if( $suiteUrl === false ){
                throw  new \Exception("Falta el parametro URL_CRM en el archivo .env o controlvalley.crm.url_crm en config");
            }
        }
        ob_start();
        $curl_request = curl_init();

        try {
            $url_ = $suiteUrl;
            curl_setopt($curl_request, CURLOPT_URL, $url_);
            curl_setopt($curl_request, CURLOPT_POST, 1);
            curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            curl_setopt($curl_request, CURLOPT_HEADER, 1);
            curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);

            $jsonEncodedData = json_encode($parameters);

            $post = array(
                "method" => $method,
                "input_type" => "JSON",
                "response_type" => "JSON",
                "rest_data" => $jsonEncodedData
            );

            curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
            $result = curl_exec($curl_request);
            curl_close($curl_request);

            $result = explode("\r\n\r\n", $result, 2);
            $response = json_decode($result[1]);

            return $response;
        } finally {
            ob_end_flush();
        }
    }

    public static function throwError($result){
        $error = "Error inesperad, objeto de respuesta desde el crm: \n\n";
        $error .= print_r( $result, true );
        throw new \Exception($error);
    }

    public static function logVarDump($var, $text = "")
    {
        /*ob_start();
        var_dump($var);
        $result = ob_get_clean();*/
        $result = print_r($var, true);
        Log::info("------------------Start: $text----------");
        Log::info($result);
        Log::info("------------------End: $text----------");
    }


}