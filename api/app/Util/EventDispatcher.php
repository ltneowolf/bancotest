<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 08/08/2016
 * Time: 7:59 PM
 */

namespace App\Util;

use App\Http\Controllers\BaseController;
use App\Models\GE\Getask;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class EventDispatcher
{

    public static function dispatchEvent($event, $params, $destinatarios = "", $templateId = ""){
        $tarea = new Getask();
        $tarea->alerta = $event;
        $tarea->datos = serialize($params);
        $tarea->intentos = "0";
        $tarea->error = "0";
        $tarea->traza = "N/A";
        $tarea->finalizado = "0";
        $tarea->getemplate_id = $templateId."";

        $tarea->destinatario = $destinatarios;

        try{
            if( env("CREA_GUIAS", false) ){
                $usuario = User::find(1);
            }else{
                $usuario = JWTAuth::toUser();
            }
            $tarea->loggedusermail = $usuario->email;

        }catch(\Exception $ex){
            //No hay tocken lo registro sin usuario
            if( ! env("CREA_GUIAS", false) ){
                BaseController::logEx($ex);
            }
        }

        $tarea->save();
    }

    public static function dispatchManualEvent($params, $destinatarios = "", $templateId = ""){
        EventDispatcher::dispatchEvent("MANUAL_MAIL", $params, $destinatarios, $templateId);
    }

    public static function dispatchGeneralMail($params, $destinatarios = ""){
        $mailGeneralTemplate = TemplateUtils::findTemplate("MENSAJE_GENERAL");
        if ($mailGeneralTemplate) {
            EventDispatcher::dispatchManualEvent($params, $destinatarios, $mailGeneralTemplate->id);
        } else {
            throw new \Exception("Plantilla 'MENSAJE_GENERAL' no registrada en el sistema");
        }
    }

}