<?php

namespace App\Util;

use App\Models\GE\GelangDef;

class LanguajeUtil
{

    //metodo para traducir label, en caso de que no se encuentre devuelve el mismo label ingresado
    public static function translate($label, $languaje = 'es')
    {
        $lang_def = GelangDef::join('gelanguages lang', 'gelang_defs.gelanguage_id', '=', 'lang.id')
            ->where('lang.lang', '=', $languaje)
            ->where('gelang_defs.key', '=', strtoupper($label))
            ->select('gelang_defs.value')
            ->first();
        if (is_null($lang_def)) {
            return $label;
        } else {
            return $lang_def["value"];
        }
    }

    //metodo para traducir arrays de label recibe array('label1', 'label2', ...) y retorna array('label1' => 'trans1', 'label2' => 'trans2', ...)
    public static function translateArray($labels, $languaje = 'es')
    {
        $traducciones = array();
        for ($i = 0; $i < count($labels); $i++) {
            $traducciones[$labels[$i]] = self::translate($labels, $languaje);
        }
        return $traducciones;
    }
}