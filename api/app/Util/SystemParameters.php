<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 13/09/2016
 * Time: 3:14 PM
 */

namespace App\Util;

use App\Models\GE\Geparameters;

use DB;
use Log;

class SystemParameters
{

    /**
     * Retorna un parametro del sistema
     * @param $key String contiene el parametro del sistema.
     * @param $delimiter String si se envia este parametro el sistema retornara un array, este parametro sirve para
     * delimitar el array.
     * @return string Parametro del sistema del tipo String. Retorna VALUE_NOT_FOUND si no encuentra el valor.
     */
    public static function getStrSysParam($key, $delimiter = null)
    {

        $result = Geparameters::whereRaw("upper(geparameters.key) = ?", array(strtoupper($key)))->select('strval')->first();

        if (count($result) == 0 || !isset($result["strval"])) {
            return "VALUE_NOT_FOUND";
        }

        if ($delimiter != null) {
            return explode($delimiter, $result["strval"]);
        } else {
            return $result["strval"];
        }
    }

    /**
     * Retorna un parametro del sistema
     * @param $key String Llave que contiene el parametro del sistema.
     * @return Double Parametro del sistema del tipo numerico. Retorna VALUE_NOT_FOUND si no encuentra el valor.
     */
    public static function getNumSysParam($key)
    {

        $result = Geparameters::whereRaw("upper(geparameters.key) = ?", array(strtoupper($key)))->select('numval')->first();

        if (count($result) == 0 || !isset($result["numval"])) {
            return "VALUE_NOT_FOUND";
        }

        return $result["numval"];
    }


    public static function setSysParam($tipo, $key, $value, $obs = "N/A", $modulo = "1")
    {
        $result = Geparameters::
        whereRaw(
            "upper(geparameters.key) = ?", array(strtoupper($key))
        )->first();
        if (count($result) == 0) {

            $data = ['key' => $key,
                'numval' => $tipo == 'N' ? $value : '',
                'strval' => $tipo == 'S' ? $value : '',
                "gemodulo_id" => $modulo];

            if ($obs != "N/A") {
                $data["observation"] = $obs;
            }

            //No se encontró se crea
            $result = Geparameters::create($data);

        } else {
            if ($tipo == 'N') {
                $result->numval = $value;
            }

            if ($tipo == 'S') {
                $result->strval = $value;
            }

            if ($obs != "") {
                $result->observation = $obs;
            }
            $result->save();
        }

        return $result;
    }

    public static function setNumSysParam($key, $value, $obs = "N/A", $modulo = "1")
    {
        return SystemParameters::setSysParam('N', $key, $value, $obs, $modulo);
    }

    public static function setStrSysParam($key, $value, $obs = "N/A", $modulo = "1")
    {
        return SystemParameters::setSysParam('S', $key, $value, $obs, $modulo);
    }

    /**
     * Get Parameters
     * 
     * Sirve para consultar multiples parametros del sistema al mismo tiempo
     * 
     * @param Array $keys
     * @return Array Con la siguiente estructura: [key => ['numval' => 0, 'strval' => ""], ...] 
     *  donde key es el nombre del parametro del sistema en mayuscula
     */
    public static function getParameters($keys)
    {
        $resultQuery = Geparameters::whereIn("key", $keys)
            ->select('strval','numval',DB::raw('upper(key) key'))
            ->get();

        $parameters = array();

        foreach($resultQuery as $param)
        {
            $parameters[$param->key] = [ 
                'strval' => is_null($param->strval) ? 'VALUE_NOT_FOUND': $param->strval, 
                'numval' => is_null($param->numval) ? 'VALUE_NOT_FOUND': $param->numval 
            ];
        }

        if(count($parameters) != count($keys))
        {
            foreach($keys as $k => $key) 
            {
                if(!array_key_exists($key, $parameters)){
                    $parameters[$key] =  [ 'strval' => 'VALUE_NOT_FOUND', 'numval' => 'VALUE_NOT_FOUND' ];
                }
            }
        }

        return $parameters;
    }

}