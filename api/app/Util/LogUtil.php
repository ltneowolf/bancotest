<?php
/**
 * Created by PhpStorm.
 * User: sistemas
 * Date: 10/02/2017
 * Time: 17:50
 */

namespace App\Util;


use App\Http\Controllers\BaseController;
use App\Models\GE\Gelog;
use App\Models\User;
use Log;
use JWTAuth;
use Validator;

class LogUtil
{
    /**
     * Function que permite el registro de logs
     * @param array $data Un array con los siguientes campos
     *          -mensaje        -Mensaje a guardar
     *          -proceso_id     -Es un id de la tabla Getipos que me indica el proceso que esta generando el LOG.
     *          -ref_registro   -Es el ID o Código del registro que esta generando el log.
     *          -no_login       -Es un boolean, le indica al sistema si se debe validar la sesión, si no se registra
     *                              con el id del administrador.
     */
    static function saveLog($data)
    {

        if (!array_key_exists("no_login", $data)) {
            $data["no_login"] = false;
        }

        if ($data["no_login"]) {
            try {
                $user = User::findOrFail(SystemParameters::getNumSysParam("USUARIO_LOG_CONTROL"));
            } catch (\Exception $ex) {
                BaseController::logEx($ex);
                throw new \Exception($ex->getMessage(), $ex->getCode());
            }
        } else {
            $user = JWTAuth::toUser();
        }

        $data["user_id"] = $user->id;

        $rules = array(
            array('user_id' => 'required',
                'proceso_id' => 'required',
                'mensaje' => 'required',
                'ref_registro' => 'required'
            ),
            array('user_id.required' => 'REQUIRED_:attribute',
                'mensaje.required' => 'REQUIRED_:attribute',
                'proceso_id.required' => 'REQUIRED_:attribute',
                'ref_registro.required' => 'REQUIRED_:attribute'
            )
        );

        $validator = Validator::make($data,
            $rules[0],
            $rules[1]);

        if ($validator->fails()) {
            $errores = $validator->errors()->toArray();
            throw new \Exception($errores);
        }

        try {
            $gelog = New Gelog;
            $gelog->fill($data);
            $gelog->save();
        } catch (\Exception $ex) {
            BaseController::logEx($ex);
            throw new \Exception($ex->getMessage(), $ex->getCode());
        }

    }
}