<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 11/08/2016
 * Time: 9:46 AM
 */

namespace App\Util;

use Illuminate\Support\Facades\Storage;

class PdfUtils
{
    /**
     * Toma una plantilla y la renderiza en un pdf utilizando los datos suministrados
     * @param $templateId
     * @param $templateParams
     * @param string $papel
     * @param string $orientacion
     * @param null $filename
     * @param string $type
     * @param string $path
     * @return mixed
     */
    public static function renderPdfTemplate($templateId, $templateParams, $papel = 'A4', $orientacion = 'portrait', $filename = null, $type = "stream"){
        if( $filename == null ){
            $filename = "print $templateId .pdf";
        }
        try{
            $html = TemplateUtils::renderHtmlTemplate($templateId, $templateParams);
        }catch(\Exception $ex){
            $html = 'ERROR: '.$ex->getMessage();
        }
        if( $type == "stream" ){
            return self::streamPdfHtml($html, $papel, $orientacion, $filename);
        }
        if( $type == "file" ){
            return self::filePdfHtml($html, $papel, $orientacion, $filename);
        }
        return "TIPO NO SOPORTADO";

    }

    public static function streamPdfTemplate($templateId, $templateParams, $papel = 'A4', $orientacion = 'portrait', $filename = null){
        return self::renderPdfTemplate($templateId, $templateParams, $papel, $orientacion, $filename, "stream");
    }

    public static function streamFilePdfTemplate($templateId, $templateParams, $papel = 'A4', $orientacion = 'portrait', $filename = null){
        return self::renderPdfTemplate($templateId, $templateParams, $papel, $orientacion, $filename, "file");
    }

    private static function renderPdfHtml($html, $papel = 'A4', $orientacion = 'portrait', $filename = null, $type = "stream"){
        if( $filename == null ){
            $filename = "print.pdf";
        }
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setPaper($papel, $orientacion);

        $pdf->loadHTML( $html );
        //return $html;

        switch ($type){
            case "file":
                //$pdf->d();
                $output = $pdf->stream();
                $filenameGen = md5(microtime());
                Storage::put("PDFUTIL/".$filenameGen, $output);
                return array(
                    "fileid" => $filenameGen,
                    "ruta" => "PDFUTIL",
                    "filename" => $filename,
                    "content_type" => "application/pdf"
                );
            default:
                return $pdf->stream($filename);
        }
    }

    public static function streamPdfHtml($html, $papel = 'A4', $orientacion = 'portrait', $filename = null){
        return self::renderPdfHtml($html, $papel, $orientacion, $filename, "stream");
    }
    public static function filePdfHtml($html, $papel = 'A4', $orientacion = 'portrait', $filename = null){
        return self::renderPdfHtml($html, $papel, $orientacion, $filename, "file");
    }


}