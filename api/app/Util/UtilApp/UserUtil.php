<?php
/**
 * Created by PhpStorm.
 * User: desarrollador
 * Date: 25/05/2017
 * Time: 7:54 AM
 */

namespace App\Util\UtilApp;

use App\Models\User;
use JWTAuth;
use Illuminate\Support\Facades\Hash;
use DB;

class UserUtil
{
    public static function changePassword($old, $new)
    {
        $user = JWTAuth::toUser();
        if (!Hash::check($old, $user->password) || is_null($new) || empty($new)) {
            return array('ERROR' => 'PASS_NOT_VALID');
        }
        $user->password = $new;
        $user->save();
        return array("SUCCESS" => "PASS_UPDATE_OK_LABEL");
    }
}