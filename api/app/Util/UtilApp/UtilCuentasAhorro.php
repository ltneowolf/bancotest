<?php

namespace App\Util\UtilApp;

use App\Models\TEST\Cliente;
use App\Models\TEST\CtaahoTransaccione;
use App\Models\TEST\CuentaAhorro;
use App\Util\SystemParameters;
use Log;

class UtilCuentasAhorro
{
    public static function getAccountNumber()
    {
        do {
            $number = mt_rand(100000000, 999999999);
        } while (!empty(CuentaAhorro::where('numero_cuenta', $number)->first()));

        return $number;
    }

    public static function setClaveCuenta($cliente_id)
    {
        $cliente = Cliente::find($cliente_id);

        if (is_null($cliente)) {
            return null;
        }

        $clave = substr($cliente->identificacion, -4);

        return $clave;
    }

    public static function verificarApertura($cuenta_id, $monto, $tipo)
    {
        $cuentaIsntance = CuentaAhorro::find($cuenta_id);

        $msgRespuesta = array(
            'tipo' => true,
            'msg' => ''
        );

        if (is_null($cuentaIsntance)) {
            $msgRespuesta['tipo'] = false;
            $msgRespuesta['msg'] = 'CTA_NO_EXISTE_LABEL';
        }

        if (!$cuentaIsntance->estado) {
            $msgRespuesta['tipo'] = false;
            $msgRespuesta['msg'] = 'CTA_NO_ACTIVADA_LABEL';

            return $msgRespuesta;
        }

        if ($monto <= 0) {
            $msgRespuesta['tipo'] = false;
            $msgRespuesta['msg'] = 'MONTO_NEGATIVO_LABEL';

            return $msgRespuesta;
        }

        $minimo = SystemParameters::getNumSysParam("MINIMO_ACTIVACION_CUENTA");
        $tipConsignacion = SystemParameters::getNumSysParam("TIPO_TRS_CONSIGNACION");
        $tipRetiro = SystemParameters::getNumSysParam("TIPO_TRS_RETIRO");

        $consignacion = CtaahoTransaccione::where('monto', '>=', $minimo)
            ->where('tipo_transaccion', $tipConsignacion)
            ->first();

        if (is_null($consignacion)) {

            if ($tipo == $tipConsignacion && $monto >= $minimo) {
                return $msgRespuesta;
            } else {

                $msgRespuesta['tipo'] = false;
                $msgRespuesta['msg'] = 'NOT_CONSIGNA_DISPONIBLE_LABEL';

                return $msgRespuesta;
            }
        }

        if ($tipo == $tipRetiro) {
            if ($cuentaIsntance->saldo < $monto) {
                $msgRespuesta['tipo'] = false;
                $msgRespuesta['msg'] = 'MONTO_INFERIOR_SALDO_LABEL';

                return $msgRespuesta;
            }
        }

        return $msgRespuesta;
    }
}