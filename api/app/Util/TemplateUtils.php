<?php
/**
 * Created by PhpStorm.
 * User: Daniel
 * Date: 11/08/2016
 * Time: 9:46 AM
 */

namespace App\Util;


use App\Models\GE\Getemplate;
use JWTAuth;
use Carbon\Carbon;
use DB;

class TemplateUtils
{
    /**
     * @var array Estos indices no pueden ser utilizados como tablas
     */
    public static $INDICES_RESERVADOS_TABLAS = ["adjuntos"];

    /**
     *
     * @param $idTemplate - Es el id o nombre de la plantilla a renderizar
     * @param $datos
     * @return array con las siguientes posiciones:
     * asunto, body
     * @throws \Exception en caso de que no exista la plantilla
     */
    public static function renderMailTemplate($idTemplate, $datos)
    {

        if (is_numeric($idTemplate)) {
            $template = Getemplate::find($idTemplate);
        } else {
            $template = self::findTemplate($idTemplate);
        }

        if (!$template) {
            return array('body' => "TEMPLATE_" . $idTemplate . "_NOT_FOUND", 'asunto' => "TEMPLATE_" . $idTemplate . "_NOT_FOUND");
        }
        //1) renderizo las tablas de ser necesario
        $nuevosDatos = self::generarTablas($datos);

        //2) genero el nuevo asunto
        $asunto = str_replace(array_keys($nuevosDatos), $nuevosDatos, $template->asunto);
        $body = str_replace(array_keys($nuevosDatos), $nuevosDatos, $template->cuerpomsg);

        return array_merge($datos, array('asunto' => $asunto, 'body' => $body));
    }

    /**
     *
     * @param $idTemplate - Es el id de la plantilla a renderizar
     * @param $datos
     * @return string con el HTML que representa la plantailla. se toma desde la template el atributo body
     * @throws \Exception en caso de que no exista la plantilla
     */
    public static function renderHtmlTemplate($idTemplate, $datos)
    {
        $template = self::renderMailTemplate($idTemplate, $datos);

        return $template['body'];
    }

    /**
     * Funcion encargada de recorrer la data de la plantilla, obtener los arrays y convertirlos en tablas.
     * Cada elemento del arraay que se recibe, se evalua si es un array se convierte en una tabla html.
     * Esta funcion cambia el array por una tabla html
     * @param $arrayData Entre sus valores debe tener un array con la siguiente estructura
     * array(
     *      ["titulo1"=>"valor", "titulo2"="valor"],
     *      ["titulo1"=>"valor", "titulo2"="valor"],
     *      ["titulo1"=>"valor", "titulo2"="valor"],
     *      ,..
     * )
     * @return array El array con la tabla
     */
    public static function generarTablas($arrayData)
    {
        $arrayDataWhTbl = collect($arrayData)->map(function ($item, $key) {
            //Si es un array entonces genero la tabla
            if (is_array($item) && !in_array($key, self::$INDICES_RESERVADOS_TABLAS)) {
                if (count($item) > 0) {
                    //Obtengo las Keys del primer valor las cuales serán los encabezados de la tabla
                    $titulos = array_keys($item[0]);
                    $tbTitulo = "<tr>";
                    foreach ($titulos as $titulo) {
                        $tbTitulo .= "<td align='center>'><b>" . strtoupper($titulo) . "</b></td>";
                    }
                    $tbTitulo .= "</tr>";

                    $tbCuepo = "";
                    foreach ($item as $valor) {
                        $tbCuepo .= "<tr>";
                        foreach ($titulos as $indice) {
                            $tbCuepo .= "<td>" . $valor[$indice] . "</td>";
                        }
                        $tbCuepo .= "</tr>";
                    }
                    $item = "<table>" . $tbTitulo . $tbCuepo . "</table>";
                } else {
                    $item = "<span>No hay datos para mostrar en la tabla</span>";
                }
            } else if (in_array($key, self::$INDICES_RESERVADOS_TABLAS)) {
                return "";
            }

            return $item;
        })->toArray();

        return $arrayDataWhTbl;
    }

    public static function findTemplate($templateName)
    {
        return Getemplate::where(DB::raw('upper(nombre)'), strtoupper($templateName))->get()->first();
    }
}