<?php

namespace App\Console;

use App\Console\Commands\LimpiarGetasks;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\Alertas::class,
        \App\Console\Commands\ActualizaMoneda::class,
        LimpiarGetasks::class

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('alertas')
            ->everyMinute()
            ->appendOutputTo("storage/logs/scheduler.log");

        $schedule->command('actualizaMoneda')
            ->dailyAt('06:00')
            ->appendOutputTo("storage/logs/currency.log");

        $schedule->command('limpiarGetasks')
            ->dailyAt('02:00')
            ->appendOutputTo("storage/logs/limpiarGetasks.log");
    }
}
