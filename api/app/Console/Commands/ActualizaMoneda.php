<?php

namespace App\Console\Commands;

use App\Models\GE\Gemoneda;
use App\Util\EventDispatcher;
use App\Util\StaticContent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use SoapClient;


class ActualizaMoneda extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'actualizaMoneda';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando encargado de actualizar los factores de conversion de las monedas';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
         * Utilizamos curl para conectarnos al recurso de Currency Layer, este recurso recibe las monedas que deseamos convertir
         * y el factor de conversion que deseamos manejar. El API nos devulve el valor de la moneda en ese momento de
         * la consulta.
         * El recurso retorna un objeto como el siguiente:
            {
              "success":true,
              "terms":"https:\/\/currencylayer.com\/terms",
              "privacy":"https:\/\/currencylayer.com\/privacy",
              "timestamp":1533736882,
              "source":"USD",
              "quotes":{
                "USDEUR":0.8628,
                "USDGBP":0.776799,
                "USDCAD":1.30684,
                "USDCOP":2900
              }
            }
         *
         */

        //Se omite la de colombia debido a que es consultada en el web service de la superitendencia y la de USA porque
        //es la referencia
        $gemonedas = Gemoneda::select('abreviatura')->whereNotIn('abreviatura', ['USD', 'COP'])->get();

        //Retorno todas las monedas separadas por coma
        $currencies = $gemonedas->implode('abreviatura', ',');

        $financeURL = getenv('CURRENCY_LAYER_API')
            . getenv('CURRENCY_LAYER_API_LIVE_ENDPOINT')
            . '?access_key=' . getenv('CURRENCY_LAYER_ACCES_KEY')
            . '&source=USD'
            . '&currencies=' . $currencies
            . '&format=1';

        if (isset($currencies) && $currencies != '') {
            $curl_obj = curl_init($financeURL);
            curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, true);

            $financeURLResponse = curl_exec($curl_obj);

            //Reviso que la petición fue correcta.
            if ($financeURLResponse == false) {

                $mensaje = "Error al actualizar la moneda: El recurso no esta disponible"
                    . "\nURL del recurso: " . $financeURL
                    . "\nCURL Error: " . curl_error($curl_obj)
                    . "\nCURL Error Code " . curl_errno($curl_obj);

                curl_close($curl_obj);

                Log::critical($mensaje);

                EventDispatcher::dispatchEvent('ERROR_ACTUALIZA_MONEDA', ["[cuerpo]" => $mensaje]);

                return false;

            } else {

                curl_close($curl_obj);
                $financeObj = json_decode($financeURLResponse);
                $validarJSON = StaticContent::validarJSON(json_last_error());

                //Valido que el JSON me retorne información.
                if ($validarJSON['codigo'] == 'JSON_ERROR_NONE') {

                    //Reviso la cantidad de divisas que me retrona el recurso, si es 0 retorno el error;
                    if ($financeObj->success == false) {
                        $mensaje = "Error al actualizar la moneda: El recurso retorno error";
                        $mensaje .= "\nRespuesta: [" . $financeObj->error->code . "] " . $financeObj->error->info;
                        Log::critical($mensaje);

                        EventDispatcher::dispatchEvent('ERROR_ACTUALIZA_MONEDA', ["[cuerpo]" => $mensaje]);

                        return false;
                    }

                    //Recorro las monedas para actualizar el factor de conversion
                    foreach ($financeObj->quotes as $key => $quote) {

                        try {
                            //La llave siempre va a empezar con la palara USD mas el nombre de la moneda Ejemplo USDCOP
                            $abreviatura = substr($key, 3);
                            Gemoneda::where('abreviatura', $abreviatura)
                                ->update(["factor_conversion" => round($quote, 2, PHP_ROUND_HALF_UP)]);
                        } catch (\Exception $ex) {

                            $mensaje = "Error al actualizar la moneda: " . $ex->getMessage();
                            $mensaje .= "\nRecurso: " . $financeURL;
                            $mensaje .= "\nAbreviatura Recurso: " . $key;
                            $mensaje .= "\nAbreviatura Moneda: " . $abreviatura;
                            $mensaje .= "\nFactor de conversion: " . $quote;
                            Log::critical($mensaje);

                            EventDispatcher::dispatchEvent('ERROR_ACTUALIZA_MONEDA', ["[cuerpo]" => $mensaje]);

                            return false;
                        }
                    }
                } else {

                    $mensaje = "Error al actualizar la moneda: El JSON contiene errores";
                    $mensaje .= "\nRecurso: " . $financeURL;
                    $mensaje .= "\nRespuesta del Recurso: " . $financeObj;
                    $mensaje .= "\nCódigo del error: " . $validarJSON['codigo'];
                    $mensaje .= "\nMensaje del error: " . $validarJSON['msj'];

                    Log::critical($mensaje);

                    EventDispatcher::dispatchEvent('ERROR_ACTUALIZA_MONEDA', ["[cuerpo]" => $mensaje]);

                    return false;
                }
            }
        }

        /**
         * Ahora se procede a consultar la TRM para Colombia.
         * Para eso consumimos un servicio soap publicado por la Super Intendecia Financiera.
         **/
        $date = date("Y-m-d");
        try {
            $soap = new soapclient(getenv('SUPER_FINANCIERA_WSDL'), array(
                'soap_version' => SOAP_1_1,
                'trace' => 1,
                "location" => getenv('SUPER_FINANCIERA_WSDL_LOCATION'),
            ));
            $response = $soap->queryTCRM(array('tcrmQueryAssociatedDate' => $date));
            $response = $response->return;
            if ($response->success) {
                $trm = $response->value;
                $moneda = Gemoneda::where('abreviatura', '=', 'COP')->first();
                $moneda["factor_conversion"] = $trm;
                $moneda->update();

                return true;
            }
        } catch (\Exception $e) {
            $mensaje = "Error al actualizar la TRM desde Superintendencia Financiera de Colombia: " . $e->getMessage();
            Log::critical($mensaje);

            EventDispatcher::dispatchEvent('ERROR_ACTUALIZA_MONEDA', ["[cuerpo]" => $mensaje]);
            return false;
        }
    }
}
