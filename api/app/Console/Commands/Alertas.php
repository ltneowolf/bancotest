<?php

namespace App\Console\Commands;

use App\Http\Controllers\BaseController;
use App\Models\GE\Getask;
use App\Models\GE\GetipoAlerta;
use App\Models\User;
use App\Util\SendMail;
use App\Util\SystemParameters;
use App\Util\TemplateUtils;
use Illuminate\Console\Command;

class Alertas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alertas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display an inspiring quote';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $state = SystemParameters::getNumSysParam("ENVIANDO_CORREOS");
        $nuevo = false;
        if ($state == "VALUE_NOT_FOUND") {
            //Creo el parametro
            $nuevo = true;
            $state = SystemParameters::setNumSysParam("ENVIANDO_CORREOS", 1, "Parametro para verificar si esta corriendo un envio de correos");
        }

        if ($nuevo || $state == "0") {
            echo "Sistema de correos no corriendo, se envian mensajes. \n";
            $tareas = Getask::where('finalizado', 0)->get();
            foreach ($tareas as $tarea) {
                $tarea->intentos++;
                $result = "";
                $smtpSettings = null;
                try {
                    //Existen dos tipos de alertas MANUALES Y AUTOMATICAS.
                    //las MANUALES tienen como alerta MANUAL_MAIL e indican simplemente los destinatarios y la plantilla
                    $datos = unserialize($tarea->datos);

                    //Verifico si el usuario tiene configuracion SMTP personalizada
                    //para sacar el usuario logeado se utiliza el correo
                    if (!is_null($tarea->loggedusermail) && !empty($tarea->loggedusermail)) {
                        $user = User::where('email', $tarea->loggedusermail)->first();
                        if ($user && $user->activasmtp) {
                            //El usuario activa la configuracion del smtp
                            $smtpSettings = [
                                "host" => $user->hostsmtp,
                                "port" => $user->portsmtp,
                                "seguridad" => strtoupper($user->seguridadsmtp),
                                "reqAuth" => $user->autenticacionsmtp == 1,
                                "username" => $user->usersmtp,
                                "password" => $user->passwordsmtp,
                                "remitente" => [
                                    "name" => ( !is_null($user->nameremismtp) && !empty($user->nameremismtp) ) ? $user->nameremismtp : $user->nombre,
                                    "mail" => $user->emailremismtp
                                ]];
                        }
                    }

                    if ($tarea->alerta == 'MANUAL_MAIL') {
                        //1) Preparo la plantilla
                        if ($tarea->getemplate_id != "")
                            $renderedTemplate = TemplateUtils::renderMailTemplate($tarea->getemplate_id, $datos);
                        else
                            $renderedTemplate = $datos;

                        //2) Preparo los destinatarios
                        //En este caso se ignora el usuario logeado porque es un email manual
                        //se espera un TO, CC o CCO en los datos del correo
                        $to = "noreply@valleygroups.com";
                        $cc = "";
                        $cco = "";
                        $replyTo = "";

                        if( isset($tarea->destinatario)){
                            $to = $tarea->destinatario;
                        }

                        if (array_key_exists("replyTo", $datos)) {
                            $replyTo = $datos["replyTo"];
                        }

                        if (array_key_exists("[cc]", $datos)) {
                            $cc = $datos["[cc]"];
                        }

                        if (array_key_exists("[cco]", $datos)) {
                            $cco = $datos["[cco]"];
                        }
                        //3 envio los correos
                        $result = SendMail::sendMail(
                            array('asunto' => $renderedTemplate['asunto'],
                                'body' => $renderedTemplate['body'],
                                'destinatario' => $to,
                                'cc' => $cc,
                                'cco' => $cco,
                                'replyTo' => $replyTo,
                                "adjuntos" => array_key_exists("adjuntos", $datos) ? $datos["adjuntos"] : [],
                                'objectgroup' => (isset($renderedTemplate['objectgroup']) && strlen($renderedTemplate['objectgroup']) > 0) ? ($renderedTemplate['objectgroup']) : "",
                                'objectid' => (isset($renderedTemplate['objectid']) && strlen($renderedTemplate['objectid']) > 0) ? ($renderedTemplate['objectid']) : ""
                            ), $smtpSettings);
                    } else {
                        //1) busco las notificaciones qued debe generar esta alerta
                        $alerta = GetipoAlerta::where('nombre', $tarea->alerta)->first();
                        foreach ($alerta->alertas as $notificacion) {
                            //2) Hago el render de la template

                            if ($notificacion->getemplate_id != "")
                                $renderedTemplate = TemplateUtils::renderMailTemplate($notificacion->getemplate_id, $datos);
                            else
                                $renderedTemplate = $datos;

                            $asunto = "SIN ASUNTO";
                            if (array_key_exists("asunto", $renderedTemplate)) {
                                $asunto = $renderedTemplate["asunto"];
                            } else if (array_key_exists("ASUNTO", $renderedTemplate)) {
                                $asunto = $renderedTemplate["ASUNTO"];
                            } else if (array_key_exists("[asunto]", $renderedTemplate)) {
                                $asunto = $renderedTemplate["[asunto]"];
                            } else if (array_key_exists("[ASUNTO]", $renderedTemplate)) {
                                $asunto = $renderedTemplate["[ASUNTO]"];
                            }

                            $body = "SIN BODY VERIFICAR ALERTA " . $alerta->id . "Proyecto: " . env("PROYECTO", "sin proyecto en .env");
                            if (array_key_exists("body", $renderedTemplate)) {
                                $body = $renderedTemplate["body"];
                            } else if (array_key_exists("BODY", $renderedTemplate)) {
                                $body = $renderedTemplate["BODY"];
                            } else if (array_key_exists("[body]", $renderedTemplate)) {
                                $body = $renderedTemplate["[body]"];
                            } else if (array_key_exists("[BODY]", $renderedTemplate)) {
                                $body = $renderedTemplate["[BODY]"];
                            }

                            $datosCorreo = array('asunto' => $asunto, 'body' => $body);
                            // 3) Preparo los destinatarios
                            //$datosCorreo['destinatario'] = "noreply@valleygroups.com";

                            if ($notificacion->isuserlogged) {
                                //Se debe enviar el correo al usuario loggeado
                                if ($tarea->loggedusermail != "") {
                                    $datosCorreo['destinatario'] = $tarea->loggedusermail;
                                }
                            }
                            if (!is_null($tarea->destinatario) && !empty($tarea->destinatario)) {
                                if (!array_key_exists('destinatario', $datosCorreo) || $datosCorreo['destinatario'] == "") {
                                    $datosCorreo['destinatario'] = is_null($tarea->destinatario) ? "noreply@boxexpress.com" : $tarea->destinatario;
                                } else {
                                    $datosCorreo['destinatario'] .= ";" . $tarea->destinatario;
                                }
                            } else {
                                if (!array_key_exists('destinatario', $datosCorreo) || $datosCorreo['destinatario'] == "") {
                                    $datosCorreo['destinatario'] = "noreply@boxexpress.com";
                                }
                            }

                            if ($notificacion->siemprecc == "[CC]") {
                                $cc = str_replace(array_keys($datos), $datos, $notificacion->siemprecc);
                                if (empty($cc)) {
                                    $cc = null;
                                }
                                $datosCorreo['cc'] = $cc;
                            } else {
                                $datosCorreo['cc'] = $notificacion->siemprecc;
                            }

                            if ($notificacion->siemprecco == "[CCO]") {
                                $cco = str_replace(array_keys($datos), $datos, $notificacion->siemprecco);
                                if (empty($cco)) {
                                    $cco = null;
                                }
                                $datosCorreo['cco'] = $cco;
                            } else {
                                $datosCorreo['cco'] = $notificacion->siemprecco;
                            }

                            $result = $result . "\n" . SendMail::sendMail($datosCorreo, $smtpSettings);
                        }
                    }
                    //finalmente datos de la tarea
                    $tarea->traza = $result;
                    $tarea->finalizado = true;
                    $tarea->error = false;
                } catch (\Exception $ex) {
                    $tarea->traza = $ex->getMessage() . "\n" . $ex->getTraceAsString();
                    $tarea->error = true;
                    $tarea->finalizado = false;
                    BaseController::logEx($ex);
                    if (isset($datosCorreo))
                        \Log::info($datosCorreo);
                    //\Log::info($datos);
                }
                $tarea->save();
            }
            SystemParameters::setNumSysParam("ENVIANDO_CORREOS", 0);
            echo "Proceso finalizado. \n";
        }
    }
}
