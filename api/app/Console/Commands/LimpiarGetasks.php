<?php
/**
 * Created by PhpStorm.
 * User: sistemas
 * Date: 12/06/2018
 * Time: 8:32
 */

namespace App\Console\Commands;

use App\Models\GE\Getask;
use Illuminate\Console\Command;
use DB;

class LimpiarGetasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'limpiarGetasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tarea que limpia los getasks que ya finalizaron';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $diasVida = env('DIAS_GETASKS', 15);

        Getask::where('finalizado', '1')
            ->whereRaw("ROUND(SYSDATE  - TRUNC(CREATED_AT)) > " . $diasVida . " ")
            ->delete();;

        echo "Exito";
    }
}