import { Component, isDevMode, OnInit, Renderer2 } from '@angular/core';
import { PageNavService } from "./control-core/services/page-nav.service";
import { IEndpoint } from "./control-core/models/se/endpoint.model";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  currentEndpoint: IEndpoint;

  private _subCurrentEndPoint: Subscription = null;

  constructor(private renderer: Renderer2,
              private _pageNavService: PageNavService) {
  }

  ngOnInit() {
    if(isDevMode()){
      this.renderer.addClass(document.body, 'theme-green');
    }else{
      this.renderer.addClass(document.body, 'theme-white');
    }
    this._subCurrentEndPoint = this._pageNavService.getCurrentEndPoint$()
        .subscribe(endPoint => {
          setTimeout(() => {
            this.currentEndpoint = endPoint;
          });
        });
  }

  ngOnDestroy() {
    if (this._subCurrentEndPoint != null)
      this._subCurrentEndPoint.unsubscribe();
  }
}
