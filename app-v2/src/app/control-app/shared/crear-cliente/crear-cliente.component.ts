import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HttpClient, HttpRequest} from "@angular/common/http";
import {TranslateService} from "@ngx-translate/core";
import {CONTROL_APP_CONST} from "../../config/control-app.constant";
import Swal from 'sweetalert2'
import * as cloneDeep from 'lodash/cloneDeep';

import {DANGER_NOTIFY, notify, WARING_NOTIFY} from "../../../control-core/util/notify.util";
import {Subscription} from "rxjs/Subscription";
import {AuthService} from "../../../control-core/services/auth.service";
import {SHAKE_ANIMATION, animation} from '../../../control-core/util/animation.util';
import {GeparametersService} from "../../../control-core/services/geparameters.service";
import {NGXLogger} from "ngx-logger";
import {BuscarCiudadModel} from "../buscar-ciudad/buscar-ciudad.model";
import {CiudadModel, DepartamentoModel, PaisModel} from "../../models/general/gestion-geografica.model";
import {ITipoTable, TipoTableModel} from "../../models/general/tipo.model";

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-crear-cliente',
  templateUrl: 'crear-cliente.component.html'
})
export class CrearClienteComponent implements OnInit {

  @Output('onBuscaCiudad') buscaCiudadEmit = new EventEmitter<string>();
  @Output() onCreacion: EventEmitter<{ success: boolean, msg: string }> = new EventEmitter();

  public _nombres: string = '';
  public _apellidos: string = '';
  public _direccion: string = '';
  public _telefono: string = '';
  public _identificacion: string = '';
  public _email: string = '';
  public tipoIde: ITipoTable = new TipoTableModel();
  public _ciudad: BuscarCiudadModel = new BuscarCiudadModel(new CiudadModel(), new DepartamentoModel(), new PaisModel());

  dataValidaccion = {
    error: {
      ciudad: false
    },
    text: {
      ciudad: 'SELECCIONE_CIUDAD_LABEL',
    }
  };

  confirmaCliente: string = 'CONFIRMA_ENVIO_INFORMACION_LABEL';

  public dataFormData: FormData;
  public parametroIdenitifcaciones: string = '';
  public listaIdentificacioness: Array<{ id: string, nombre: string }>;
  public _YES_LABEL = this._translate.instant('YES_LABEL');
  public _NO_LABEL = this._translate.instant('NO_LABEL');

  public _objetEmmit: {
    success: boolean,
    msg: string
  };
  private _subGeparameters$: Subscription = null;

  blockSendNotBoton: boolean = false

  dataErrorCliente = {
    error: {
      _nombres: false,
      _apellidos: false,
      _direccion: false,
      _telefono: false,
      _identificacion: false,
      _email: false,
      tipoIde: false,
      _ciudad: false,
    },
    text: {
      required: 'CAMPO_REQUIRED_LABEL',
      email: '',
    }
  };

  constructor(private _http: HttpClient,
              private _translate: TranslateService,
              private _parameters: GeparametersService,
              private _authService: AuthService,
              private _logger: NGXLogger) {
  }

  ngOnInit() {

    this._subGeparameters$ = this._parameters.state$.subscribe((estado) => {
      if (estado == 'ready') {
        this.parametroIdenitifcaciones = this._parameters.getStrval('GRO_TIPO_IDENTIFICACIONES');
      }
    });

    this.listaIdentificacioness = [];

    this.getIdentificacioness();
  }

  sendCliente() {
    try {
      if (!this.checkCliente()) {
        let mensaje = this._translate.instant("ERROR_FORMULARIO_CLIENTE_LABEL");
        notify(mensaje, DANGER_NOTIFY);

        return false;
      }

      this.blockSendNotBoton = true;

      this.dataFormData = new FormData();

      this.dataFormData.append('nombres', this._nombres);
      this.dataFormData.append('apellidos', this._apellidos);
      this.dataFormData.append('direccion', this._direccion);
      this.dataFormData.append('telefono', this._telefono);
      this.dataFormData.append('identificacion', this._identificacion);
      this.dataFormData.append('email', this._email);
      this.dataFormData.append('tipo_identificacion', this.tipoIde.id);
      this.dataFormData.append('geciudade_id', this._ciudad.ciudad_id);

      Swal({
        title: '',
        html: '<span style="font-size: 15px">' + this.confirmaCliente + '</span><br>',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: this._YES_LABEL,
        cancelButtonText: this._NO_LABEL,
        onClose: () =>
          this.blockSendNotBoton = false
      }).then((result) => {
        if (result.value) {

          const req = new HttpRequest<FormData>('POST', `${CONTROL_APP_CONST.API_URL}clientes`, this.dataFormData, {
            reportProgress: true
          });
          this._http.request(req)
            .subscribe(
              (response) => {
                this.blockSendNotBoton = false;
                this._objetEmmit = {success: true, msg: 'SE_REGISTRO_CLIENTE_LABEL'};
              },
              (error) => {
                this.dataFormData = new FormData();
                this.blockSendNotBoton = false;
                this._objetEmmit = {success: false, msg: error};
              }, () => {

                this._nombres = '';
                this._apellidos = '';
                this._direccion = '';
                this._telefono = '';
                this._identificacion = '';
                this._email = '';
                this.tipoIde = new TipoTableModel();
                this._ciudad = new BuscarCiudadModel(new CiudadModel(), new DepartamentoModel(), new PaisModel());

                this._authService.loadUserInfo();
                this.onCreacion.emit(this._objetEmmit);
              }
            );
        } else if (result.dismiss === Swal.DismissReason.cancel) {

          this.blockSendNotBoton = false;
        }
      });
    } catch (err) {
      this.dataFormData = new FormData();
      this.blockSendNotBoton = false;
      this.onCreacion.emit({success: false, msg: 'INTERNAL_ERROR_CONTACT_ADMIN'});
    }
  }

  checkCliente() {
    this.dataErrorCliente.error._nombres = false;
    this.dataErrorCliente.error._apellidos = false;
    this.dataErrorCliente.error._direccion = false;
    this.dataErrorCliente.error._telefono = false;
    this.dataErrorCliente.error._identificacion = false;
    this.dataErrorCliente.error._email = false;
    this.dataErrorCliente.error.tipoIde = false;
    this.dataValidaccion.error.ciudad = false;

    let x = 0;

    if (this._nombres == '') {
      this.dataErrorCliente.error._nombres = true;
      x++;
    }
    if (this._apellidos == '') {
      this.dataErrorCliente.error._apellidos = true;
      x++;
    }
    if (this._direccion == '') {
      this.dataErrorCliente.error._direccion = true;
      x++;
    }
    if (this._telefono == '') {
      this.dataErrorCliente.error._telefono = true;
      x++;
    }
    if (this._identificacion == '') {
      this.dataErrorCliente.error._identificacion = true;
      x++;
    }

    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!re.test(String(this._email ).toLowerCase())){
      this.dataErrorCliente.error._email = true;
      this.dataErrorCliente.text.email = 'EMAIL_NOT_VALID_LABEL';
      x++;
    }

    if (!this.tipoIde.id) {
      let mensaje = this._translate.instant("SEL_TIPO_IDE_LABEL");
      notify(mensaje, DANGER_NOTIFY);
      x++;
    }
    if (!this._ciudad.ciudad_id) {
      this.dataValidaccion.error.ciudad = true;
      x++;
    }

    return (x == 0);
  }

  getIdentificacioness() {
    this._http.get<ITipoTable[]>(`${CONTROL_APP_CONST.API_URL}tiposOcult`, {
      params:
        {grupo: this.parametroIdenitifcaciones}
    })
      .subscribe(
        (response) => {
          response.forEach((value) => {
            this.listaIdentificacioness.push({id: value.id, nombre: value.nombre});
          });
        },
        (error) => {
          this._logger.debug(error.error);
        }
      )
  }

  animateAddBtn() {
    animation(SHAKE_ANIMATION, '#addPqt');
  }

  ngOnDestroy() {
    this._subGeparameters$.unsubscribe();
  }
}
