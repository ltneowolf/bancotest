import { CiudadModel, DepartamentoModel, PaisModel } from "../../models/general/gestion-geografica.model";

export class BuscarCiudadModel {
  private _ciudad: CiudadModel;
  private _ciudad_id: string;
  private _estado: DepartamentoModel;
  private _estado_id: string;
  private _pais: PaisModel;
  private _pais_id: string;
  private _zona: { nombre: string };

  constructor(ciudad: CiudadModel, estado: DepartamentoModel, pais: PaisModel) {
    this.ciudad = ciudad;
    this.ciudad_id = ciudad.id;
    this.estado = estado;
    this.estado_id = estado.id;
    this.pais = pais;
    this.pais_id = pais.id;

    delete this.estado.pais;
  }

  public setFromCiudadModel(ciudad: CiudadModel) {
    this.ciudad = ciudad;
    this.ciudad_id = ciudad.id;
    this.estado = ciudad.departamento;
    this.estado_id = ciudad.departamento.id;
    this.pais = ciudad.departamento.pais;
    this.pais_id = ciudad.departamento.pais.id;

    delete this.ciudad.departamento;
    delete this.estado.pais;
  }

  public getCiudadModel() {
    let ciudadModel = new CiudadModel();
    ciudadModel.id = this.ciudad_id;
    ciudadModel.nombre = this.ciudad.nombre;
    ciudadModel.gedepartamento_id = this.estado_id;
    ciudadModel.departamento = this.estado;
    ciudadModel.departamento.pais = this.pais;
    return ciudadModel;
  }

  get ciudad(): CiudadModel {
    return this._ciudad;
  }

  set ciudad(value: CiudadModel) {
    this._ciudad = value;
  }

  get ciudad_id(): string {
    return this._ciudad_id;
  }

  set ciudad_id(value: string) {
    this._ciudad_id = value;
  }

  get estado(): DepartamentoModel {
    return this._estado;
  }

  set estado(value: DepartamentoModel) {
    this._estado = value;
  }

  get estado_id(): string {
    return this._estado_id;
  }

  set estado_id(value: string) {
    this._estado_id = value;
  }

  get pais(): PaisModel {
    return this._pais;
  }

  set pais(value: PaisModel) {
    this._pais = value;
  }

  get pais_id(): string {
    return this._pais_id;
  }

  set pais_id(value: string) {
    this._pais_id = value;
  }

  get zona(): { nombre: string } {
    return this._zona;
  }

  set zona(value: { nombre: string }) {
    this._zona = value;
  }
}