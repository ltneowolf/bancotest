import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CONTROL_APP_CONST } from "../../config/control-app.constant";
import { ErrorService } from "../../../control-core/services/error.service";
import { TranslateService } from "@ngx-translate/core";
import * as cloneDeep from 'lodash/cloneDeep';
import { BuscarCiudadModel } from "./buscar-ciudad.model";
import { CiudadModel, DepartamentoModel, PaisModel } from "../../models/general/gestion-geografica.model";

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-buscar-ciudad',
  templateUrl: './buscar-ciudad.component.html',
  styleUrls: ['./buscar-ciudad.component.css']
})
export class BuscarCiudadComponent implements OnInit {

  ciudadesEncontradas: BuscarCiudadModel[] = [];
  showList: boolean = false;
  buscando: boolean = false;
  sinRegistros: boolean = false;
  input_id: number = Math.floor((Math.random() * 100) + 1);

  @Input() label: string;
  @Input() errorText: string;
  @Input() hasError: boolean;

  @Input() set ciudad(iCiudad: BuscarCiudadModel) {
    this._ciudad = iCiudad;
    this.ciudadChange.emit(this._ciudad);
  }

  @Output() ciudadChange = new EventEmitter<BuscarCiudadModel>();
  @Output() onIniciaBusqueda = new EventEmitter<string>();

  readonly MIN_LENGTH_TO_SEARCH = 4;
  private _timer = null;
  private _timerInputFocusOut = null;
  private _ciudadBuscando: string;
  private _MINIMO_LETRAS_PARA_BUSCAR = this._translate.instant('MINIMO_LETRAS_PARA_BUSCAR',
      {n_letras: this.MIN_LENGTH_TO_SEARCH});
  private _ciudad: BuscarCiudadModel;

  constructor(private _errorService: ErrorService,
              private _translate: TranslateService,
              private _http: HttpClient) {
    this.label = "NOMBRE_CIUDAD_LABEL";
    this.hasError = false;
    this.errorText = "";
    this._ciudad = new BuscarCiudadModel(new CiudadModel(), new DepartamentoModel(), new PaisModel());
  }

  ngOnInit() {
  }

  onKeyUp($event) {
    if (this.ciudad.ciudad.nombre == "" || this.ciudad.ciudad.nombre == null) {
      this.showList = false;
      this.ciudadesEncontradas.splice(0, this.ciudadesEncontradas.length);
      if (this.ciudad) {
        this.ciudad = new BuscarCiudadModel(new CiudadModel(), new DepartamentoModel(), new PaisModel());
      }
      return
    }

    if ($event.keyCode >= 48 && $event.keyCode <= 90 || $event.keyCode == 8 || $event.keyCode == 13) {
      this.showList = true;
      this.buscando = true;
      this.sinRegistros = false;

      if (this._timer) {
        clearTimeout(this._timer);
      }
      this._timer = setTimeout(() => {
        this._timer = null;
        this.buscarCiudad();
      }, 700);
    }
  }

  onKeyDown($event) {
    //Evento de la tecla borrar
    if ($event.keyCode == 8 && this._timer) {
      clearTimeout(this._timer);
    }
  }

  buscarCiudad() {
    this.hasError = false;

    if (this.ciudad.ciudad.nombre.length < this.MIN_LENGTH_TO_SEARCH) {
      this.showList = false;
      this.buscando = false;
      this.hasError = true;
      this.errorText = this._MINIMO_LETRAS_PARA_BUSCAR;
      return;
    }

    if (this.ciudad.ciudad.nombre == "" || this.ciudad.ciudad.nombre == null
        || this._ciudadBuscando == this.ciudad.ciudad.nombre) {
      this.buscando = false;
      return;
    }

    this.onIniciaBusqueda.emit(this.ciudad.ciudad.nombre);

    this._ciudadBuscando = this.ciudad.ciudad.nombre;
    this._http.get<BuscarCiudadModel[]>(`${CONTROL_APP_CONST.API_URL}consultaCiudad/buscar/${this.ciudad.ciudad.nombre}`)
        .subscribe(response => {
              this.ciudadesEncontradas = response;
              this._ciudadBuscando = "";
              this.buscando = false;
              if (response.length == 0)
                this.sinRegistros = true;
            },
            error => {
              this._errorService.manejaError(error);
              this._ciudadBuscando = "";
            })
  }

  selectItem(ciudad: BuscarCiudadModel) {
    clearTimeout(this._timerInputFocusOut);
    this.ciudad = cloneDeep(ciudad);
    setTimeout(() => {
      this.showList = false
    }, 50);
  }

  onInputFocusOut($event) {
    this._timerInputFocusOut = setTimeout(() => {
      this.showList = false
    }, 150);
  }

  resetCiudad() {
    this._ciudad = new BuscarCiudadModel(new CiudadModel(), new DepartamentoModel(), new PaisModel());
  }

  get ciudad() {
    return this._ciudad;
  }
}
