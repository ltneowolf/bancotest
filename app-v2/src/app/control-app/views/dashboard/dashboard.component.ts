import { Component, OnInit } from '@angular/core';
import { StateService } from '@uirouter/angular';
import { GeparametersService } from "../../../control-core/services/geparameters.service";
import { AuthService } from "../../../control-core/services/auth.service";
import { IUserC, UserCModel } from "../../models/user.model";
import { Subscription } from "rxjs/Subscription";
import * as cloneDeep from 'lodash/cloneDeep';
import { DireccioneModel } from "../../models/general/cliente.model";
import { BuscarCiudadModel } from "../../shared/buscar-ciudad/buscar-ciudad.model";
import { CiudadModel, DepartamentoModel, PaisModel } from "../../models/general/gestion-geografica.model";

declare let jquery: any;
declare let $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  minimoApertura : number = 0;


  objectKeys = Object.keys;

  public objCliente: IUserC = new UserCModel();

  private _addDireccion = "#addDireccion";

  private _objCliente$Unsubscribe: Subscription = null;

  constructor(private _state: StateService,
              private _parameters: GeparametersService,
              private _authService: AuthService) {
  }

  ngOnInit() {

    this._objCliente$Unsubscribe = this._authService.userInfo$
        .subscribe((iUserInfo: IUserC) => {
          this.objCliente = cloneDeep(iUserInfo);
        });

    this._parameters.state$.subscribe((estado) => {
      if (estado == 'ready') {
       this.minimoApertura = this._parameters.getNumval('MINIMO_ACTIVACION_CUENTA');
      }
    });
  }

  goTo(state: string) {
    this._state.go(state);
  }

  ngOnDestroy() {
    this._objCliente$Unsubscribe.unsubscribe();
    $('.modal').modal('hide');
    $('.modal-backdrop').remove();
  }
}
