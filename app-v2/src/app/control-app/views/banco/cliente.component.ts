import {Component, OnInit} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {TranslateService} from "@ngx-translate/core";
import {ErrorService} from "../../../control-core/services/error.service";
import {notify, SUCCESS_NOTIFY} from "../../../control-core/util/notify.util";
import {FormControl, FormGroup} from "@angular/forms";
import {initWaitMe, stopWaitMe} from "../../../control-core/util/waitme.util";
import {CONTROL_APP_CONST} from "../../config/control-app.constant";
import {
  ISimpleTableConfig,
  SimpleTableConfig
} from "../../../control-core/shared/simple-table/simple-table.component";
import {StateService} from "@uirouter/core";

declare let jquery: any;
declare let $: any;

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html'
})
export class ClienteComponent implements OnInit {

  simpleTableDataConfig: ISimpleTableConfig;

  idClienteSel: string = '';

  consultaForm: FormGroup;
  consultando: boolean = false;

  _newCliente: string = '#_newCliente';
  _tblResultadosDiv: string = '#tblResultados';
  _detalleCliente: string = '#_detalleCliente';

  constructor(private _http: HttpClient,
              private _translate: TranslateService,
              private _state: StateService,
              private _errorService: ErrorService) {
    this.consultaForm = new FormGroup({
      identificacion: new FormControl(''),
      email: new FormControl('')
    });
  }

  ngOnInit() {

    this.simpleTableDataConfig = new SimpleTableConfig();
    this.simpleTableDataConfig.columns = [
      {
        label: "NAMES_LABEL",
        name: 'nombres'
      }, {
        label: "APELLIDOS_LABEL",
        name: 'apellidos'
      }, {
        label: "DIRECCION_LABEL",
        name: 'direccion'
      }, {
        label: "CIUDAD_LABEL",
        name: 'ciudad',
        options: {
          render: (data) => {
            return data.nombre;
          }
        }
      }, {
        label: "TELEFONO_LABEL",
        name: 'telefono'
      }, {
        label: "IDENTIFICACION_LABEL",
        name: 'identificacion',
        options: {
          render: (data, index, row) => {
            return row.tipoid.abreviatura + ' ' + data;
          }
        }
      }, {
        label: "EMAIL_LABEL",
        name: 'email'
      }, {
        label: "ASESOR_LABEL",
        name: 'asesor',
        options: {
          render: (data) => {
            return data.name;
          }
        }
      }, {
        label: "DATE_LABEL",
        name: 'created_at'
      },
    ];

    this.consultaClientees();
  }

  consultaClientees() {
    this.consultando = true;
    initWaitMe(this._tblResultadosDiv);
    this._http.get<any[]>(`${CONTROL_APP_CONST.API_URL}clientes`,
      {params: this.consultaForm.value})
      .subscribe(resConsulta => {
          this.simpleTableDataConfig.data = resConsulta;
          this.consultando = false;
          stopWaitMe(this._tblResultadosDiv);
        },
        resError => {
          this._errorService.manejaError(resError);
          this.consultando = false;
          stopWaitMe(this._tblResultadosDiv);
        });
  }

  initRegisCliente() {
    $(this._newCliente).modal('show');
  }

  onClienteCread(response) {
    if (response.success) {
      $(this._newCliente).modal('hide');
      notify(this._translate.instant(response.msg), SUCCESS_NOTIFY);
      this.consultaClientees();
    } else {
      this._errorService.manejaError(response.msg);
    }
  }

  onWarehouseSel(data) {
    if (data.success) {
      $(this._detalleCliente).modal('hide');
      this._state.go("whrdetalle", {nro_whr: data.whr});
    }
  }

  ngOnDestroy() {
    $('.modal').modal('hide');
    $('.modal-backdrop').remove();
  }
}
