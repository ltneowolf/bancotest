export interface IOrdenPago {
  pago_id: string,
  refinterna: string,
  fechavencimiento: string,
  totalpago: number,
  totalpagocop: number,
  pagada: boolean,
  porcomprobar?: boolean,
  pagocolombia?: boolean,
  whr_id: string,
  nrowhr: string,
  created_at: string,
}

export class OrdenPagoModel {
  pago_id: string;
  refinterna: string;
  fechavencimiento: string;
  totalpago: number;
  totalpagocop: number;
  pagada: boolean;
  porcomprobar?: boolean;
  pagocolombia?: boolean;
  whr_id: string;
  nrowhr: string;
  created_at: string;
}
