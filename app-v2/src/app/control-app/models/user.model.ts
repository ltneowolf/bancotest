import { IUser } from "../../control-core/models/se/user.model";

export interface IUserC extends IUser {
  name: string;
  email: string;
}

export class UserCModel implements IUserC {
  id: string = "";
  name: string = "";
  email: string = "";
}
