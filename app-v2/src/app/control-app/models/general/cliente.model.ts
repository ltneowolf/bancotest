import { CiudadModel, ICiudad } from "./gestion-geografica.model";

export interface ICliente {
  id: string,
  idbox: string,
  identificacion ?: string,
  nombres: string,
  apellidos ?: string,
  escorporativo ?: string,
  email: string,
  fecnacimiento ?: string,
  telefono: string,
  telefono_o ?: string,
  totalpuntos ?: number,
  gecliente_categoria_id ?: string,
  direcciones ?: Array<IDireccion>,
  destinatarios ?: Array<IDestinarario>,
  categoria ?: {
    id ?: string,
    nombre ?: string
  }
}

export interface IDireccion {
  id: string,
  zip?: string,
  direccion: string,
  principal: boolean,
  nombres ?: string;
  apellidos ?: string;
  telefono ?: string;
  gecliente_id ?: string,
  gedestinatario_id ?: string,
  ciudad ?: ICiudad
}

export interface IDestinarario {
  id: string,
  gecliente_id: string,
  idbox ?: string,
  identificacion ?: string,
  email ?: string,
  apellidos ?: string,
  nombres: string,
  telefono ?: string,
  telefono_o ?: string,
  dirreciones ?: Array<IDireccion>
}

export class DestinatarioModel implements IDestinarario {
  id: string = '';
  gecliente_id: string = '';
  nombres: string = '';
  apellidos: string = '';
  email: string = '';
  idbox: string = '';
  identificacion: string = '';
  telefono: string = '';
}

export class ClienteModel implements ICliente {
  id: string;
  idbox: string;
  identificacion: string;
  nombres: string;
  apellidos: string;
  email: string;
  fecnacimiento: string;
  telefono: string;
  telefono_o: string;
  totalpuntos: number;
  gecliente_categoria_id: string;
}


export class DireccioneModel {
  id: string = "";
  zip?: string = "";
  direccion: string = "";
  principal: boolean = false;
  nombres: string = '';
  apellidos: string = '';
  gecliente_id ?: string = "";
  gedestinatario_id ?: string = "";
  geciudade_id: string = "";
  ciudad: CiudadModel = new CiudadModel();
  telefono = "";
  idbox = "";
  identificacion: string = "";
  email = "";
}
