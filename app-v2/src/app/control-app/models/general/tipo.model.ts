import {DepartamentoModel, ICiudad} from "./gestion-geografica.model";

export interface ITipoTable {
  id: string;
  nombre: string;
  grupo?: string;
  gemodulo_id: number;
  abreviatura?: string;
}
export class TipoTableModel implements ITipoTable {
  id: string = "";
  nombre: string = "";
  grupo: string = "";
  gemodulo_id: number;
}

export interface IModuloTable {
  id: string;
  nombre: string;
}
