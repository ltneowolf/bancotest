export interface ICiudad {
  id: string;
  nombre: string;
  gezona_id: string;
  gedepartamento_id: string;
  departamento?: DepartamentoModel;
}

export class CiudadModel implements ICiudad {
  id: string = "";
  nombre: string = "";
  gezona_id: string = "";
  gedepartamento_id: string = "";
  departamento: DepartamentoModel = new DepartamentoModel();
}

export interface IDepartamento {
  id: string;
  nombre: string;
  gezona_id: string;
  gepaise_id: string;
  pais?: IPais;
}

export class DepartamentoModel implements IDepartamento {
  id: string = "";
  nombre: string = "";
  gezona_id: string = "";
  gepaise_id: string = "";
  pais: PaisModel = new PaisModel();
}

export interface IPais {
  id: string;
  nombre: string;
}

export class PaisModel implements IPais {
  id: string = "";
  nombre: string = "";
}