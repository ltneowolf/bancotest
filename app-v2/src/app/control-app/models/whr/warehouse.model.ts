import { CiudadModel, ICiudad } from "../general/gestion-geografica.model";

export interface IRespuestaConsultaWHR {
  nrowhr: string,
  whr_id: string,
  guia_id?: string,
  nrogui: string,
  numpeticiones?: number,
  has_operacion?: boolean,
  ref_ordpago?: string,
  totalpago?: number,
  fechavencimiento?: string,
  created_at: string,
  tracking?: string,
  paqdesc?: string,
  peso?: number,
  largo?: number,
  ancho?: number,
  alto?: number,
  valordeclarado?: number,
  estado?: string,
  es_documento: string,
  tipo_paq?: string,
}

export class WharehouseModel {
  peso: number;
  largo: number;
  alto: number;
  ancho: number;
  valordeclarado: number;
  valorasegurado: number;
  id: string = '';
  webpaquete_id: string = '';
  nrowhr: string = '';
  numpeticiones?: number;
  created_at: string = '';
  updated_at: string = '';
  remidbox: string = '';
  remnombres: string = '';
  remapellidos?: string = '';
  remzip?: string = '';
  remciudad?: string = '';
  remdireccion?: string = '';
  rememail?: string = '';
  remtelefono?: string = '';
  desnombres: string = '';
  desapellidos: string = '';
  deszip: string = '';
  desciudad: string = '';
  desdireccion: string = '';
  desemail: string = '';
  destelefono: string = '';
  desidentificacion: string = '';
  desidbox: string = '';
  desid: string = '';
  paqdesc: string = '';
  tracking: string = '';
  geestatu_id: string = '';
  geagencia_id: string = '';
  observacion: string = '';
  historia: IHistoriaWHR[] = [];
  cobros: Array<any> = [];
  contenido: Array<any> = [];
  anulado: boolean = false;
  es_documento: boolean;
  propietario: string = '';
  guia: { whrwarehouse_id: string; id: string; nrogui: string } = {whrwarehouse_id: this.id, id: '', nrogui: ''};
  tienda: { id: string, nombre: string } = {id: '', nombre: ''};
  transportadora: { id: string, nombre: string } = {id: '', nombre: ''};
  ciudaddes: CiudadModel = new CiudadModel();
  ciudadrem: CiudadModel = new CiudadModel();
  newordenpago: string = "";
  despachado: boolean = false;
  cotizacion: any = {};
  notificacion: any = {};

  constructor() {

  }



}

export interface IHistoriaWHR {
  id: string;
  geestatu_id: number;
  statu: string;
  observacion: string;
  usuario: string;
  fecha: string;
}

export class RespuestaConsultaWHRModel implements IRespuestaConsultaWHR {
  nrowhr: string = '';
  whr_id: string = '';
  guia_id: string = '';
  nrogui: string = '';
  ref_ordpago: string = '';
  totalpago: number = 0;
  es_documento: string = '';
  fechavencimiento?: string = '';
  created_at: string = '';
  tracking: string = '';
  paqdesc: string = '';
  peso: number = 0;
  tipo_paq: string = '';
  numpeticiones: number = 0;
  has_operacion: boolean = false;
  largo: number = 0;
  ancho: number = 0;
  alto: number = 0;
  valordeclarado: number = 0;
  estado: string = '';
}