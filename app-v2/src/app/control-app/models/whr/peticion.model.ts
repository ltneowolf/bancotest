export interface IPeticion {
    id_ptc: string;
    tracking: string;
    id_cliente: string;
    idbox: string;
    nombres: string;
    apellidos: string;
    observacion: string;
    finalizado: boolean;
    created_at: string;
    fechaestimada: string;
    tipo_id?: string;
    tipo_nombre?: string;
    comentarios?: Array<IPeticionComentario>;
    warehouses?: Array<IPeticionWHR>
}

export class PeticionModel implements IPeticion {
    id_ptc: string;
    tracking: string;
    id_cliente: string;
    idbox: string;
    nombres: string;
    apellidos: string;
    observacion: string;
    finalizado: boolean;
    created_at: string;
    fechaestimada: string;
}

export interface IPeticionComentario {
    id: string;
    comentario: string;
    idbox: string;
    cliente: string;
    empleado?: string;
    created_at: string;
}

export class PeticionComentarioModel implements IPeticionComentario {
    id: string;
    comentario: string;
    idbox: string;
    cliente: string;
    created_at: string;
}

export interface IPeticionWHR {
    id: string;
    id_whr: string;
    nrowhr: string;
    peso: string;
    largo?: string;
    alto: string;
    ancho: string;
}

export class PeticionWhrModel implements IPeticionWHR {
    id: string;
    id_whr: string;
    nrowhr: string;
    peso: string;
    largo?: string;
    alto: string;
    ancho: string;
}