import {UserModel} from "../../../control-core/models/se/user.model";
import {TipoTableModel} from "../general/tipo.model";

export interface ITransaccion {
  id: number,
  descripcion: string,
  monto: number,
  tipo_transaccion: number,
  cajero_id: number,
  cuenta_ahorro_id: number
}

export class TransaccionModel implements ITransaccion {
  id: number;
  descripcion: string;
  monto: number;
  tipo_transaccion: number;
  cajero_id: number;
  cuenta_ahorro_id: number;
  cajero: UserModel = new UserModel();
  tipotrs: TipoTableModel = new TipoTableModel();

}
