import {UserModel} from "../../../control-core/models/se/user.model";
import {ITransaccion} from "./transaccion.model";

export interface ICuenta {
  id: string,
  numero_cuenta: string,
  estado: boolean,
  cliente_id: number,
  asesor_id: number,
  transacciones?: Array<ITransaccion>
}

export class CuentaModel implements ICuenta {
  id: string;
  numero_cuenta: string;
  estado: boolean;
  cliente_id: number;
  asesor_id: number;
  cliente: UserModel = new UserModel();
  asesor: UserModel = new UserModel();
}
