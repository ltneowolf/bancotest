import {CiudadModel} from "../general/gestion-geografica.model";
import {TipoTableModel} from "../general/tipo.model";
import {UserModel} from "../../../control-core/models/se/user.model";
import {ICuenta} from "./cuenta.model";

export interface ICliente {
  id: string,
  nombres: string,
  apellidos: string,
  direccion: string,
  telefono: string,
  identificacion: string,
  email: string,
  tipo_identificacion: number,
  geciudade_id: number,
  asesor_id: number,
  cuentas?: Array<ICuenta>
}

export class ClienteModel implements ICliente {
  id: string;
  nombres: string;
  apellidos: string;
  direccion: string;
  telefono: string;
  identificacion: string;
  tipo_identificacion: number;
  geciudade_id: number;
  asesor_id: number;
  email: string;
  tipoid: TipoTableModel = new TipoTableModel();
  ciudad: CiudadModel = new CiudadModel();
  asesor: UserModel = new UserModel();
}
