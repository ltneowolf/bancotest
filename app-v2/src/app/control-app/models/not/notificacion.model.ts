export interface IPaquetebas {
  rowId ?: number,
  tracking ?: string,
  valordeclarado ?: number,
  valorasegurado ?: number,
  peso ?: number,
  altopqt ?: number,
  anchopqt ?: number,
  largopqt ?: number,
  url ?: string,
  paqueteShortUrl ?: string,
  cantidad ?: number,
  paqdesc ?: string,
  webtienda_id ?: string,
  webtransportadora_id ?: string,
  tienda ?: ITiendas,
  transportadora ?: ITransportadoras,
}

export class PaquetebasModel implements IPaquetebas {
  rowId ?: number;
  tracking ?: string;
  valordeclarado ?: number;
  valorasegurado ?: number;
  peso ?: number;
  altopqt ?: number;
  anchopqt ?: number;
  largopqt ?: number;
  url ?: string;
  paqueteShortUrl ?: string;
  cantidad ?: number;
  paqdesc ?: string;
}

export interface ITiendas {
  id: string,
  nombre: string
}

export class TiendaModel implements ITiendas {
  id: string;
  nombre: string;
}

export interface ITransportadoras {
  id: string;
  nombre: string;
}

export class TransportadoModel implements ITransportadoras {
  id: string;
  nombre: string;
}