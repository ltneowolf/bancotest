import {AppComponent} from "../../app.component";
import {DashboardComponent} from "../views/dashboard/dashboard.component";
import {ClienteComponent} from "../views/banco/cliente.component";


const appState = {
  name: "app",
  redirectTo: "dashboard",
  component: AppComponent,
  data: {requiresAuth: true}
};

const dashboardState = {
  parent: "app",
  name: "dashboard",
  url: "/dashboard",
  component: DashboardComponent,
  data: {requiresAuth: true}
};

const clientesState = {
  parent: "app",
  name: "clientes",
  url: "/clientes",
  component: ClienteComponent,
  data: {requiresAuth: true}
};

export const CONTROL_APP_STATE = [
  appState,
  dashboardState,
  clientesState
];
