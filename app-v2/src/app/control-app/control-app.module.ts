import {NgModule} from '@angular/core';
import {UIRouterModule} from "@uirouter/angular";

import {CONTROL_APP_STATE} from "./config/control-app.states";
import {ControlCoreModule} from "../control-core/control-core.module";
import {DashboardComponent} from './views/dashboard/dashboard.component';
import {BuscarCiudadComponent} from './shared/buscar-ciudad/buscar-ciudad.component';
import {ClienteComponent} from "./views/banco/cliente.component";
import {CrearClienteComponent} from "./shared/crear-cliente/crear-cliente.component";

@NgModule({
  declarations: [
    DashboardComponent,
    BuscarCiudadComponent,
    ClienteComponent,
    CrearClienteComponent
  ],
  imports: [
    ControlCoreModule,
    UIRouterModule.forChild({
      states: CONTROL_APP_STATE
    })
  ],
  providers: [],
  exports: [
    ControlCoreModule,
    BuscarCiudadComponent,
    ClienteComponent,
    CrearClienteComponent
  ]
})
export class ControlAppModule {
}

