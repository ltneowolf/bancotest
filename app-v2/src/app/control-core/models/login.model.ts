export interface ILogin {
    email: string,
    password: string
}

export class LoginModel implements ILogin {
    public email: string = "";
    public password: string= "";
}