export interface IEndpoint {
    id: number;
    state?: string;
    menu_label: string;
    page_title?: string;
    page_desc: string;
    seendpoint_id?: number;
    show: boolean;
    icon?: string;
    sort: number;
    seendpoints?: Array<IEndpoint>;
}

export class EndpointModel implements IEndpoint {
    id: number;
    menu_label: string;
    page_desc: string;
    show: boolean;
    icon: string;
    sort: number;
}