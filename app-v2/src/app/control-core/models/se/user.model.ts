import {EndpointModel} from "./endpoint.model";
import {RolModel} from "./rol.model";
import { DireccioneModel, IDireccion } from "../../../control-app/models/general/cliente.model";

export interface IUser {
    id: string;
    name: string;
    email: string;
    telefono?: string;
    seendpoints?: Array<EndpointModel>;
    seroles?: Array<RolModel>;
    all_states?: Array<string>
}

export class UserModel implements IUser {
    public id: string;
    public name: string;
    public email: string;
    public seendpoints: Array<EndpointModel> = [];
    public seroles: Array<RolModel> = [];
}