export interface IParameter {
  id: string;
  key: string;
  numval?: number;
  strval?: string;
  observation: string;
}

export class ParameterModel implements IParameter {
  id: string;
  key: string;
  observation: string;
}