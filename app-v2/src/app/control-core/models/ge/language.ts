export interface ILanguage {
    id: number,
    lang: string,
    label: string
}

export class LanguageModel implements ILanguage {
    id: number;
    lang: string;
    label: string;

    constructor(id?: number, lang?: string, label?: string) {
        this.id = id;
        this.lang = lang;
        this.label = label;
    }
}

export interface ILanguageDef {
    key: string,
    value: string
}

export class LanguageDefModel implements ILanguageDef {
    key: string;
    value: string;

    constructor(key?: string, value?: string) {
        this.key = key;
        this.value = value;
    }
}

export interface ILanguagesResponse {
    [codigo: string]: ILanguage
}