import {LoginComponent} from "../views/login/login.component";
import {Transition} from '@uirouter/core';

export const loginState = {
    name: "login",
    url: "/login?email&recordar",
    component: LoginComponent,
    data: {
        isPublicState: true
    },
    resolve: [
        {token: 'returnTo', deps: [Transition], resolveFn: returnTo}
    ]
};

export const CONTROL_CORE_STATE = [
    loginState
];

/**
 * Return To
 *
 * Funcion encargada de controlar el acceso al login, para los casos en donde esta siendo redirigido cuando no ha sido
 * autenticado o esta intentando ingresar directamente al login.
 *
 * Mas info: https://stackblitz.com/github/ui-router/sample-app-angular
 *
 * @param {Transition} transition
 * @returns {TargetState}
 */
export function returnTo(transition: Transition): any {

    if (transition.redirectedFrom() != null) {
        //Redirige a la ruta original donde el usuario intentaba acceder antes de ser autenticado
        return transition.redirectedFrom().targetState();
    }

    const state = transition.router.stateService;

    //Si el usuario no fue redirigido al login entonces se devuelve al estado original de donde vinieron
    if (transition.from().name !== '' && transition.from().name != 'registro') {
        return state.target(transition.from(), transition.params('from'))
    }

    //Si fromState esta vacio entonces se redirecciona al dashboard
    return state.target('dashboard');

}