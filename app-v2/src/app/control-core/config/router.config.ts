import {UIRouter} from '@uirouter/core';
import {approveTransitionHook} from "./router-functions/approve-transition.hook";
import {requiresAuthHook} from "./router-functions/requires-auth.hook";
import {viewTransitionHook} from "./router-functions/view-transition.hook";

export function routerConfigFn(router: UIRouter) {
    const transitionService = router.transitionService;
    requiresAuthHook(transitionService);
    approveTransitionHook(transitionService);
    viewTransitionHook(transitionService);
}