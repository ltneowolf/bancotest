import {CONTROL_APP_CONST} from "../../control-app/config/control-app.constant";

interface ControlCoreConstantInt {
    TOKEN_NAME: string,
    URL_WITHOUT_TOKEN: Array<string>,
    KEY_LANG_NAME_STG:string,
    DEFAULT_LANG:string,
}

export const CONTROL_CORE_CONST: ControlCoreConstantInt = {
    TOKEN_NAME: "access_token",
    URL_WITHOUT_TOKEN: [CONTROL_APP_CONST.API_URL + "lang"],
    KEY_LANG_NAME_STG:"lang_pref",
    DEFAULT_LANG : "es"
};