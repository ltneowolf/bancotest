import {TransitionService} from "@uirouter/core";
import {PageNavService} from "../../services/page-nav.service";

/**
 * View Transition Hook
 *
 * Funcion que actualiza el current state en PageNavService.
 * Tambien previene que se inicie una transicion hacia un state vacio.
 *
 * @param {TransitionService} transitionService
 */
export function viewTransitionHook(transitionService: TransitionService) {
    const viewTransition = (transition) => {
        const pageNavService = transition.injector().get(PageNavService);
        pageNavService.setCurrentState(transition.to());
    };

    /**
     * Previene que se ejecute una transicion hacia un estate vacio.
     */
    transitionService.onBefore({}, transition => {
        if (transition.to().name == '') return false
    });
    transitionService.onSuccess({}, viewTransition);
}