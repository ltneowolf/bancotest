import { AuthService } from "../../services/auth.service";
import { TransitionService } from "@uirouter/core";
import { IUser } from "../../models/se/user.model";
import { NGXLogger } from "ngx-logger";

/**
 * Approve Transition Hook
 * Verifica si el usuario puede acceder a la ruta especificada
 *
 * @param {TransitionService} transitionService
 */
export function approveTransitionHook(transitionService: TransitionService) {
  //Se omiten rutas que tienen la propiedad isPublicState en true
  const approveTransitionCriteria = {
    to: (state) => state.data && !state.data.isPublicState
  };

  transitionService.onBefore(approveTransitionCriteria, (transition) => {
    const authService: AuthService = transition.injector().get(AuthService);
    const logger: NGXLogger = transition.injector().get(NGXLogger);
    const resolvePromise = (resolve) => {
      /**
       * Con el siguiente codigo lo que hacemos es devolver una promesa debido a que cuando se recarga la pagina
       * el sistema debe cargar el user info desde el API y puede darse el caso que cuando se ejecute este metodo
       * aun no haya cargado la informacion y ademas el UIRouter no soporta Observables.
       */
      const nextObservable = (userInfo: IUser) => {
        logger.debug(transition.to(), "TRANSITION TO");
        //Valido que ya se hayan cargado los states del usuario
        if (userInfo.all_states != null) {
          let url = transition.to().url.toUpperCase();
          let stateName = transition.to().name.toUpperCase();
          url = url.substr(1, url.indexOf("/", 1) - 1);
          let filtroState = userInfo.all_states.filter((value) => {
            return (value != null && (value.toUpperCase() === stateName || value.toUpperCase() === url));
          });
          //No encontro el state, entonces se redirecciona al dashboard
          if (filtroState.length == 0) {
            logger.debug("TRANSICION NO APROBADA");
            resolve(transition.router.stateService.target('dashboard'));
          }
          //Encontro state, permite la transicion
          resolve(true);
        }

      };

      authService.userInfo$.subscribe(nextObservable);
    };

    return new Promise(resolvePromise);
  });
}
