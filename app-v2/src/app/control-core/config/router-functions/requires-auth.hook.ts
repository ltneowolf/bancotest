import {AuthService} from "../../services/auth.service";
import {JwtHelperService} from "@auth0/angular-jwt";
import {TransitionService} from "@uirouter/core";

/**
 * Requires Auth Hook
 * Funcion que valida si un state requiere autenticacion.
 *
 * @param {TransitionService} transitionService
 */
export function requiresAuthHook(transitionService: TransitionService) {
    // Se verifica si el estate de destino requiere autenticacion
    const requiresAuthCriteria = {
        to: (state) => state.data && state.data.requiresAuth
    };

    const redirectToLogin = (transition) => {
        const authService: AuthService = transition.injector().get(AuthService);
        const jwtHelperService = new JwtHelperService();
        const $state = transition.router.stateService;
        
        if (jwtHelperService.isTokenExpired(authService.token)) {
            return $state.target('login', undefined, {location: false});
        }
    };

    transitionService.onBefore(requiresAuthCriteria, redirectToLogin, {priority: 10});
}