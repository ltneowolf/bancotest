import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-footer',
    template: `
        <div class="copyright">
            &copy; 2017-2018 <a href="http://boxexpress.com" target="_blank">Control App. Valley Group</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.0
        </div>`
})
export class FooterComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
