import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { Observable } from "rxjs/Observable";
import { StateService } from '@uirouter/core';
import { LanguageService } from "../../services/language.service";
import { ILanguage } from "../../models/ge/language";
import { Subscription } from "rxjs/Subscription";
import { IUserC, UserCModel } from "../../../control-app/models/user.model";
import { TranslateService } from "@ngx-translate/core";
import { CONTROL_APP_CONST } from "../../../control-app/config/control-app.constant";
import { HttpClient } from "@angular/common/http";
import { ErrorService } from "../../services/error.service";
import { notify, SUCCESS_NOTIFY } from "../../util/notify.util";

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent implements OnInit {

  userInfo: IUserC = new UserCModel();
  langPref: string;
  langs: Observable<ILanguage[]>;

  private _editPasswordModal = "#editPasswordModal";
  private _terminosYCondicionesModal = "#terminosYCondicionesModal";
  private _promoShow = "#promoShow";

  menserro = {
    campObErr: this._translate.instant("FIELD_REQUIRED"),
    campNotErr: this._translate.instant("NEW_PASS_ERROR")
  };

  objectNewPassword = {
    oldPassword: '',
    newPassword: '',
    confirmPassword: '',
  };

  objectErrorPassword = {
    oldPasswordError: false,
    newPasswordError: false,
    confirmPasswordError: false,
    oldPasswordText: this.menserro.campObErr,
    newPasswordText: this.menserro.campObErr,
    confirmPasswordText: this.menserro.campObErr
  };

  private _userInfo$Unsubscribe: Subscription = null;

  constructor(private _authService: AuthService,
              private _state: StateService,
              private _http: HttpClient,
              private _errorService: ErrorService,
              private _translate: TranslateService,
              private _languageService: LanguageService) {
  }

  ngOnInit() {

    this.langs = this._languageService.languages$;
    this.langPref = this._languageService.langPref;
    this._userInfo$Unsubscribe = this._authService.userInfo$
        .subscribe((iUserInfo: IUserC) => this.userInfo = iUserInfo);

    //Se cargan las funcionalidades de la template
    $.AdminBSB.leftSideBar.activate();
    $.AdminBSB.rightSideBar.activate();
    $.AdminBSB.navbar.activate();
    $.AdminBSB.dropdownMenu.activate();
    $.AdminBSB.search.activate();

  }

  setLang(lang: string) {
    this._languageService.langPref = lang;
    this.langPref = lang;
  }

  initEditPassword() {
    this.objectNewPassword.oldPassword = '';
    this.objectNewPassword.newPassword = '';
    this.objectNewPassword.confirmPassword = '';
    $(this._editPasswordModal).modal('show');
  }

  logout() {
    this._authService.logout();
    this._state.go("login", {}, {reload: true});
  }

  sendNewPassword() {
    if (this.checkPassword()) {
      return false;
    }

    this._http.put<any>(`${CONTROL_APP_CONST.API_URL}changepassword`,
        {
          newPass: this.objectNewPassword.newPassword,
          oldPass: this.objectNewPassword.oldPassword,
          confirmPass: this.objectNewPassword.confirmPassword
        })
        .subscribe(
            (response) => {
              this.objectNewPassword.oldPassword = '';
              this.objectNewPassword.newPassword = '';
              this.objectNewPassword.confirmPassword = '';

              $(this._editPasswordModal).modal('hide');

              notify(this._translate.instant(response.data), SUCCESS_NOTIFY);
            }, (error) => {
              this._errorService.manejaError(error);
            }
        )
  }

  aceptarTermCondi() {
    this._http.put<any>(`${CONTROL_APP_CONST.API_URL}updatecliente/aceptarTerminosCondiciones`,
        {acepta_terminos_condiciones: 1})
        .subscribe(
            (response) => {
              this._authService.loadUserInfo();
            },
            (error) => {
            }
        )
  }

  checkPassword() {
    let x = 0;

    this.objectErrorPassword.oldPasswordError = false;
    this.objectErrorPassword.newPasswordError = false;
    this.objectErrorPassword.confirmPasswordError = false;

    if (this.objectNewPassword.oldPassword == '') {
      this.objectErrorPassword.oldPasswordError = true;
      x++;
    }

    if (this.objectNewPassword.newPassword == '') {
      this.objectErrorPassword.newPasswordError = true;
      x++;
    }

    if (this.objectNewPassword.confirmPassword == '') {
      this.objectErrorPassword.confirmPasswordError = true;
      this.objectErrorPassword.confirmPasswordText = this.menserro.campObErr;
      x++;
    }

    if (this.objectNewPassword.newPassword != this.objectNewPassword.confirmPassword) {
      this.objectErrorPassword.confirmPasswordError = true;
      this.objectErrorPassword.confirmPasswordText = this.menserro.campNotErr;
      x++;
    }

    return x > 0;
  }

  ngOnDestroy() {
    this._userInfo$Unsubscribe.unsubscribe();
    $(`${this._terminosYCondicionesModal}, ${this._editPasswordModal}, ${this._promoShow}`).remove();
  }
}
