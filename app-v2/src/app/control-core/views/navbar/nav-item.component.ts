import { Component, Input, OnInit } from "@angular/core";
import { EndpointModel, IEndpoint } from "../../models/se/endpoint.model";
import { StateObject } from "@uirouter/core";
import { PageNavService } from "../../services/page-nav.service";
import { Subscription } from "rxjs/Subscription";

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-nav-item',
  templateUrl: './nav-item.component.html'
})
export class NavItemComponent implements OnInit {
  @Input() navItems: IEndpoint[];

  stateActivo: StateObject;

  private _subCurrentState: Subscription = null;
  private _isCurrentChecked: boolean = false;

  constructor(private pageNavService: PageNavService) {
  }

  ngOnInit() {
    let $body = $('body');

    this._subCurrentState = this.pageNavService.getCurrentState$().subscribe(state => {
      this._isCurrentChecked = false;
      this.stateActivo = state;

      if($body.hasClass('overlay-open')){
        $('.overlay').fadeOut();
        $body.removeClass('overlay-open');
      }

    });
  }

  /**
   * Get Menu
   * Filtra los menus visibles, es utilziado por el HTML para la construccion del menu.
   *
   * @param {EndpointModel[]} menu
   * @returns {EndpointModel[]}
   */
  getMenu(menu: IEndpoint[]): IEndpoint[] {
    if (menu == undefined)
      return [];

    return menu.filter(value => {
      return (value.show == true)
    })
  }

  /**
   * Is Active
   * Verifica si el item del menú es el end point activo
   *
   * @param {IEndpoint} endPoint
   * @returns {boolean}
   */
  isActive(endPoint: IEndpoint): boolean {
    if (endPoint.state == this.stateActivo.name) {
      if (!this._isCurrentChecked) {
        this.pageNavService.publicCurrentEndPoint(endPoint);
        this._isCurrentChecked = true;
      }
      return true;
    }
    return false
  }

  navigateTo($event: any, menuItem: IEndpoint) {
    if (menuItem.seendpoints.length > 0) {
      this._templateFunctions($event);
    }
  }

  private _templateFunctions(e) {
    let element = $(e.srcElement);
    let content = element.next();

    if ($(element.parents('ul')[0]).hasClass('list')) {
      let $not = $(e.target).hasClass('menu-toggle') ? e.target : $(e.target).parents('.menu-toggle');

      $.each($('.menu-toggle.toggled')
          .not($not)
          .next(), function (i, val) {
        if ($(val).is(':visible')) {
          $(val).prev().toggleClass('toggled');
          $(val).slideUp();
        }
      });
    }

    element.toggleClass('toggled');
    content.slideToggle(320);
  }

  ngOnDestroy() {
    if (this._subCurrentState)
      this._subCurrentState.unsubscribe();
  }

}