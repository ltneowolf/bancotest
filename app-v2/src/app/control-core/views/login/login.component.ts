import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Subscription } from "rxjs/Subscription";
import { StateService, TargetState } from '@uirouter/core';

import { ILogin, LoginModel } from "../../models/login.model";
import { AuthService } from "../../services/auth.service";
import { notify, SUCCESS_NOTIFY } from "../../util/notify.util";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  @Input() returnTo: TargetState;

  loginForm: FormGroup;
  recordarForm: FormGroup;
  loginModel: ILogin;
  arrayFocus = Array();
  autenticando: boolean = false;
  hasAuthError: boolean;
  authErrorMessage: string;
  recordar: boolean = false;
  recordando = false;

  private _loginAuthServiceRef: Subscription = null;
  private _returnToOriginalState = () => {
    const state = this.returnTo.state();
    const params = this.returnTo.params();
    const options = Object.assign({}, this.returnTo.options(), {reload: true});
    this._state.go(state, params, options);
  };
  private _unsubscribeIsAtuh: Subscription = null;

  constructor(private _authService: AuthService,
              private _state: StateService,
              private _formBuilder: FormBuilder,
              private _renderer: Renderer2,
              private _translateService: TranslateService) {
  }

  ngOnInit() {
    //Verifico si ya esta logueado para redireccionarlo a la ventana desde de donde viene.
    this._unsubscribeIsAtuh = this._authService.isAuthenticated
        .subscribe((autenticated: boolean) => {
          if (autenticated)
            this._returnToOriginalState();
        });

    this.loginModel = new LoginModel();

    this.arrayFocus["email"] = false;
    this.arrayFocus["password"] = false;
    this.arrayFocus["emailR"] = false;

    if (this._state.params.email != null && this._state.params.email != "") {
      this.loginModel.email = this._state.params.email;
      this.arrayFocus["password"] = true;
    }

    if (this._state.params.recordar != null && this._state.params.recordar == 'true') {
      this.modoRecordar(null,true);
    }

    this.authErrorMessage = "";
    this.hasAuthError = false;
    this._renderer.addClass(document.body, 'login-page');
    this.loginForm = this._formBuilder.group(
        {
          email: [this.loginModel.email, [Validators.required, Validators.email]],
          password: ['', [Validators.required]]
        }
    );
    this.recordarForm = this._formBuilder.group(
        {
          emailr: ['', [Validators.required, Validators.email]]
        }
    );

  }

  login() {
    this.hasAuthError = false;
    this.autenticando = true;

    this._loginAuthServiceRef = this._authService.login(this.loginModel)
        .subscribe(
            () => {
            },
            error => {
              this.hasAuthError = true;
              this.authErrorMessage = error.error.message;
              this.autenticando = false
            }
        );
  }

  modoRecordar(e, recordar: boolean) {
    if(e != null){
      e.preventDefault();
    }
    this.recordar = recordar;
  }

  recordarContrasena() {
    this.recordando = true;
    this._authService.remenberPassword(this.loginModel.email)
        .subscribe(
            respuesta => {
              this.recordando = false;
              this.recordar = false;
              notify(this._translateService.instant("EMAIL_RECORDATORIO_ENVIADO"), SUCCESS_NOTIFY);
            },
            error => {
              this.hasAuthError = true;
              this.recordando = false;
              this.authErrorMessage = "USUARIO_NO_REGISTRADO";
            }
        )
  }

  ngOnDestroy() {
    this._renderer.removeClass(document.body, 'login-page');
    if (this._loginAuthServiceRef !== null)
      this._loginAuthServiceRef.unsubscribe();

    if (this._unsubscribeIsAtuh != null)
      this._unsubscribeIsAtuh.unsubscribe();
  }

}
