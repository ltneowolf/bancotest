import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { TranslateModule } from "@ngx-translate/core"
import { UIRouterModule } from "@uirouter/angular";
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/takeLast';
import "rxjs/add/operator/do";

import { CONTROL_CORE_STATE } from "./config/control-core.states";
import { LoginComponent } from './views/login/login.component';
import { NavbarComponent } from './views/navbar/navbar.component';
import { FooterComponent } from './views/footer/footer.component';
import { AuthService } from "./services/auth.service";
import { AuthHttpInterceptorService } from "./services/auth-http-interceptor.service";
import { InitCoreService } from "./services/init.core.service";
import { LanguageService } from "./services/language.service";
import { NavItemComponent } from "./views/navbar/nav-item.component";
import { PageNavService } from "./services/page-nav.service";
import { InputComponent } from './shared/input/input.component';
import { RadioComponent } from './shared/radio/radio.component';
import { SelectLangComponent } from './shared/select-lang/select-lang.component';
import { ErrorService } from './services/error.service';
import { PanelComponent } from './shared/panel/panel.component';
import { SimpleTableComponent } from './shared/simple-table/simple-table.component';
import { InputAreaComponent } from './shared/input-area/input-area.component';
import { LoggerModule, NgxLoggerLevel } from "ngx-logger";
import { environment } from "../../environments/environment";
import { ngfModule } from "angular-file";
import { TooltipModule } from "ngx-tooltip";
import { GeparametersService } from "./services/geparameters.service";
import { BlockQuoteComponent } from './shared/block-quote/block-quote.component';
import { ValidatorService } from "./services/validator-service";

@NgModule({
  declarations: [
    LoginComponent,
    NavbarComponent,
    FooterComponent,
    NavItemComponent,
    InputComponent,
    RadioComponent,
    SelectLangComponent,
    PanelComponent,
    SimpleTableComponent,
    InputAreaComponent,
    BlockQuoteComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ngfModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    UIRouterModule.forChild({
      states: CONTROL_CORE_STATE
    }),
    LoggerModule.forRoot({
      level: environment.production ? NgxLoggerLevel.LOG : NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.OFF
    })
  ],
  providers: [
    AuthService,
    InitCoreService,
    LanguageService,
    PageNavService,
    LoggerModule,
    ErrorService,
    GeparametersService,
    {
      provide: APP_INITIALIZER,
      useFactory: (initCore: InitCoreService) => () => initCore.init(),
      deps: [InitCoreService, AuthService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptorService,
      multi: true,
    },
    ValidatorService,
  ],
  exports:
      [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        UIRouterModule,
        NavbarComponent,
        FooterComponent,
        TranslateModule,
        LoginComponent,
        NavItemComponent,
        InputComponent,
        RadioComponent,
        SelectLangComponent,
        PanelComponent,
        SimpleTableComponent,
        InputAreaComponent,
        ngfModule,
        BlockQuoteComponent,
        TooltipModule
      ]
})

export class ControlCoreModule {
}
