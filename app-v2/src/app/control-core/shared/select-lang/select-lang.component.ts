import { Component, OnInit } from '@angular/core';
import { LanguageService } from "../../services/language.service";
import { Observable } from "rxjs/Observable";
import { ILanguage } from "../../models/ge/language";

@Component({
  selector: 'core-select-lang',
  templateUrl: './select-lang.component.html'
})
export class SelectLangComponent implements OnInit {

  langs: Observable<ILanguage[]>;
  langSelected: string;

  constructor(private _languageService: LanguageService) {
  }

  ngOnInit() {
    this.langs = this._languageService.languages$;
    this.langSelected = this._languageService.langPref;
  }


  changeLang(lang) {
    this._languageService.langPref = lang;
  }


}
