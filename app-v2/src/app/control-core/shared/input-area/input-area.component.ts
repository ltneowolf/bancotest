import {
  Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges,
  ViewChild
} from '@angular/core';
import { FormGroup } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'core-input-area',
  templateUrl: './input-area.component.html'
})
export class InputAreaComponent implements OnChanges, OnInit {

  @ViewChild('core_input_area') input_area: ElementRef;

  @Input() label: string;
  @Input() id: string = 'inpta_' + Date.now();
  @Input() name: string;
  @Input() formGroup?: FormGroup;
  @Input() hasError: boolean;
  @Input() errorText: string;
  @Input() helpText?: string;
  @Input() disabled: boolean = false;
  @Input() rows: number;
  @Input() autofocus: boolean;
  @Input() maxlength: number = 10000000;

  @Input() ctModel: any;
  @Output() ctModelChange = new EventEmitter();

  @Output('onFocus') outOnFocus = new EventEmitter();
  @Output('onFocusOut') outOnFocusOut = new EventEmitter();

  focused: boolean;
  private _model: any;
  private _MAX_LENGHT_TEXT = '';

  constructor(private _translate: TranslateService) {
    this.focused = false;
  }

  ngOnInit() {
    this._MAX_LENGHT_TEXT = this._translate.instant('MAX_LENGTH_VALUE', {maxl: this.maxlength})
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ctModel != null && changes.ctModel.currentValue != null) {
      if (changes.ctModel.currentValue.length > this.maxlength) {
        this.hasError = true;
        this.errorText = this._MAX_LENGHT_TEXT;
      }
      this._model = changes.ctModel.currentValue;
    }
  };

  get model(): any {
    return this._model;
  }

  set model(value: any) {
    /*Valida que la longitud del texto no sea mayor al maxlength, si es mayor no actualiza mas el modelo*/
    if (value.length > this.maxlength) {
      value = this._model;
    } else {
      this.hasError = false;
      this.errorText = '';
    }
    this._model = value;
    this.ctModelChange.emit(this._model);
  }

  onFocus = ($event) => {
    this.focused = true;
    this.outOnFocus.emit($event)
  };

  onFocusOut = ($event) => {
    this.focused = false;
    this.outOnFocusOut.emit($event);
  };

  /**
   * Set Focus
   *
   * Permite setear manualmente el foco del input
   */
  public setFocus = () => {
    this.input_area.nativeElement.focus();
  }
}
