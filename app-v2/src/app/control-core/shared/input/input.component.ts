import {
  Component, Input, Output, EventEmitter, SimpleChanges, OnChanges, OnInit,
  ViewChild, ElementRef
} from '@angular/core';
import { FormGroup } from "@angular/forms";
import { ValidatorService } from "../../services/validator-service";

/**
 * Input Component
 *
 * Componente encargado de crear input, modo de uso:
 *
 * Si se va a usar en un formulario formGroup:
 * <app-input
 *  type="email"
 *  label="EMAIL_FIELD_LABEL"
 *  [formGroup]="registroForm" -->Este es el objeto encargado de manejar el formulario
 *  name="email"
 *  id="email">
 *</app-input>
 *
 * Si no se va a usar en un formulario formGroup:
 * <app-input
 *  type="email"
 *  label="EMAIL_FIELD_LABEL"
 *  [hasError]="error.hasError"
 *  [errorText]="error.errorText"
 *  [disabled]="true"
 *  [(ctModel)]="email.ctmodel"
 *  name="email"
 *  id="email">
 *</app-input>
 *
 */
@Component({
  selector: 'core-input',
  templateUrl: './input.component.html'
})
export class InputComponent implements OnChanges, OnInit {

  @ViewChild('core_input') input: ElementRef;

  @Input() type: string;
  @Input() label: string;
  @Input() id: string = 'inpt_' + Date.now();
  @Input() name: string;
  @Input() formGroup?: FormGroup;
  @Input() hasError: boolean;
  @Input() errorText?: string;
  @Input() helpText?: string;
  @Input() disabled: boolean = false;
  @Input() step: number = 1;
  @Input() autofocus: boolean;
  @Input() placeholder: string = "";

  @Input() ctModel: any;
  @Output() ctModelChange = new EventEmitter();

  @Output('onFocus') outOnFocus = new EventEmitter();
  @Output('onFocusOut') outOnFocusOut = new EventEmitter();

  focused: boolean;
  private _model: any;

  constructor(private _validatorService: ValidatorService) {
  }

  ngOnInit() {
    this.focused = false;
  };

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ctModel != null && changes.ctModel.currentValue != null) {
      this._model = changes.ctModel.currentValue;
    }
  }

  get model(): any {
    return this._model;
  }

  set model(value: any) {
    this._model = value;
    this.ctModelChange.emit(this._model);
  }

  onFocus($event) {
    this.focused = true;
    this.outOnFocus.emit($event)
  }

  onFocusOut($event) {
    this.focused = false;
    this.outOnFocusOut.emit($event)
  }

  /**
   * Set Focus
   *
   * Permite setear manualmente el foco del input
   */
  public setFocus = () => {
    this.input.nativeElement.focus();
  };

  isEmptyControl() {
    return (this._validatorService.isEmpty(this.formGroup.controls[this.name].value));
  }
}
