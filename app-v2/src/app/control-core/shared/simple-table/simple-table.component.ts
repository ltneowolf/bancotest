import { Component, EventEmitter, Input, OnInit, Output, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'core-simple-table',
  templateUrl: './simple-table.component.html'
})
export class SimpleTableComponent implements OnInit {

  @Input() dataConfig: ISimpleTableConfig;
  @Input() title: string;
  @Input() description: string;
  @Input() id_table?: string = Math.floor((Math.random() * 100) + 1) + '_cstbl';
  @Input() color?: string = '';

  @Output('onSelectRow') outSelectedRow: EventEmitter<any> = new EventEmitter<any>();

  selectedRow;

  constructor(private _sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  printData(index, column, row, data) {
    if (column.options != null) {
      if (column.options.render != null) {
        let dataTmp = column.options.render(data, index, row);
        return this._sanitizer.sanitize(SecurityContext.HTML, dataTmp);
      } else if (column.options.action != null) {
        return "";
      } else {
        return "NO DEFINIDO";
      }
    } else {
      return data;
    }
  }

  selectRow(row) {
    if (this.dataConfig.allowSelectRow) {
      if (row == this.selectedRow) {
        this.selectedRow = {};
        this.outSelectedRow.emit(this.selectedRow);
      } else {
        this.selectedRow = row;
        this.outSelectedRow.emit(this.selectedRow);
      }
    }
  }
}

export interface ISimpleTableConfig {
  columns: Array<{ label: string, name: string, options?: { render?: (data?, index?, row?) => string, action?: (data?, row?) => void } }>;
  optionColumns: Array<{ label: string, action?: (data?, row?) => void, hidden?: (data?) => boolean }>;
  data: Array<any>;
  allowSelectRow: Boolean;
}

export class SimpleTableConfig<T> implements ISimpleTableConfig {
  columns: Array<{ label: string, name: string, options?: {} }>;
  optionColumns: Array<{ label: string, action?: (data?, row?) => void, hidden?: (data?) => boolean }>;
  data: Array<T> = [];
  private _allowSelectRow: boolean = false;

  constructor(columns?) {
    this.columns = columns;
  }

  get allowSelectRow(): boolean {
    return this._allowSelectRow;
  }

  set allowSelectRow(value: boolean) {
    this._allowSelectRow = value;
  }
}