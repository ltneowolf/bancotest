import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'core-panel',
  templateUrl: './panel.component.html'
})
export class PanelComponent implements OnInit {

  @Input() title: string;
  @Input() description: string = '';
  @Input() numcolum?: number = 12;
  @Input() color?: string;
  @Input() icono?: string;
  @Input() clases?: string = '';
  @Input() actions?: IActionPanel[];
  @Input() id_panel?: string = '';
  @Input() showHelpButton?: boolean = false;

  @Output() onHelp: EventEmitter<boolean> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  helpFunction(){
    this.onHelp.emit(true);
  }

}

export interface IActionPanel {
  label: string;
  action: () => void;
  show?: () => boolean;
}
