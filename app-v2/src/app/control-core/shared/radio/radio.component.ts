import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

export interface IRadioElement {
  value: any;
  label: string,
  disabled?: boolean
}

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html'
})
export class RadioComponent implements OnChanges {

  @Input() radioElements: IRadioElement[] = [];
  @Input() name: string;
  @Input() id: string;
  @Input() label: string;
  @Input() hasError: boolean;
  @Input() errorText: boolean;

  @Input() ctModel: any;
  @Output() ctModelChange = new EventEmitter<any>();

  private _model: IRadioElement;

  constructor() {
  }


  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ctModel.currentValue != null && this.radioElements.length > 0) {
      this._model = this.radioElements.filter(value => {
        return value.value == changes.ctModel.currentValue;
      })[0];
    }
  }

  get model(): IRadioElement {
    return this._model;
  }

  set model(value: IRadioElement) {
    this._model = value;
    this.ctModelChange.emit(value.value);
  }

}