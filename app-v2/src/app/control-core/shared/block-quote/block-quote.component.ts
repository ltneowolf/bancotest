import { Component, Input, OnInit, SecurityContext } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'core-block-quote',
  templateUrl: './block-quote.component.html'
})
export class BlockQuoteComponent implements OnInit {

  @Input('class') inClass: string = 'col-md-4';
  @Input() label: string = '';

  @Input('text') set text(value: string) {
    this._text = value + '';
  }

  private _text: string = '';

  constructor(private _sanitizer: DomSanitizer) {
  }

  ngOnInit() {
  }

  get text() {
    /**
     * Reemplaza los santos de linea \n por <br>
     */
    return this._sanitizer.sanitize(SecurityContext.HTML, this._text.replace(/\n/g, "<br>"));
  }

}
