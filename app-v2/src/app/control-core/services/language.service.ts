import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Observable} from "rxjs/Observable";

import {ILanguageDef, ILanguage} from "../models/ge/language";
import {CONTROL_CORE_CONST} from "../config/control-core.constant";
import {CONTROL_APP_CONST} from "../../control-app/config/control-app.constant";
import {DANGER_NOTIFY, notify} from "../util/notify.util";
import {TranslateService} from "@ngx-translate/core";

@Injectable()
export class LanguageService {

    private _langPref: string;
    private _languages: BehaviorSubject<ILanguage[]> = new BehaviorSubject<ILanguage[]>([]);
    private _langDefs: Array<ILanguageDef>;
    private _langURL: string = `${CONTROL_APP_CONST.API_URL}lang`;

    constructor(private http: HttpClient, private translateService: TranslateService) {
        this._langPref = localStorage.getItem(CONTROL_CORE_CONST.KEY_LANG_NAME_STG);
        if (this._langPref == null) {
            this._langPref = CONTROL_CORE_CONST.DEFAULT_LANG;
            localStorage.setItem(CONTROL_CORE_CONST.KEY_LANG_NAME_STG, this._langPref);
        }

        this.translateService.use(this._langPref);
    }

    /**
     * Set Lang Pref
     *
     * Setea el lenguaje que el sistema debe utilizar
     *
     * @param {string} langPref
     */
    set langPref(langPref: string) {
        if (this._langPref !== langPref) {
            this._langPref = langPref;
            localStorage.setItem(CONTROL_CORE_CONST.KEY_LANG_NAME_STG, langPref);
            this.loadLangDefs(this._langPref);
            this.translateService.use(this._langPref);
        }
    }

    /**
     * Load Lang Defs
     *
     * Carga todas las definiciones del lenguaje envíado.
     *
     * @param {string} ilanguage Lenguaje, si no se envía carga el lenguaje almacenado en el local storage.
     */
    loadLangDefs(ilanguage?: string) {
        let loadLang = this._langPref;

        if (ilanguage != null)
            loadLang = ilanguage;

        this.http.get<ILanguageDef[]>(`${this._langURL}/${loadLang}`)
            .subscribe(
                resLangDef => {
                    this._langDefs = resLangDef;
                    this.translateService.setTranslation(this._langPref,this._langDefs);
                },
                resError => {
                    notify("Error al cargar la definición de lenguajes", DANGER_NOTIFY);
                    console.log(resError);
                }
            );
    }

    /**
     * Load Languages
     *
     * Carga los lenguajes soportados por el sistema.
     */
    loadLanguages(): void {
        this.http.get<ILanguage[]>(`${CONTROL_APP_CONST.API_URL}langs`)
            .subscribe(
                resLanguage => {
                    this._languages.next(resLanguage);
                },
                resError => {
                    console.log(resError);
                    notify("Error al cargar los lenguajes", DANGER_NOTIFY)
                }
            )
    }

    /**
     * Get Languages
     * Obtiene el listado de lenguages soportados por el sistema.
     *
     * @returns {Observable<ILanguage[]>}
     */
    get languages$(): Observable<ILanguage[]> {
        return this._languages.asObservable();
    }

    get langPref(): string {
        return this._langPref;
    }

    get langURL(): string {
        return this._langURL;
    }

}