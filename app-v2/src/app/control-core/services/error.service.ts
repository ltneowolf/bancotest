import { Injectable } from '@angular/core';
import { DANGER_NOTIFY, notify } from "../util/notify.util";
import { TranslateService } from "@ngx-translate/core";
import { HttpErrorResponse } from "@angular/common/http";

@Injectable()
export class ErrorService {

  constructor(private _translateService: TranslateService,) {
  }

  manejaError(response: HttpErrorResponse): string {
    let mensaje = "";
    switch (response.status) {
      case 0:
      case -1:
        mensaje = this._translateService.instant("INTERNAL_ERROR_CONTACT_ADMIN");
        notify(mensaje, DANGER_NOTIFY);
        break;
      case 500:
        console.log(typeof response.error);
        if (typeof response.error != 'undefined') {
          if (typeof response.error.error == 'string') {
            mensaje = this._translateService.instant(response.error.error.toUpperCase());
            notify(mensaje, DANGER_NOTIFY);
          } else {
            mensaje = this.errorGeneral(response.error);
          }
        }
        break;
      case 400:
        mensaje = this.errorGeneral(response.error);
        break;
      case 401:
        mensaje = this._translateService.instant("NO_TIENE_PERMISOS");
        notify(mensaje, DANGER_NOTIFY);
        break;
      default:
        mensaje = this.errorGeneral(response.error);
        break;
    }
    return mensaje;
  }

  errorGeneral(error): string {
    if (typeof error == "string") {
      return this.errorString(error);
    }
    let mensaje = "<ul>";
    if (Array.isArray(error)) {
      error.forEach(value => {
        mensaje += "<li>" + this._translateService.instant(value) + "</li>";
      });
      mensaje += "</ul>";
    } else {
      for (let property in error) {
        if (error.hasOwnProperty(property)) {
          //cada atributo es un vector de mensajes:
          mensaje += "<li>" + this._translateService.instant(property) + ": ";
          let m = error[property];
          if (m.length > 0) {
            mensaje += "<ul>"
          }
          if (Array.isArray(m)) {
            for (let i = 0; i < m.length; i++) {
              mensaje += "<li>" + this._translateService.instant(m[i].toUpperCase()) + "</li>"
            }
          } else if (typeof m == 'object') {
            for (let mProperty in m) {
              if (m.hasOwnProperty(mProperty)) {
                mensaje += "<li>" + this._translateService.instant(m[mProperty]) + "</li>"
              }
            }
          } else {
            mensaje += "<li>" + this._translateService.instant(m.toUpperCase()) + "</li>"
          }
          if (m.length > 0) {
            mensaje += "</ul>"
          }
          mensaje += "</li>"
        }
      }
    }
    notify(mensaje, DANGER_NOTIFY);
    return mensaje.replace(/<ul><li>|<\/li><li>/gi, "\n")
        .replace(/<\/ul\>|\<\/li>/gi, "");
  }

  errorString(error: string): string {
    notify(this._translateService.instant(error), DANGER_NOTIFY);
    return error;
  }
}
