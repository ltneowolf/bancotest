import { Injectable } from '@angular/core';
import { FormControl, ValidatorFn, AbstractControl } from '@angular/forms';

const SOLO_NUMEROS_REGEX = /^\d+$/;

/**
 * Validator Service
 *
 * Servicio para realizar validaciones de datos
 *
 * Modo de instanciacion:
 *
 * @component({})
 * export class componente {
 *  constructor (private _validatorService: ValidatorService) {
 *
 *  }
 * }
 */
@Injectable()
export class ValidatorService {


  constructor() {
  }

  /**
   * Solo Numeros Form Validartor
   *
   * Funcion que se utiliza en la validacion de forlmularios reactivos, para validar que el valor solo tenga numeros.
   *
   * Modo de uso:
   * telefono: new FormControl('', [this._validatorService.soloNumerosFormValidator]);
   *
   * @param {FormControl} control
   * @returns {{notNumeric: boolean}}
   */
  public soloNumerosFormValidator = (control: FormControl): { [key: string]: any } | null => {
    return this.soloNumeros(control.value) ? null : {noEsNumerico: true};
  };

  /**
   * Solo Numeros
   * Funcion para validar si un string contiene solo valores numericos sin signo ni decimales
   *
   * @param {string} valor
   * @returns {boolean}
   */
  public soloNumeros = (valor: any): boolean => {
    return (!this.isEmpty(valor) && SOLO_NUMEROS_REGEX.test(valor));
  };

  /**
   * Is Empty
   * Valida si una variable esta vacia
   * @param data
   * @returns {boolean}
   */
  public isEmpty = (data: any): boolean => {
    if (typeof(data) === 'object') {
      if (JSON.stringify(data) === '{}' || JSON.stringify(data) === '[]') {
        return true;
      } else if (!data) {
        return true;
      }
      return false;
    } else if (typeof(data) === 'string') {
      if (!data.trim()) {
        return true;
      }
      return false;
    } else if (typeof(data) === 'undefined') {
      return true;
    } else {
      return false;
    }
  };

}
