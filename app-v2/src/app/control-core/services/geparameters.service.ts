import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { IParameter } from "../models/ge/parameter.model";
import { Observable } from "rxjs/Observable";
import { CONTROL_APP_CONST } from "../../control-app/config/control-app.constant";
import { ErrorService } from "./error.service";
import { NGXLogger } from "ngx-logger";

@Injectable()
export class GeparametersService {

  private _state: BehaviorSubject<string> = new BehaviorSubject('wait');
  private _listParameters: IParameter[];

  constructor(private _http: HttpClient,
              private _errorService: ErrorService,
              private _logger: NGXLogger) {
    this._listParameters = [];
  }

  loadParameters() {
    return this._http.get<IParameter[]>(`${CONTROL_APP_CONST.API_URL}geparametersServ`)
        .subscribe(
            lisParamsResponse => {
              this._listParameters = lisParamsResponse;
              this._state.next('ready');
            },
            errorResponse => {
              this._state.next('error');
              this._errorService.manejaError(errorResponse);
            }
        );
  }

  private filterParam(key: string): IParameter {
    let filtro = this._listParameters.filter((parameter) => {
      return parameter.key.toUpperCase() == key.toUpperCase();
    });

    if (filtro.length > 0) {
      return filtro[0];
    }

    this._logger.error(key, 'PARAMETER_NOT_FOUND');
    throw new Error('PARAMETER_NOT_FOUND');
  }

  getNumval(key: string): number {
    try {
      return this.filterParam(key).numval;
    }
    catch (e) {
      throw new Error(e);
    }
  }

  getStrval(key: string): string {
    try {
      return this.filterParam(key).strval;
    }
    catch (e) {
      throw new Error(e);
    }
  }

  getParam(key: string): IParameter {
    try {
      return this.filterParam(key);
    }
    catch (e) {
      throw new Error(e);
    }
  }

  get listParameters(): IParameter[] {
    return this._listParameters;
  }

  get state$(): Observable<string> {
    return this._state.asObservable();
  }
}
