import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { JwtHelperService } from "@auth0/angular-jwt";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { tap } from "rxjs/operators";

import { CONTROL_APP_CONST } from "../../control-app/config/control-app.constant";
import { CONTROL_CORE_CONST } from "../config/control-core.constant";
import { LoginModel } from "../models/login.model";
import { ErrorService } from "./error.service";
import { GeparametersService } from "./geparameters.service";
import { IUserC, UserCModel } from "../../control-app/models/user.model";
import { StateService } from "@uirouter/core";

export interface LoginRequestInterface {
  token: string,
  userId: string
}

const USER_ID_STORAGE = "user_id";

/**
 * Auth Service
 *
 * Servicio encargado de autenticar usuarios en la aplicación.
 *
 */
@Injectable()
export class AuthService {

  private _urlLogIn: string = CONTROL_APP_CONST.API_URL + 'login';
  private _urlLogOut: string = CONTROL_APP_CONST.API_URL + 'logout';
  private _jwtHelper = new JwtHelperService();
  private _token: string = "";
  private _userInfo: BehaviorSubject<IUserC> = new BehaviorSubject<IUserC>(new UserCModel);
  private _authenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient,
              private _errorService: ErrorService,
              private _parameters: GeparametersService,
              private _state: StateService) {
    this._token = localStorage.getItem(CONTROL_CORE_CONST.TOKEN_NAME);
    if (!this._jwtHelper.isTokenExpired(this._token)) {
      this._authenticated.next(true);
    }
  }

  /**
   * Get Token
   *
   * Obtiene el token almacenado en el local storage
   * @returns {string}
   */
  get token() {
    return this._token;
  }

  /**
   * Get User ID
   *
   * Retorna el ID del usuario logueado.
   *
   * @returns {string}
   */
  get userID(): string {
    return localStorage.getItem(USER_ID_STORAGE);
  }

  /**
   * Get Login URL
   * Retorna la URL de login
   * @returns {string}
   */
  get urlLogin(): string {
    return this._urlLogIn;
  }

  /**
   * Get URL Logout
   *
   * Retorna la URL de Logout
   * @returns {string}
   */
  get urlLogout(): string {
    return this._urlLogOut;
  }

  /**
   * Set Token
   *
   * Almacena el token en el localStorage
   *
   * @param {string} token
   */
  set tokenFn(token: string) {
    this._token = token;
    localStorage.setItem(CONTROL_CORE_CONST.TOKEN_NAME, this._token);
  }

  /**
   * Get Token Expiration Date
   *
   * Retorna la fecha de expiracion del token
   *
   * @returns {Date}
   */
  get tokenExpirationDate(): Date {
    return this._jwtHelper.getTokenExpirationDate(this.token);
  }

  /**
   * Is Authenticated
   *
   * Verifica si un usuario esta autenticado
   *
   * @returns {boolean}
   */
  get isAuthenticated() {
    return this._authenticated.asObservable();
  }

  get userInfo$(): Observable<IUserC> {
    return this._userInfo.asObservable();
  }

  loadUserInfo() {
    if (this.userID != null || this.userID != '') {
      return this.http.get<UserCModel>(`${CONTROL_APP_CONST.API_URL}users/${this.userID}`)
          .pipe()
          .subscribe(
              userInfoResponse => {
                this._userInfo.next(userInfoResponse);
                this._parameters.loadParameters();
              },
              errorResponse => {
                this._authenticated.next(false);
                this._errorService.manejaError(errorResponse);
                localStorage.removeItem(USER_ID_STORAGE);
                localStorage.removeItem(CONTROL_CORE_CONST.TOKEN_NAME);
                this._state.go('login');
              }
          );
    }
  }

  login(loginModel: LoginModel): Observable<LoginRequestInterface> {
    return this.http.post<LoginRequestInterface>(this._urlLogIn, loginModel)
        .pipe(tap(
            (loginResponse: LoginRequestInterface) => {
              this.tokenFn = loginResponse.token;
              localStorage.setItem(USER_ID_STORAGE, loginResponse.userId);
              this.loadUserInfo();
              this._authenticated.next(true);
            },
            (errorData) => {
              this._authenticated.next(false);
            })
        );
  }

  logout() {
    localStorage.removeItem(CONTROL_CORE_CONST.TOKEN_NAME);
    localStorage.removeItem(USER_ID_STORAGE);
    this._userInfo.next(new UserCModel());
    this._authenticated.next(false);

    this.http.post(this._urlLogOut, {})
        .subscribe(() => {
          this._token = null;
        });
  }

  remenberPassword(email: string): Observable<any> {
    return this.http.post(`${CONTROL_APP_CONST.API_URL}recordar`, {email: email})
  }

}
