import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { CONTROL_APP_CONST } from "../../control-app/config/control-app.constant";
import { AuthService } from "./auth.service";
import { CONTROL_CORE_CONST } from "../config/control-core.constant";
import { NGXLogger } from "ngx-logger";

/**
 * Auth Http Interceptor Service
 *
 * Servicio que intercepta las peticiones HTTP y si van dirigidas hacía el API entonces les agrega el header de
 * autenticación.
 *
 */
@Injectable()
export class AuthHttpInterceptorService implements HttpInterceptor {

  constructor(private _authService: AuthService, private _logger: NGXLogger) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let tmpUrlRq = req.url.substr(0, CONTROL_APP_CONST.API_URL.length).toUpperCase();

    //Valido que la URL de la petición no este en el array de url's sin token
    let isUrlWhiteList = (CONTROL_CORE_CONST.URL_WITHOUT_TOKEN.indexOf(req.url) !== -1);
    if (!isUrlWhiteList)
      isUrlWhiteList = (CONTROL_APP_CONST.URL_WITHOUT_TOKEN.indexOf(req.url) !== -1);

    /**
     * A todas las peticiones realizadas hacia el API le agregamos el navbar de autenticacion.
     * Excepto a la url del login y las url que están en las constantes de la aplicacion
     */
    if (tmpUrlRq === CONTROL_APP_CONST.API_URL.toUpperCase()
        && !isUrlWhiteList
        && req.url != this._authService.urlLogin) {
      let reqCl = req.clone({
        headers: req.headers.set("Authorization", `Bearer ${this._authService.token}`)
      });

      return next.handle(reqCl).do(() => {
      }, (resError) => this._logger.error(resError));
    }

    return next.handle(req);
  }
}