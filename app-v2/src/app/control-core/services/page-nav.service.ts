import { Injectable } from "@angular/core";
import { UIRouter } from "@uirouter/core";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { StateObject } from "@uirouter/angular";
import { Observable } from "rxjs/Observable";
import { EndpointModel, IEndpoint } from "../models/se/endpoint.model";

@Injectable()
export class PageNavService {

  private _currentState$: BehaviorSubject<StateObject>;
  private _currentEndPoint$: BehaviorSubject<IEndpoint> = new BehaviorSubject<IEndpoint>(new EndpointModel());

  constructor(private _uiRouter: UIRouter) {
    this._currentState$ = new BehaviorSubject<StateObject>(this._uiRouter.stateService.$current);
  }

  setCurrentState(state: StateObject) {
    this._currentState$.next(state);
  }

  getCurrentState$(): Observable<StateObject> {
    return this._currentState$.asObservable();
  }

  getCurrentEndPoint$(): Observable<IEndpoint> {
    return this._currentEndPoint$.asObservable();
  }

  publicCurrentEndPoint(endPoint: IEndpoint) {
    this._currentEndPoint$.next(endPoint);
  }

}