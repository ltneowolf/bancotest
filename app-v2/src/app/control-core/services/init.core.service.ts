import { Injectable } from "@angular/core/";
import { AuthService } from "./auth.service";
import { LanguageService } from "./language.service";
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable()
export class InitCoreService {

  constructor(private _authService: AuthService,
              private _languageService: LanguageService) {
  }

  init() {
    let jwtHelper = new JwtHelperService();
    if (!jwtHelper.isTokenExpired(this._authService.token)) {
      this._authService.loadUserInfo();
    }

    this._languageService.loadLanguages();
    this._languageService.loadLangDefs();
  }
}