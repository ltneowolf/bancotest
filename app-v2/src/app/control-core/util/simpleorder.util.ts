/**
 * simplerOrder
 * Organiza un Array de Objetos en base a una Key del objeto
 *
 * @param colection
 * @param key
 * @param typedata
 * @param order
 */
export function simplerOrder(colection: Array<any>, key: string, typedata: string, order: string = '') {

  colection.sort((a, b) => {
    let ini = order == 'DESC' ? b : a;
    let end = order == 'DESC' ? a : b;

    if (typedata == 'number') {
      return parseFloat(ini[key]) - parseFloat(end[key])
    } else {
      return ini[key].localeCompare(end[key])
    }
  });

  return colection;
}