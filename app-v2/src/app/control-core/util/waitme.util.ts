declare var jquery: any;
declare var $: any;

export interface IWaitMeOption {

  // animation effect. Use: 'bounce' - default, 'none', 'rotateplane', 'stretch',
  // 'orbit', 'roundBounce', 'win8', 'win8_linear', 'ios', 'facebook', 'rotation',
  // 'timer', 'pulse', 'progressBar', 'bouncePulse', 'img'.
  effect ?: string;
  // place text under the effect. Use: 'text'.
  text ?: string;
  //background for container. Use: 'rgba(255,255,255,0.7)' - default, false.
  bg ?: string;
  // color for background animation and text. Use: '#000' - default, ['','',...] - you can use multicolor for effect.
  color ?: string;
  // set max size for elem animation. Use: '' - default, 40
  maxSize ?: number;
  // wait time im ms to close. Use: -1 - default, 3000
  waitTime ?: number
  // change text position. Use: 'vertical' - default, 'horizontal'.
  textPos ?: string;
  // change font size. Use: '' - default, '18px'.
  fontSize ?: number;
  // - url to image. Use: '' - default, 'url' - for effect: 'img'
  source ?: string;
}

export class WaitMeOptionModel {
  // animation effect. Use: 'bounce' - default, 'none', 'rotateplane', 'stretch',
  // 'orbit', 'roundBounce', 'win8', 'win8_linear', 'ios', 'facebook', 'rotation',
  // 'timer', 'pulse', 'progressBar', 'bouncePulse', 'img'.
  effect ?: string;
  text ?: string;
  bg ?: string;
  color ?: string;
  maxSize ?: number;
  waitTime ?: number;
  textPos ?: string;
  fontSize ?: number;
  source ?: string;
}

export const BOUNCE_ANIMATION = 'bounce';
export const BOUNCEIN_ANIMATION = 'bounceIn';
export const POULSE_ANIMATION = 'poulse';

/**
 * initWaitMe
 * Aplica un efecto de bloqueo en un elemento determinado, acepta valores en el parametro opcions
 *
 * @param elemento
 * @param options
 */
export function initWaitMe(elemento, options ?: IWaitMeOption) {
  let iOptions = new WaitMeOptionModel();

  iOptions.color = '#C19E57';

  if (options != null) {
    Object.assign(iOptions, options);
  }

  setTimeout(() => {
    $(elemento).waitMe(iOptions);
  }, 50);
}

/**
 * stopWaitMe
 * aplica un efecto de animacion a un elemento seleccionado
 *
 * @param elemento
 */
export function stopWaitMe(elemento) {
  $(elemento).waitMe('hide');
}