declare var jquery: any;
declare var $: any;

export const BOUNCE_ANIMATION = 'bounce';
export const BOUNCEIN_ANIMATION = 'bounceIn';
export const PULSE_ANIMATION = 'pulse';
export const SHAKE_ANIMATION = 'shake';

/**
 * animation
 * aplica un efecto de animacion a un elemento seleccionado
 *
 * @param animation
 * @param elemento
 */
export function animation(animation, elemento) {
    var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
    $(elemento).addClass('animated ' + animation).one(animationEnd, function () {
        $(elemento).removeClass('animated ' + animation);
    });
}