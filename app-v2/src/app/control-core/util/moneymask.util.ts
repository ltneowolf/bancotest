export const SYMBOL_COP = 'COP';
export const SYMBOL_USD = 'USD';

/**
 * Notify
 * Muestra un "toastr" en la ventana, mas informacion: http://bootstrap-growl.remabledesigns.com/
 *
 * @param data
 * @param symbol
 */
export function moneymask(data: number|string, symbol?): string {
  if (symbol == null)
    symbol = "";

  if (data != null) {
    return symbol + ' $' + data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1'")
  }

  return '';
}