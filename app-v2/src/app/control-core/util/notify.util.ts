declare var jquery: any;
declare var $: any;

export const SUCCESS_NOTIFY = 'success';
export const INFO_NOTIFY = 'info';
export const WARING_NOTIFY = 'warning';
export const DANGER_NOTIFY = 'danger';

/**
 * Notify
 * Muestra un "toastr" en la ventana, mas informacion: http://bootstrap-growl.remabledesigns.com/
 *
 * @param iMessage
 * @param iType
 * @param iSettings
 */
export function notify(iMessage, iType, iSettings?) {
  let settings = {type: iType, offset: {y: 80, x: 20}, z_index: 2000};

  if (iSettings != null)
    Object.assign(settings, iSettings);

  $.notify({message: iMessage}, settings);
}