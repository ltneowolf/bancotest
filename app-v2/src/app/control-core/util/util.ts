/**
 Util
 Clase para almacenar las utilerias del sistema.
 */

/**
 * Extraer Numeros
 *
 * Funcion encargada de extraer todos los numeros de una cadena de texto.
 *
 * @param data string
 * @returns {string}
 */
export function extraerNumeros(data): string {
  let numeros = data.match(/(\d+)/g);
  if (Array.isArray(numeros)) {
    return numeros.join('');
  }
  return "";
}