import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ControlAppModule} from "./control-app/control-app.module";
import {TranslateModule} from "@ngx-translate/core";
import {UIRouterModule, UIView} from "@uirouter/angular";
import {routerConfigFn} from "./control-core/config/router.config";


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        ControlAppModule,
        TranslateModule.forRoot(),
        UIRouterModule.forRoot({
            otherwise: {state: 'dashboard'},
            config: routerConfigFn
        })
    ],
    providers: [],
    bootstrap: [UIView]
})
export class AppModule {
}
