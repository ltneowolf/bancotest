<?php

namespace Illuminate\Database;

use App\Exceptions\SeederExistsException;
use Illuminate\Console\Command;
use Illuminate\Container\Container;

class Seeder
{
    /**
     * The container instance.
     *
     * @var \Illuminate\Container\Container
     */
    protected $container;

    /**
     * The console command instance.
     *
     * @var \Illuminate\Console\Command
     */
    protected $command;

    public function __construct()
    {
        $class = get_class($this);

        $exist = \App\Models\Seed::where('nombre', $class)->first();

        if (isset($exist)) {
            throw new SeederExistsException("Class $class already exists in the database");

        } else {
            if (($class != 'DatabaseSeeder') && ($class != 'AppExecSeeder') && ($class != 'GuiasMasaSeeder') && ($class != 'DummyDataSeed')) {
                /*
                 * Insert the $class's name in the Seed table
                 * */
                \App\Models\Seed::create(['nombre' => $class]);
            }
        }

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    }

    /**
     * Seed the given connection from the given path.
     *
     * @param  string $class
     * @return void
     */
    public function call($class)
    {
        try {
            $this->resolve($class)->run();

            if (isset($this->command)) {
                $this->command->getOutput()->writeln("<info>Seeded:</info> $class");
            }

        } catch (SeederExistsException $e) {
            //echo $e;
        }

    }

    /**
     * Resolve an instance of the given seeder class.
     *
     * @param  string $class
     * @return \Illuminate\Database\Seeder
     */
    protected function resolve($class)
    {
        if (isset($this->container)) {
            $instance = $this->container->make($class);

            $instance->setContainer($this->container);
        } else {
            $instance = new $class;
        }

        if (isset($this->command)) {
            $instance->setCommand($this->command);
        }

        return $instance;
    }

    /**
     * Set the IoC container instance.
     *
     * @param  \Illuminate\Container\Container $container
     * @return $this
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;

        return $this;
    }

    /**
     * Set the console command instance.
     *
     * @param  \Illuminate\Console\Command $command
     * @return $this
     */
    public function setCommand(Command $command)
    {
        $this->command = $command;

        return $this;
    }
}
