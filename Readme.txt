------------------------ API ------------------------
instalar composer
$ composer install
Configurar el enviroment en la raiz del proyecto.
Generar la clave del api con el comando 
$ php artisan key:generate
------------------------ FIN API ------------------------

------------------------ APP ------------------------
instalar node.js
$ git config --global url."https://".insteadOf git://
$ npm install
$ npm install bower
$ npm install gulp
$ bower install --save
$ gulp build-dev

1. Ir a la carpeta src/js/core/config/ copiar el archivo env.js.example y pegarlo con el nombre env.js, configurar las URL
del API

2. Copiar la carpeta assets a public/
En la carpeta src/js/ang_pre crear el archivo appVersion.js y agregar las siguientes variables APP_VERSION y APP_DATE
------------------------ FIN APP ------------------------

------------------------ APP-V2 ------------------------
instalar node.js

> npm install

1. Ir a la carpeta src/app/control-app/config/ copiar el archivo control-app.constant.ts.bk y pegarlo con nombre el control-app.constant.ts
configurar las URL del API
2. Para compilar: ng build --base-href=/app/
3. Para servidor en modo desarrollo: ng serve
4. En el archivo src/assets/js/helpers.js modificar la funcion checkCompatibleBrowser, modifique las versiones de los
navegadores incompatibles y tambien modifique la pagina a donde debe redigir la aplicación en caso de ser incompatible.
------------------------ FIN APP-V2 ------------------------

En windows:

Revisar que tenga instalados los clientes para la base de datos
Si utiliza Oracle:
Crear la variable de entorno ORACLE_HOME
Instalar los drivers de Oracle